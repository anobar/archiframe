﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ArchiFrame.Account.Register1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        
    <div class="rightnav" >
        <div class="body_header_style">
        Sign Up 
    </div>
        <div id="reg1"> 


     
                           <ol>
                           <li>
                               <label class="lable">First name<em class="red">*</em></label><br />
                                   <asp:TextBox runat="server" ID="txtFirstName"  />         
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />" />
                                         
                              </li>

                             <li>
                             <label class="lable">Last name<em class="red">*</em></label><br />
                               <asp:TextBox runat="server" ID="txtlastName"  />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtlastName"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  />
                                  
                            </li>
                            <li>
                                 <label class="lable">Email(user name)<em class="red">*</em></label><br />
                                  <asp:TextBox runat="server" ID="txtEmail" TextMode="Email" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  />
                              
             
                            </li>
                               </ol>
                           </div>
         <div id="reg2"> 
             <ol>
                            <li>
                                 <label class="lable">Password<em class="red">*</em></label><br />
                                <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  />
                                
                            </li>
                               <li>

                               <label class="lable">Confirm Password<em class="red">*</em></label><br />
                                <asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="Password" />
                                <asp:RequiredFieldValidator ID="fieldValConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                     CssClass="field-validation-error" Display="Dynamic" ErrorMessage="<img src='../images/error.png' />"  />

                                <asp:CompareValidator ID="compareValConfirmPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"
                                     CssClass="field-validation-error" Display="Dynamic" ErrorMessage="Passwords are not match." />
                               </li>
                               <li>
                                 <label class="lable">Company name<em class="red">*</em></label><br />
                           
                                <asp:TextBox runat="server" ID="txtCompanyName"/>
                                   
                                <asp:RequiredFieldValidator ID="fieldValCompanyName" runat="server" ControlToValidate="txtCompanyName"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  />

                               </li>
                               </ol>
               <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="cssbutton" OnClick="btnRegister_Click" />
                         </div>
                          
                           <asp:Label ID="lblErrorMessage" runat="server" Text=""  CssClass="field-validation-error"></asp:Label>
                        

      
                      
                            
                       
                            
                </div>
    
</asp:Content>
