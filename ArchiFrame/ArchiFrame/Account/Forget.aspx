﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Forget.aspx.cs" Inherits="ArchiFrame.Account.Forget" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <div id="leftnav">
               
                  <ul class="menu" >
		            <li class="item1"   ><a  href="/Default.aspx" class="active"> Home</a>
		
		            </li>
		            <li class="item2" >  <a href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			      
                        </ul>
		            </li>
		            <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                                   <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                      </ul>
		            </li>  
		          <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  

	               </ul>
          </div>
       <div class="rightnav" >
        <div class="body_header_style">
      Forgot my Password
    </div>

 <div class="rightnav"  >
        <div id="middlenav"> 
            Please enter your email address you used to sign-up and a new password will be sent to your email. 
            <br /> <br />
             <label class="lable">Email(user name)<em class="red">*</em></label><br />
                        <asp:TextBox runat="server" ID="txtEmail" TextMode="Email" CssClass="textbox" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                        CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  />
             <asp:CustomValidator ID="IsUserExist" runat="server" ControlToValidate="txtEmail" CssClass="field_validation_error" ErrorMessage="User name does not exsit." OnServerValidate="userValidator"></asp:CustomValidator>
            <br />
           <asp:Button ID="btnSendEmail" runat="server" Text="Send" OnClick="btnSendEmail_Click"  CssClass="cssbutton" />
            <br />
              </div>
        
        </div>
</asp:Content>
<asp:Content runat="server" ID="footerContent" ContentPlaceHolderID="Footer">
    </asp:Content>