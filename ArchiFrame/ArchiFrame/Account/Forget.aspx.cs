﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using ArchiFrame.Class;
using System.Text;
namespace ArchiFrame.Account
{
    public partial class Forget : System.Web.UI.Page
    {
         protected MembershipUser usr ;
         private static Random random = new Random((int)DateTime.Now.Ticks);
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void userValidator(object source, ServerValidateEventArgs args)
        {
           usr= Membership.GetUser(args.Value);
           args.IsValid = (usr != null);
        }
        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
             if (Page.IsValid)
	            {
                 string  txtpasword =  RandomString(8);
                 usr.ChangePassword(usr.ResetPassword(), txtpasword);
                 string emailtxt = "Your new password  is : \n\r  " + txtpasword;
                 //Common.SendEmail("ArciFrame", usr.Email, "New Password", emailtxt);
                 Common.SendEmail("no-reply@arciframe.com.au", usr.Email, "ArciFrame New Password", emailtxt);

                 Response.Redirect(HttpContext.Current.Request.Url.Scheme 
                                    + "://"
                                    + HttpContext.Current.Request.Url.Authority 
                                    + HttpContext.Current.Request.ApplicationPath + "showmessage.aspx/?msg=2");
	            }
           
           
        }
        private string RandomString(int size)
        {
           
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }
    }
}