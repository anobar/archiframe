﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ArchiFrame.Account.Login" %>
<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
     <div id="leftnav">
               
                      <ul class="menu" >
		            <li class="item1"  ><a  href="/Default.aspx"> Home</a>
		
		            </li>
		            <li class="item2" >  <a  href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			        
                        </ul>
		            </li>
		          
			     
		        <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                                   <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                      </ul>
		            </li>  
  <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  
	               </ul>
          </div>
   <div class="rightnav" >
        <div class="body_header_style">
      Login
    </div>
       <div id="reg12">
       <div id="reg1"> 
         
           <asp:Login runat="server"  ID ="LoginBox" ViewStateMode="Disabled" RenderOuterTable="false">
            <LayoutTemplate>
                <p class="validation-summary-errors">
                    <asp:Literal runat="server" ID="FailureText"  />
                </p>
           
                
                    <ol>
                        <li>
                         <label class="lable">Username<em class="red">*</em></label><br />
                            <asp:TextBox runat="server" ID="UserName"  CssClass="textbox"/>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="UserName" CssClass="field-validation-error" ErrorMessage="The user name field is required." />
                        </li>
                        <li>
                               <label class="lable">Password<em class="red">*</em></label><br />
                            <asp:TextBox runat="server" ID="Password" TextMode="Password"  CssClass="textbox" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="Password" CssClass="field-validation-error" ErrorMessage="The password field is required." />
                        </li>
                        <li>
                            <asp:CheckBox runat="server" ID="RememberMe" />
                           <label class="lable">Remember Me</label><br />
                        </li>
                    </ol>
                    <asp:Button  runat="server" CommandName="Login" CssClass="cssbutton" Text="Log in"/>

            
            </LayoutTemplate>
        </asp:Login>
        <p>
            <asp:HyperLink runat="server" ID="RegisterHyperLink" ViewStateMode="Disabled" CssClass="underline">Register</asp:HyperLink>
         
            if you don't have an account.
        
            <br /> <br />
             <label  class="lblbtn"><a style="text-decoration:none;" href="../Account/Forget.aspx">Forgort password</a></label>

        <p>

                       
            
         </p>
           </div>
        <div class="reg2"> 
                
                         <asp:Label runat="server" ID="lblMsg" Text=""  class="validation-summary-msg"/><br />
                   
                        
                  
              
               
          
            </div>

       </div>
     
       </div>
</asp:Content>
<asp:Content runat="server" ID="footerContent" ContentPlaceHolderID="Footer">
    </asp:Content>