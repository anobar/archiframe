﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
namespace ArchiFrame.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        
            
      
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
           
            RegisterHyperLink.NavigateUrl = "Register.aspx";
       
            if (Request.QueryString.Get("msg") != null)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "", true);
                lblMsg.Text = " Congratulations! your email is confirmed successfully.You can now log on to the system.";
              
            }
          

          
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }


           
        }

        protected void lnkForget_Click(object sender, EventArgs e)
        {
         Response.Redirect("Forget.aspx");
        }

      

        
    

       

        
      
    }
}