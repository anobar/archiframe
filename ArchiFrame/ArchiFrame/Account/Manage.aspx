﻿<%@ Page Title="Manage Account" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="ArchiFrame.Account.Manage" %>
<%@ Register Src="~/Account/OpenAuthProviders.ascx" TagPrefix="uc" TagName="OpenAuthProviders" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
<div id="leftnav">
               
                  <ul class="menu" >
		            <li class="item1"  ><a  href="/Default.aspx"> Home</a>
		
		            </li>
		            <li class="item2" >  <a  href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			      
                              </ul>
		            </li>
		            <li class="item3"  ><a class="active" href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a class="selected" href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                                   <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                      </ul>
		            </li>  
		          <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  

	               </ul>
          </div>
     <div class="rightnav" >
       <div id="reg1" style="width:320px"> 
     <div class="body_header_style">
     Change Password
    </div>
    <section id="passwordForm">
        <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
            <p class="message-success"><%: SuccessMessage %></p>
        </asp:PlaceHolder>

        

        <asp:PlaceHolder runat="server" ID="setPassword" Visible="false">
         
            <fieldset>
                <legend>Set Password Form</legend>
                <ol>
                    <li>
                        <asp:Label runat="server" AssociatedControlID="password">Password</asp:Label>
                        <asp:TextBox runat="server" ID="password" TextMode="Password" CssClass="textbox" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="password"
                            CssClass="field-validation-error" ErrorMessage="The password field is required."
                            Display="Dynamic" ValidationGroup="SetPassword" />
                        
                        <asp:ModelErrorMessage runat="server" ModelStateKey="NewPassword" AssociatedControlID="password"
                            CssClass="field-validation-error" SetFocusOnError="true" />
                        
                    </li>
                    <li>
                        <asp:Label runat="server" AssociatedControlID="confirmPassword">Confirm password</asp:Label><br />
                        <asp:TextBox runat="server" ID="confirmPassword" TextMode="Password" CssClass="textbox" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="confirmPassword"
                            CssClass="field-validation-error" Display="Dynamic" ErrorMessage="The confirm password field is required."
                            ValidationGroup="SetPassword" />
                        <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="confirmPassword"
                            CssClass="field-validation-error" Display="Dynamic" ErrorMessage="The password and confirmation password do not match."
                            ValidationGroup="SetPassword" />
                    </li>
                </ol>
                <asp:Button runat="server" Text="Set Password" ValidationGroup="SetPassword" OnClick="setPassword_Click"  CssClass="cssbutton"/>
            </fieldset>
        </asp:PlaceHolder>

        <asp:PlaceHolder runat="server" ID="changePassword" Visible="false">
           
            <asp:ChangePassword runat="server" CancelDestinationPageUrl="~/" ViewStateMode="Disabled" RenderOuterTable="false" SuccessPageUrl="Manage.aspx?m=ChangePwdSuccess">
                <ChangePasswordTemplate>
                    <p class="validation-summary-errors">
                        <asp:Literal runat="server" ID="FailureText" />
                    </p>
                    <fieldset class="changePassword">
                        <legend>Change password details</legend>
                        <ol>
                            <li>
                                <asp:Label runat="server" ID="CurrentPasswordLabel" AssociatedControlID="CurrentPassword">Current password<em class="red">*</em></asp:Label><br />
                                <asp:TextBox runat="server" ID="CurrentPassword"  TextMode="Password" CssClass="textbox" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="CurrentPassword"
                                    CssClass="field-validation-error" ErrorMessage="The current password field is required."
                                    ValidationGroup="ChangePassword" />
                            </li>
                            <li>
                                <asp:Label runat="server" ID="NewPasswordLabel" AssociatedControlID="NewPassword">New password<em class="red">*</em></asp:Label><br />
                                <asp:TextBox runat="server" ID="NewPassword" CssClass="textbox" TextMode="Password" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="NewPassword"
                                    CssClass="field-validation-error" ErrorMessage="The new password is required."
                                    ValidationGroup="ChangePassword" />
                            </li>
                            <li>
                                <asp:Label runat="server" ID="ConfirmNewPasswordLabel" AssociatedControlID="ConfirmNewPassword">Confirm new password<em class="red">*</em></asp:Label><br />
                                <asp:TextBox runat="server" ID="ConfirmNewPassword" CssClass="textbox" TextMode="Password" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmNewPassword"
                                    CssClass="field-validation-error" Display="Dynamic" ErrorMessage="Confirm new password is required."
                                    ValidationGroup="ChangePassword" />
                                <asp:CompareValidator runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword"
                                    CssClass="field-validation-error" Display="Dynamic" ErrorMessage="The new password and confirmation password do not match."
                                    ValidationGroup="ChangePassword" />
                            </li>
                        </ol>
                        <asp:Button runat="server" CommandName="ChangePassword" Text="Change password" ValidationGroup="ChangePassword"  CssClass="cssbutton"/>
                    </fieldset>
                </ChangePasswordTemplate>
            </asp:ChangePassword>
        </asp:PlaceHolder>
    </section>

    <%--<section id="externalLoginsForm">
        
        <asp:ListView runat="server"
            ItemType="Microsoft.AspNet.Membership.OpenAuth.OpenAuthAccountData"
            SelectMethod="GetExternalLogins" DeleteMethod="RemoveExternalLogin" DataKeyNames="ProviderName,ProviderUserId">
        
            <LayoutTemplate>
                <h3>Registered external logins</h3>
                <table>
                    <thead><tr><th>Service</th><th>User Name</th><th>Last Used</th><th>&nbsp;</th></tr></thead>
                    <tbody>
                        <tr runat="server" id="itemPlaceholder"></tr>
                    </tbody>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    
                    <td><%#: Item.ProviderDisplayName %></td>
                    <td><%#: Item.ProviderUserName %></td>
                    <td><%#: ConvertToDisplayDateTime(Item.LastUsedUtc) %></td>
                    <td>
                        <asp:Button runat="server" Text="Remove" CommandName="Delete" CausesValidation="false" 
                            ToolTip='<%# "Remove this " + Item.ProviderDisplayName + " login from your account" %>'
                            Visible="<%# CanRemoveExternalLogins %>" />
                    </td>
                    
                </tr>
            </ItemTemplate>
        </asp:ListView>

        <h3>Add an external login</h3>
        <uc:OpenAuthProviders runat="server" ReturnUrl="~/Account/Manage.aspx" />
      
    </section>--%>
             </div>

        </div>
</asp:Content>
