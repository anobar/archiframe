﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ArchiFrame.Account.Register1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

         <div id="leftnav">
               
                    <ul class="menu" >
		            <li class="item1"  ><a  href="/Default.aspx"> Home</a>
		
		            </li>
		            <li class="item2" >  <a  href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			                 <li class="subitem1" ><a href="/Estimator/ReportSys.aspx">Report</a></li>
                              </ul>
		            </li>
		       <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a  href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                                   <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                      </ul>
		            </li>  
  <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  
	               </ul>
          </div>
    <div class="rightnav" >
        <div class="body_header_style">
        Sign Up 
    </div>
        <div id="reg12">
        <div id="reg1"> 


     
                           <ol>
                           <li>
                               <label class="lable">First name<em class="red">*</em></label><br />
                                   <asp:TextBox runat="server" ID="txtFirstName"  CssClass="textbox" />         
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />" /><br /><br />
                                         
                              </li>

                             <li>
                             <label class="lable">Last name<em class="red">*</em></label><br />
                               <asp:TextBox runat="server" ID="txtlastName" CssClass="textbox"  />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtlastName"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  /><br /><br />
                                  
                            </li>
                            <li>
                                 <label class="lable">Email(user name)<em class="red">*</em></label><br />
                                  <asp:TextBox runat="server" ID="txtEmail" TextMode="Email" CssClass="textbox"  />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  /><br />
                              
             
                            </li>
                               </ol>
                           </div>
         <div id="reg2"> 
             <ol>   
                  <li>
                                 <label class="lable">Company name<em class="red">*</em></label><br />
                           
                                <asp:TextBox runat="server" ID="txtCompanyName" CssClass="textbox" />
                                   
                                <asp:RequiredFieldValidator ID="fieldValCompanyName" runat="server" ControlToValidate="txtCompanyName"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  /><br /><br />

                               </li>
                            <li>
                                 <label class="lable">Password<em class="red">*</em></label><br />
                                <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="textbox"  />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassword"
                                    CssClass="field-validation-error" ErrorMessage="<img src='../images/error.png' />"  /><br /><br />
                                
                            </li>
                               <li>

                               <label class="lable">Confirm Password<em class="red">*</em></label><br />
                                <asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="Password" CssClass="textbox" />
                                <asp:RequiredFieldValidator ID="fieldValConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                     CssClass="field-validation-error" Display="Dynamic" ErrorMessage="<img src='../images/error.png' />"  /><br />

                                <asp:CompareValidator ID="compareValConfirmPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"
                                     CssClass="field-validation-error" Display="Dynamic" ErrorMessage="Passwords are not match." />
                               </li>
                            
                               </ol>
          
                         </div>
                       </div>  
           <asp:Label ID="lblErrorMessage" runat="server" Text=""  CssClass="field-validation-error"></asp:Label>
          <div class="btnholder"> <div class="btnholderight"> <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="cssbutton" OnClick="btnRegister_Click" /></div></div>
                </div>
    
    
</asp:Content>
<asp:Content runat="server" ID="footerContent" ContentPlaceHolderID="Footer">
    </asp:Content>