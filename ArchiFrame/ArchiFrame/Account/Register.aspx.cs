﻿using ArchiFrame.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace ArchiFrame.Account
{
    public partial class Register1 : System.Web.UI.Page
    {
        static string DEFAULT_ROLE = "Administrator";
        //static string CnnString = ConfigurationManager.ConnectionStrings["ArchiFrameConnectionString"].ConnectionString;
        //static string AddClientQuery = "INSERT INTO [dbo].[Client] ([companyName]) VALUES (@companyName); SELECT CAST(scope_identity() AS int)";
        //static string UpdateUsersTableQuery = "UPDATE Users SET FirstName=@firstName, LastName=@LastName, clientId= @clientId where UserId=@userId";

        private ArchiFrameEntities db = new ArchiFrameEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private string AddUser()
        {
           
            MembershipUser usr = Membership.CreateUser(txtEmail.Text, txtPassword.Text, txtEmail.Text);
            
            string userId = usr.ProviderUserKey.ToString();
            return userId;
        }


        private string AddAdditionalUserProfileInfo_Transaction(string userId)
        {
            //  SqlTransaction transaction = null;
            //  SqlConnection con = null;
            string retVa = null;

            try
            {
                string[] users = { txtEmail.Text };

                //Add user details in user_profile table
                User_Profile newuser = new User_Profile();
                newuser.UserId = Guid.Parse(userId);
                newuser.UserFirstName = txtFirstName.Text;
                newuser.UserLastName = txtlastName.Text;
                //Check and Add company
                var newclient = from s in db.Clients
                                where s.companyName == txtCompanyName.Text.Trim()
                                select s.ID;
                if (newclient.ToList().Count == 0)
                {
                    Client client = new Client();
                    client.companyName = txtCompanyName.Text.Trim();
                    if (db.Clients.Count()==0)
                    {
                        client.ID = 1; 
                    }
                    else
                    {
                        client.ID = db.Clients.Max(i => i.ID) + 1;
                    }
                  
                    db.Clients.Add(client);
                    newuser.UserCompany_Id = client.ID;

                }
                else
                {
                    newuser.UserCompany_Id = db.Clients.Where(u => u.companyName == txtCompanyName.Text).Select(u => u.ID).First();
                }
                db.User_Profile.Add(newuser);
                //Roles.AddUsersToRole(users, DEFAULT_ROLE);
                db.SaveChanges();
            }

            catch (Exception ex)
            {
                // Roll back the transaction. 
                Membership.DeleteUser(txtEmail.Text);
                retVa = ex.Message;
                FormsAuthentication.SignOut();
                // TODO: Delete just created user and user_in_role

            }
            //con.Close();
            return retVa;
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {


                try
                {

                    string userId = AddUser();
                    //Inactive User
                    MembershipUser usr = Membership.GetUser(txtEmail.Text);
                    usr.IsApproved = false;
                    Membership.UpdateUser(usr);
            
                   // FormsAuthentication.SetAuthCookie(txtEmail.Text, createPersistentCookie: false);

                    // Set Cookie


                    // Redirect user
                    string message = AddAdditionalUserProfileInfo_Transaction(userId);
                    if (message != null)
                    {
                        lblErrorMessage.Text = message;
                    }
                    else
                    {

                     
                        string emailtxt = "Dear " + txtFirstName.Text + ",\n\r To activate  your username click the link below: \n\r  " + HttpContext.Current.Request.Url.Scheme
                                    + "://"
                                    + HttpContext.Current.Request.Url.Authority
                                    + HttpContext.Current.Request.ApplicationPath + "Default.aspx/?ur=" + usr.ProviderUserKey.ToString() + " \n\r \n\r  Regards, \n\r \n\r ArciFrame";
                        Common.SendEmail("no-reply@arciframe.com.au", usr.Email, "ArciFrame Activation Link", emailtxt);
                        //Common.SendEmail("Itegture", usr.Email, "Activation link", emailtxt);
                        Response.Redirect(HttpContext.Current.Request.Url.Scheme
                                    + "://"
                                    + HttpContext.Current.Request.Url.Authority
                                    + HttpContext.Current.Request.ApplicationPath + "ShowMessage.aspx/?msg=1");
                    }
                }
                catch (Exception ex)
                {
                    lblErrorMessage.Text = ex.Message;
                }
            }
        }
    }
}