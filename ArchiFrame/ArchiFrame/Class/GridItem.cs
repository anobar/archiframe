﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArchiFrame.Class
{

    public class GridItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public string complexity { get; set; }
        public string description { get; set; }
        public int? quantity { get; set; }
        public int systemable { get; set; }
        public int sourceid { get; set; }
        public int targid { get; set; }
        public string sourcename { get; set; }
        public string targetname { get; set; }
        public bool IsNew { get; set; }
        public bool inScope { get; set; }
        public int?  projectTypeId { get; set; }
        public string projectTypeName { get; set; }
    }
}