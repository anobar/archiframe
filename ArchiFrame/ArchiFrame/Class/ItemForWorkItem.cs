﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArchiFrame.Class
{
    public class ItemForWorkItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public string complexity { get; set; }
        public string description { get; set; }
        public int? quantity { get; set; }
    }
}