﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArchiFrame.Class
{
    public class Project_Scope
    {
        public int ScopeId { get; set; }
        public string ScopeName { get; set; }
     public float rate{get;set;}
        public List<DeploymentEnvironment> ScopeDeploymentList { get; set; }
    }
}