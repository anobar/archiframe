﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ArchiFrame._Default" %>

<%--<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>.</h1>
                <h2>Modify this template to jump-start your ASP.NET application.</h2>
            </hgroup>
            <p>
                To learn more about ASP.NET, visit <a href="http://asp.net" title="ASP.NET Website">http://asp.net</a>.
                The page features <mark>videos, tutorials, and samples</mark> to help you get the most from ASP.NET.
                If you have any questions about ASP.NET visit
                <a href="http://forums.asp.net/18.aspx" title="ASP.NET Forum">our forums</a>.
            </p>
        </div>
    </section>
</asp:Content>--%>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

 <script  type="text/javascript">
     var currentValue = 0;
     function handleClick(myRadio) {
         alert('Old value: ' + currentValue);
         alert('New value: ' + myRadio.value);
         currentValue = myRadio.value;
     }
</script>
    
     <div id="leftnav">
               
                  <ul class="menu" >
		            <li class="item1"   ><a  href="/Default.aspx" class="active"> Home</a>
		
		            </li>
		            <li class="item2" >  <a href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				                                        
                             <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
                             
			            </ul>
		            </li>
                     
		            <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		              <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                      <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                      </ul>
		            </li>  
       <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  
	               </ul>
          </div>


           <div class="rightnav" >
                <div class="body_header_style">
              Welcome to ArciFrame!
            </div>

        <div class="Wholediv">
             <input type="text" size="2" id="stepper"/>
                 Please start defining your project by clicking the button below:
                     <br /> <br />
                <label  class="lblbtn"><a style="text-decoration:none;" href="../Estimator/ProjectGeneralInfo.aspx">Create or Modify Project</a></label>
     
                <br /> <br />
                <br /> <br />

                 If you have already created your project, you can define work-items and see estimate results:
                     <br /> <br />
                <label  class="lblbtn"><a style="text-decoration:none;" href="../Estimator/ProjectWorkItems.aspx">Work Items</a></label>
                <br /><br />
                <label  class="lblbtn"><a style="text-decoration:none;" href="../Estimator/EstimateResult.aspx">Estimate</a></label>

         </div>


     </div>
</asp:Content>
<asp:Content runat="server" ID="footerContent1" ContentPlaceHolderID="Footer">

    </asp:Content>
