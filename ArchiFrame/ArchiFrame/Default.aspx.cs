﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace ArchiFrame
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Class.Common.SendEmail("admin@integture.com.au", "anobar@yahoo.com", "a very nice test", "body body");
            //Class.Common.SendEmail();

            if (!IsPostBack)
            {
                if (Request.QueryString["ur"] != null)
                {
                    
                        Guid id = Guid.Parse(Request.QueryString["ur"]);
                        MembershipUser usr = Membership.GetUser(id);
                        usr.IsApproved = true;
                        Membership.UpdateUser(usr);
                        FormsAuthentication.SignOut();
                        string uri = HttpContext.Current.Request.Url.Scheme
                                    + "://";
                        uri += HttpContext.Current.Request.Url.Authority;
                        uri += HttpContext.Current.Request.ApplicationPath;
                        Response.Redirect(uri + "Account/login.aspx?msg=1");
                        

                 


                }
            } 
        }

       
    }
}