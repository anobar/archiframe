﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EstimateResult.aspx.cs" Inherits="ArchiFrame.Estimator.EstimateResult" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



    <link href="../StyleSheets/EstimateResultStyleSheet.css" rel="stylesheet" />
     <div id="leftnav">
                  <ul class="menu" >
		            <li class="item1"  ><a  href="/Default.aspx"> Home</a>
		
		            </li>
		            <li class="item2" >  <a  class="active" href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a class="selected" href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			        
                        </ul>
		            </li>
		            <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                                   <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                      </ul>
		            </li>  
		          <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  

	               </ul>
          </div>
       <div class="rightnav">
    
                 <div class="body_header_style">
                    Estimation Result
                     <div class="divexcel">
       
              <asp:ImageButton runat="server"  ID="ImageButton1"  OnClick="btnexcel_Click" Height="50px" ImageUrl="~/Images/excel.png" Width="50px"  />
              </div>
                 </div>
            <div class="EsDetail">
          <label class="lable">Select a project:</label><asp:DropDownList ID="DdlProject" runat="server"  OnSelectedIndexChanged="DdlProject_SelectedIndexChanged" class="drop_down_list_style" AutoPostBack="True"></asp:DropDownList><asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                </div>
           <div class="EsDetail">


               <asp:Label ID="lblTotalEstimateWithoutContingency"  CssClass="lblcss" runat="server" ></asp:Label>  <br />

               <br />
               <asp:Label ID="lblTotalEstimateDaysVal" runat="server" CssClass="lblcss"></asp:Label>  <br />
           </div>
          
              <div class="EsDetailGraph" >
                  
       <%--  <cc1:PieChart ID="PieChart1" runat="server"   ChartHeight="400" ChartWidth = "700"
        ChartType="Column" ChartTitleColor="#0E426C" Visible = "true">
        </cc1:PieChart>--%>
                  <br />
                    Estimate of effort by scope (with contingency):
                  <br /><br />
                       <asp:Chart ID="Chart1" runat="server" Height="361px" Width="697px"  >
                           <Series>
                               <asp:Series ChartArea="ChartArea1" Legend="Default" Name="Default">
                               </asp:Series>
                           </Series>
                           <ChartAreas>
                               <asp:ChartArea BorderWidth="0" Name="ChartArea1">
                               </asp:ChartArea>
                           </ChartAreas>
                           <Legends>
                               <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" LegendStyle="Row" Name="Default">
                               </asp:Legend>
                           </Legends>
                    <Titles>
                        <asp:Title ShadowOffset="3" Name="Items" />
                    </Titles>
                </asp:Chart>
                </div>
           <div class="EsDetail">
                <asp:TreeView ID="trvDetailedEstimate" runat="server" CssClass="tree_view_style" NodeStyle-CssClass="tree_view_style_node" RootNodeStyle-CssClass="tree_view_style_root"
                        ParentNodeStyle-CssClass="tree_view_style_parent" LeafNodeStyle-CssClass="tree_view_style_leaf" SelectedNodeStyle-CssClass="tree_view_style_selected" HoverNodeStyle-CssClass="tree_view_style_hover"  ></asp:TreeView>
           </div>
       <div class="EsDetail">       
        <table id="estimate_table" >
           
            <tr id="project_info">
                <td>
                    Project Name: &nbsp; <b><asp:Label ID="lblProjectName" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Project Type: &nbsp; <b><asp:Label ID="lblProjectType" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Project Overal Complexity: &nbsp; <b><asp:Label ID="lblProjectOveralComplexity" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Resource Skill Set: &nbsp; <b><asp:Label ID="lblResourceSkill" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Team Structure: &nbsp; <b><asp:Label ID="lblTeamStructure" runat="server" CssClass="label_style"></asp:Label></b>
                </td>
                <td>
                    Scope: &nbsp; <b><asp:Label ID="lblProjectScope" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Deployment Environments: &nbsp; <b><asp:Label ID="lblDeploymentEnvironments" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />

                </td>
            </tr>
        </table>
        
    </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="footerContent" ContentPlaceHolderID="Footer">
    </asp:Content>