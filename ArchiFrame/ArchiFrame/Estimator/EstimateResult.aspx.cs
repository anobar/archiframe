﻿using ArchiFrame.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Web.Security;

namespace ArchiFrame.Estimator
{
    public partial class EstimateResult : System.Web.UI.Page
    {

        string PROJECT_ID = string.Empty;
        private int p_id;
        private Project myproject;
        public float totalWorkItemsEffort = 0;
        public float totalWorkItemsEffortc = 0;
        string projectContingencyPercent;
        private ArchiFrameEntities db = new ArchiFrameEntities();
        private Guid id;
        Client client = new Client();
        List<Project_Scope> listscope = new List<Project_Scope>();

        float scopeFactor = 0;
        //static string CnnString = ConfigurationManager.ConnectionStrings["ArchiFrameConnectionString"].ConnectionString;

        //static string GET_ALL_REF_WORK_ITEM_TYPE_EFFORT_QUERY = "SELECT [id],[workItemTypeId], [workItemComplexityId], [isNew], [defaultEffort], [customEffort] FROM [dbo].[Ref_WorkItemTypeEffort]";
        //static string GET_ALL_WORK_ITEM_BY_PROJECT_ID_QUERY = "SELECT [id] ,[projectId], [typeId],[complexityId],[description],[quantity],[inScope],[isNew] FROM [dbo].[WorkItem] WHERE [projectId]=@projectId";
        //static string GET_ALL_PROJECT_SCOPE_BY_PROJECT_QUERY = "SELECT [id], [projectId], [projectScopeId] from [dbo].[ProjectScope] WHERE projectId = @projectId";
        //static string GET_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_QUERY = "SELECT [id],[projectId], [environmentId] FROM [DeploymentEnvironment] WHERE projectId=@projectId";
        //static string GET_ALL_REF_PROJECT_SCOPE_QUERY = "SELECT [id], [name] from [dbo].[Ref_ProjectScope]";
        //static string GET_PROJECT_BY_ID_QUERY = "SELECT * from [dbo].[Project] WHERE [id]=@projectId";

        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
            MembershipUser user = Membership.GetUser();
            string UserID = user.ProviderUserKey.ToString();
            id = Guid.Parse(UserID);
            client.ID = (from s in db.User_Profile
                         where s.UserId == id
                         select s.UserCompany_Id).First();


            if (!IsPostBack)
            {
                BindProjectDropDownList();
                try
                {

                    PROJECT_ID = Request.QueryString.Get("id");
                    p_id = Convert.ToInt32(PROJECT_ID);
                    if (string.IsNullOrEmpty(PROJECT_ID))
                    {
                        lblError.Text = "No project has been selected; please select a project from the list.";
                        return;
                    }
                    lblError.Text = "";
                    DdlProject.SelectedValue = PROJECT_ID;
                    myproject = (from s in db.Projects
                                 where s.id == p_id
                                 select s).First();
                    FillEstimate();
                    FillProjectInfo();





                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                }

            }


        }
        private void BindProjectDropDownList()
        {





            var queryddlProject = (from s in db.Projects
                                   where s.clientId == client.ID
                                   select new { s.id, s.name });
            List<itemFordpl> listItemddlProjects = new List<itemFordpl>();

            foreach (var x in queryddlProject)
            {
                itemFordpl li = new itemFordpl();
                li.ID = x.id;
                li.Name = x.name;
                listItemddlProjects.Add(li);
            }

            itemFordpl temp = new itemFordpl();
            temp.ID = -1;
            temp.Name = "[Select]";
            listItemddlProjects.Add(temp);
            DdlProject.DataSource = listItemddlProjects;
            DdlProject.DataTextField = "Name";
            DdlProject.DataValueField = "ID";
            DdlProject.DataBind();
            DdlProject.SelectedValue = "-1";





        }
        private void FillProjectInfo()
        {


            // Set project name
            lblProjectName.Text = myproject.name;


            lblProjectType.Text = db.Ref_ProjectType.Where(s => s.id == myproject.projectTypeId).Select(s => s.name).First().ToString();


            // Get project scope
            var scopelist = (from s in db.Ref_ProjectScope
                             join p in db.ProjectScopes on s.id equals p.projectScopeId
                             where p.projectId == p_id
                             select new { s.name }).ToList();
            string projectScopeList = string.Empty; ;
            foreach (var item in scopelist)
            {
                projectScopeList = projectScopeList + "<li>" + item.name + "</li>";
            }
            projectScopeList += "</ul>";
            lblProjectScope.Text = projectScopeList;

            // Get deloy env
            var deploylist = (from s in db.Ref_DeploymentEnvironment
                              join p in db.DeploymentEnvironments on s.id equals p.environmentId
                              where p.projectId == p_id
                              select new { s.name }).ToList();
            string projectEnvList = string.Empty; ;
            foreach (var item in deploylist)
            {
                projectEnvList = projectEnvList + "<li>" + item.name + "</li>";
            }

            projectEnvList += "</ul>";
            lblDeploymentEnvironments.Text = projectEnvList;

            // Set project overal complexity

            lblProjectOveralComplexity.Text = db.Ref_ProjectComplexity.Where(s => s.id == myproject.complexityId).Select(s => s.name).First();

            // Set project resource skill

            lblResourceSkill.Text = db.Ref_ResourcesSkillLevel.Where(s => s.id == myproject.resourcesSkillLevelId).Select(s => s.name).First();

            // Set project team structure

            lblTeamStructure.Text = db.Ref_TeamStructure.Where(s => s.id == myproject.teamStructureId).Select(s => s.name).First();

        }

        private void FillEstimate()
        {


            var queryWork = (from s in db.WorkItems
                             where s.projectId == p_id
                             select new { s.typeId, s.complexityId, s.quantity, s.isNew, s.inScope }).ToList();

            foreach (var item in queryWork)
            {
                var queryEfort = (from s in db.Ref_WorkItemTypeEffort
                                  where (s.workItemComplexityId == item.complexityId & s.workItemTypeId == item.typeId & s.isNew == item.isNew)
                                  select new { s.defaultEffort }).First();
                if (item.inScope == true)
                {

                    totalWorkItemsEffort += (float)item.quantity * (float)queryEfort.defaultEffort;
                }


            }



            // TODO: project scope is now calculated flat, need to make it more sophisticated by applying proper rules

            // Get Scope of current project

            var queryScope = (from s in db.ProjectScopes
                              where s.projectId == p_id
                              select new { s.projectId, s.projectScopeId }).ToList();







            // Scope Factor Constants:

            const float planning_factor = 5;
            const float requirement_gathering_and_analysis_factor = 10;
            const float architecture_and_design_factor = 15;
            const float build_factor = 20;
            const float test_factor = 15;
            float deployment_factor = 0; // deployment_factor will be updated below
            const float project_management_factor = 15;
            const float change_management_factor = 5;
            const float training_and_service_intro_factor = 5;


            foreach (var item in queryScope)
            {
                Project_Scope newscope = new Project_Scope();
                newscope.ScopeId = item.projectScopeId;
                switch (newscope.ScopeId.ToString())
                {
                    case "1":
                        // Planning and Scoping
                        newscope.ScopeName = "Planning and Scoping";
                        newscope.rate = 5;
                        scopeFactor += newscope.rate;
                        break;

                    case "2":
                        // // Requirement Gathering and Analysis
                        newscope.ScopeName = "Requirement Gathering and Analysis";
                        newscope.rate = 10;
                        scopeFactor += newscope.rate;
                        break;

                    case "3":
                        // Architecture and Design
                        newscope.ScopeName = "Architecture and Design";
                        newscope.rate = 15;
                        scopeFactor += newscope.rate;
                        break;

                    case "4": // Build
                        newscope.ScopeName = "Build";
                        newscope.rate = 20;
                        scopeFactor += newscope.rate;
                        break;
                    case "5": // Test
                        newscope.ScopeName = "Test";
                        newscope.rate = 15;
                        scopeFactor += newscope.rate;
                        break;
                    case "6": // Deployment
                        newscope.ScopeName = "Deployment";
                        newscope.rate = (float)(db.DeploymentEnvironments.Where(s => s.projectId == p_id).Count() * 2.5);
                        scopeFactor += newscope.rate;
                        break;
                    case "7": // Project Management Factor
                        newscope.ScopeName = "Project Management";
                        newscope.rate = 15;
                        scopeFactor += newscope.rate;
                        break;
                    case "8": // Change Management Factor
                        newscope.ScopeName = "Change Management";
                        newscope.rate = 5;
                        scopeFactor += newscope.rate;
                        break;
                    case "9": // Training and Service Introduction Factor
                        newscope.ScopeName = "Training and Service Introduction";
                        newscope.rate = 5;
                        scopeFactor += newscope.rate;
                        break;
                }
                listscope.Add(newscope);

            }


            // calculate the total factor in relation to other in-scope items


            //     totalWorkItemsEffort = (totalWorkItemsEffort * scopeFactor) / 100;

            // Get the details of the project

            string projectComplexity = myproject.complexityId.ToString();
            // TODO: read the ID from the table or cache

            if (projectComplexity == "1")  // 1=Simple
                totalWorkItemsEffort = (totalWorkItemsEffort * 75) / 100;  // bring the estimate 25% down
            else if (projectComplexity == "2") // 2=Medium = default
                totalWorkItemsEffort = totalWorkItemsEffort;  // Do nothing
            else if (projectComplexity == "3")
                totalWorkItemsEffort = (totalWorkItemsEffort * 125) / 100;  // push the estimate 25% up


            string projectTeamStructure = myproject.teamStructureId.ToString();
            // TODO: read the ID from the table or cache

            if (projectTeamStructure == "1")  // 1=Single Site (default)
                totalWorkItemsEffort = totalWorkItemsEffort;  // do nothing
            else if (projectTeamStructure == "2") // 2=Multi Site (All Onshore)
                totalWorkItemsEffort = (totalWorkItemsEffort * 120) / 100;  // push the estimate 20% up
            else if (projectTeamStructure == "3") // 3= Multi Site (Onshore / Offshore)
                totalWorkItemsEffort = (totalWorkItemsEffort * 135) / 100;  // push the estimate 35% up


            string projectResourceSkillLevel = myproject.resourcesSkillLevelId.ToString();
            // TODO: read the ID from the table or cache

            if (projectResourceSkillLevel == "1")  // 1=Need Upskilling
                totalWorkItemsEffort = (totalWorkItemsEffort * 125) / 100;  // push the estimate 25% up
            else if (projectResourceSkillLevel == "2") // 2=Average = default
                totalWorkItemsEffort = totalWorkItemsEffort;  // Do nothing
            else if (projectResourceSkillLevel == "3")  // 3=Highly Skilled
                totalWorkItemsEffort = (totalWorkItemsEffort * 75) / 100;  // pull the estimate 25% down

            // Applying socpe factor to totalWorkItemsEffort
            float estimateWithScopeFactorApplied = (totalWorkItemsEffort * scopeFactor) / 100;


            lblTotalEstimateWithoutContingency.Text = "Total Estimate (without&nbsp; contingency): " + Math.Round(estimateWithScopeFactorApplied, 1).ToString() + " &nbsp; Days"; // Total estimate without contingency
            Session["TotnoCo"] = Math.Round(estimateWithScopeFactorApplied, 1).ToString();
            projectContingencyPercent = myproject.contingencyPercent.ToString();
            Session["contingency"] = projectContingencyPercent;
            int perc = Convert.ToInt16(projectContingencyPercent) + 100;

            float estimateWithScopeFactorAndCongingencyApplied = (estimateWithScopeFactorApplied * perc) / 100;
            Session["TotwithCo"] = Math.Round(estimateWithScopeFactorAndCongingencyApplied, 1);
            lblTotalEstimateDaysVal.Text = "Total Estimate (with" + projectContingencyPercent + "%&nbsp; contingency): " + Math.Round(estimateWithScopeFactorAndCongingencyApplied, 1).ToString() + "&nbsp; Days"; // total estimate

            TreeNode nd = new TreeNode("Total (with contingency): " + Math.Round(estimateWithScopeFactorAndCongingencyApplied, 1).ToString());
            trvDetailedEstimate.Nodes.Add(nd);
            nd.SelectAction = TreeNodeSelectAction.Expand;

            // Breakdown of total estimate for each scope item
            string tempStr = string.Empty;
            int i = 0;
            string[] x = new string[listscope.Count()];
            float[] y = new float[listscope.Count()];
            Session["Scope"] = listscope;
            foreach (var scopeItem in listscope)
            {
                float breakdownEstimate = (scopeItem.rate * estimateWithScopeFactorAndCongingencyApplied) / scopeFactor;
                Session["scopeFactor"] = scopeFactor;
                //   tempStr += scopeItem.Key + ": " + breakdownEstimate.ToString() + "\r\n";
                nd = new TreeNode(scopeItem.ScopeName + ": " + Math.Round(breakdownEstimate, 1).ToString() + " days \r\n");
                trvDetailedEstimate.Nodes[0].ChildNodes.Add(nd);
                nd.SelectAction = TreeNodeSelectAction.None;
                y[i] = (scopeItem.rate / scopeFactor) * 360;
                x[i] = scopeItem.ScopeName + "\n\r  %" + Math.Round(y[i] / 3.6, 2).ToString() + " (" + Math.Round(breakdownEstimate, 1).ToString() + "d)";
                i += 1;
            }

            Chart1.Visible = true;
            Chart1.Series[0].Points.DataBindXY(x, y);
            Chart1.Series[0].ChartType = SeriesChartType.Pie;
            Chart1.Series[0]["PieLabelStyle"] = "Outside";
            Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            Chart1.Legends[0].Enabled = false;


        }

        protected void Chart1_Click(object sender, ImageMapEventArgs e)
        {

        }

        protected void DdlProject_SelectedIndexChanged(object sender, EventArgs e)
        {

            PROJECT_ID = DdlProject.SelectedValue;

            {
                if (PROJECT_ID == "-1")
                {
                    Response.Redirect(Request.Url.AbsoluteUri);

                }
                else
                {

                    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path) + "?id=" + PROJECT_ID);
                }
            }
        }

        protected void btnexcel_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Estimator/ExportToExcel.aspx");

            //if (Session["TotnoCo"] != null)
            //{


            //    string strFilename = "Estimate.xls";
            //    string attachment = "attachment; filename=" + strFilename;
            //    Response.ClearContent();
            //    Response.AddHeader("content-disposition", attachment);
            //    Response.ContentType = "application/vnd.ms-excel";

            //    string strenv = string.Empty;
            //    Response.Write("Total Estimate (without contingency):");
            //    Response.Write("\t" + Session["TotnoCo"].ToString() + " Days");
            //    Response.Write("\n");
            //    Response.Write("Total Estimate (with " + Session["contingency"].ToString() + "% contingency):");
            //    Response.Write("\t" + Session["TotwithCo"] + " Days");
            //    Response.Write("\n");
            //    Response.Write("\n");

            //    List<Project_Scope> listscopeex = (List<Project_Scope>)Session["Scope"];
            //    foreach (var scopeItem in listscopeex)
            //    {

            //        double x = Convert.ToDouble(Session["TotwithCo"]);
            //        Response.Write(scopeItem.ScopeName + ": \t" + (Math.Round((scopeItem.rate * x / (float)Session["scopeFactor"]), 1).ToString() + "\n"));

            //    }
            //    Response.Write("Total: \t" + Session["TotwithCo"]);


            //    Response.End();
            //}
        }


    }

}
