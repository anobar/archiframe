﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExportToExcel.aspx.cs" Inherits="ArchiFrame.Estimator.ExportToExcel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="leftnav">
               
                  <ul class="menu" >
		            <li class="item1" ><a  href="/Default.aspx"> Home</a>
		
		            </li>
		            <li class="item2"  >  <a  href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a  href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			           
                             </ul>
		            </li>
		          <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a  class="active" href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a   href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                 <li class="subitem1" ><a  class="selected" href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>

                      </ul>
		            </li>  
		          <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  

	               </ul>
          </div>

     <div class="rightnav">
     <div class="reg">
                 <div class="body_header_style">
                      Report - Export Project To Excel
                 </div>
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                     <ContentTemplate>
     <div class="Wholediv">
                 Select project :

                         <asp:DropDownList ID="ddlProjects" runat="server" AutoPostBack="true" class="drop_down_list_style" OnSelectedIndexChanged="ddlProjects_SelectedIndexChanged" >
                         </asp:DropDownList>
                
   
      <asp:CustomValidator id="CustomValidatorddlProject" runat="server" 
                      OnServerValidate="DdlValidator" 
                      ControlToValidate="ddlProjects" 
                      ErrorMessage="Please Select a Project."
                     CssClass="field_validation_error">
                    </asp:CustomValidator>
         </div>
      <div class="leftdiv">
               Items to be exported :
                 <asp:CheckBoxList ID="ChkProjectType" runat="server" >
                     <asp:ListItem Value="1" Selected="True">General Information</asp:ListItem>
                     <asp:ListItem Value="2" Selected="True">Work Items</asp:ListItem>
                     <asp:ListItem Value="3" Selected="True">Estimate Result</asp:ListItem>
               </asp:CheckBoxList>
             </div>
       
      
      </div>   
                      
              </ContentTemplate>
                 </asp:UpdatePanel>
               <div class="Wholediv">
             <asp:Button ID="btnsubmit" runat="server"  Text="Export" CssClass="cssbutton" OnClick="btnsubmit_Click"/>
                                </div>
         </div>
         </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>
