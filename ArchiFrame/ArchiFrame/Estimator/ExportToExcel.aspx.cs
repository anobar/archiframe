﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using ArchiFrame.Class;
namespace ArchiFrame.Estimator
{
    public partial class ExportToExcel : System.Web.UI.Page
    {
        Project myproject;
        float scopeFactor = 0;
        public float totalWorkItemsEffort = 0;
        private ArchiFrameEntities db = new ArchiFrameEntities();
        private Guid id;
        Client client = new Client();
        List<Project_Scope> listscope = new List<Project_Scope>();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {


                //this.BindWorkItemGridView();
                MembershipUser user = Membership.GetUser();
                string UserID = user.ProviderUserKey.ToString();
                id = Guid.Parse(UserID);
                client.ID = (from s in db.User_Profile
                             where s.UserId == id
                             select s.UserCompany_Id).First();
                Session["id"] = client.ID;
                this.BindProjectDropDownList();

            }
            else
            {
             
            }
        }
        protected void DdlValidator(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (args.Value != "-1");
        }
        private void BindProjectDropDownList()
        {
            var queryddlProject = (from s in db.Projects
                                   where s.clientId == client.ID
                                   select new { s.id, s.name });
            List<itemFordpl> listItemddlProjects = new List<itemFordpl>();

            foreach (var x in queryddlProject)
            {
                itemFordpl li = new itemFordpl();
                li.ID = x.id;
                li.Name = x.name;
                listItemddlProjects.Add(li);
            }

            itemFordpl temp = new itemFordpl();
            temp.ID = -1;
            temp.Name = "[Select]";
            listItemddlProjects.Add(temp);
            ddlProjects.DataSource = listItemddlProjects;
            ddlProjects.DataTextField = "Name";
            ddlProjects.DataValueField = "ID";
            ddlProjects.DataBind();
            ddlProjects.SelectedValue = "-1";



        }
        protected void btnsubmit_Click(object sender, EventArgs e)
        {

        }

        protected void ddlProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
     
            if (ddlProjects.SelectedValue != "-1")
            {
                //CustomValidatorddlProject. = string.Empty;
                int p_id = Convert.ToInt32(ddlProjects.SelectedValue);
                myproject = (from s in db.Projects
                             where s.id == p_id
                             select s).First();
                string strFilename = myproject.name + ".xls";
                string attachment = "attachment; filename=" + strFilename;
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/vnd.ms-excel";

                string strenv = string.Empty;

                foreach (ListItem ItemType in ChkProjectType.Items)
                {

                    if (ItemType.Value=="1" & ItemType.Selected)
                    {
                        Response.Write("Project General Information");
                        Response.Write("\n");
                        Response.Write(" Project Name: ");
                        Response.Write("\t" + myproject.name);
                        Response.Write("\n");
                        Response.Write("Project Type: ");
                        Response.Write("\t" + db.Ref_ProjectType.Where(s => s.id == myproject.projectTypeId).Select(s => s.name).First());
                        Response.Write("\n");
                        Response.Write("Project Overal Complexity:");
                        Response.Write("\t" + db.Ref_ProjectComplexity.Where(s => s.id == myproject.complexityId).Select(s => s.name).First());
                        Response.Write("\n");
                        Response.Write("Project Contingency:");
                        Response.Write("\t" + myproject.contingencyPercent);
                        Response.Write("\n");
                        Response.Write("Resource Skill Set: ");
                        Response.Write("\t" + db.Ref_ResourcesSkillLevel.Where(s => s.id == myproject.resourcesSkillLevelId).Select(s => s.name).First());
                        Response.Write("\n");
                        Response.Write("Team Structure:");
                        Response.Write("\t" + db.Ref_TeamStructure.Where(s => s.id == myproject.teamStructureId).Select(s => s.name).First());
                        Response.Write("\n");
                        Response.Write("\n");
                        Response.Write("Project Scope");
                        Response.Write("\n");

                        var query = (from s in db.ProjectScopes
                                     join p in db.Ref_ProjectScope on s.projectScopeId equals p.id
                                     where s.projectId == myproject.id
                                     select new { p.name, s.projectId }).ToList();
                        foreach (var item in query)
                        {
                            if (item.name != "Deployment")
                            {
                                Response.Write("\t" + item.name);
                                Response.Write("\n");
                            }
                            if (item.name == "Deployment")
                            {
                                var queryDeploy = (from s in db.DeploymentEnvironments
                                                   join p in db.Ref_DeploymentEnvironment on s.environmentId equals p.id
                                                   where s.projectId == item.projectId
                                                   select new { p.name }).ToList();
                                strenv = "\t Deployment \n Deployment Environments \n";

                                foreach (var itemenv in queryDeploy)
                                {
                                    strenv += "\t " + itemenv.name + "\n";
                                }

                            }
                        }
                        if (!String.IsNullOrEmpty(strenv))
                        {
                            Response.Write(strenv);
                        }
                        Response.Write("\n");
                        Response.Write("\n");
                    }

                    if (ItemType.Value == "2" & ItemType.Selected)
                    {
                        Response.Write("Project Work items");
                        Response.Write("\n");
                        var queryitems = (from s in db.WorkItems
                                          join p in db.Ref_WorkItemType on s.typeId equals p.id
                                          join q in db.WorkItemComplexities on s.complexityId equals q.id
                                          where s.projectId == myproject.id
                                          select new { p.name, s.id, q.complexity, s.description, s.quantity, p.systemable, s.isNew, s.inScope }).ToList();


                        Response.Write("Type");

                        Response.Write("\t");
                        Response.Write("Complexity");

                        Response.Write("\t");
                        Response.Write("Description");

                        Response.Write("\t");
                        Response.Write("Quantity");

                        Response.Write("\t");
                        Response.Write("New ?");

                        Response.Write("\t");
                        Response.Write("In Scope ?");

                        Response.Write("\t");
                        Response.Write("Source System");

                        Response.Write("\t");
                        Response.Write("Target  System");
                        Response.Write("\n");
                        foreach (var item in queryitems)
                        {

                            Response.Write(item.name);

                            Response.Write("\t" + item.complexity);

                            Response.Write("\t" + item.description);

                            Response.Write("\t" + item.quantity);

                            Response.Write("\t" + item.isNew);

                            Response.Write("\t" + item.inScope);

                            int s_id = db.workitemsystems.Where(s => s.workitemid == item.id).Select(s => s.systemsourceid).First();
                            Response.Write("\t" + db.Systemees.Where(s => s.id == s_id).Select(s => s.name).First());

                            int t_id = db.workitemsystems.Where(s => s.workitemid == item.id).Select(s => s.systemtargetid).First();
                            if (t_id != -1)
                            {
                                Response.Write("\t" + db.Systemees.Where(s => s.id == t_id).Select(s => s.name).First());

                            }

                            Response.Write("\n");



                        }
                    }

                    if (ItemType.Value == "3" & ItemType.Selected)
                    {
                        Response.Write("\n");
                        Response.Write("\n");
                        Response.Write("\n");
                        var queryWork = (from s in db.WorkItems
                                         where s.projectId == p_id
                                         select new { s.typeId, s.complexityId, s.quantity, s.isNew, s.inScope }).ToList();

                        foreach (var item in queryWork)
                        {
                            var queryEfort = (from s in db.Ref_WorkItemTypeEffort
                                              where (s.workItemComplexityId == item.complexityId & s.workItemTypeId == item.typeId & s.isNew == item.isNew)
                                              select new { s.defaultEffort }).First();
                            if (item.inScope == true)
                            {

                                totalWorkItemsEffort += (float)item.quantity * (float)queryEfort.defaultEffort;
                            }


                        }

                        var queryScope = (from s in db.ProjectScopes
                                          where s.projectId == p_id
                                          select new { s.projectId, s.projectScopeId }).ToList();







                        // Scope Factor Constants:

                        const float planning_factor = 5;
                        const float requirement_gathering_and_analysis_factor = 10;
                        const float architecture_and_design_factor = 15;
                        const float build_factor = 20;
                        const float test_factor = 15;
                        float deployment_factor = 0; // deployment_factor will be updated below
                        const float project_management_factor = 15;
                        const float change_management_factor = 5;
                        const float training_and_service_intro_factor = 5;


                        foreach (var item in queryScope)
                        {
                            Project_Scope newscope = new Project_Scope();
                            newscope.ScopeId = item.projectScopeId;
                            switch (newscope.ScopeId.ToString())
                            {
                                case "1":
                                    // Planning and Scoping
                                    newscope.ScopeName = "Planning and Scoping";
                                    newscope.rate = 5;
                                    scopeFactor += newscope.rate;
                                    break;

                                case "2":
                                    // // Requirement Gathering and Analysis
                                    newscope.ScopeName = "Requirement Gathering and Analysis";
                                    newscope.rate = 10;
                                    scopeFactor += newscope.rate;
                                    break;

                                case "3":
                                    // Architecture and Design
                                    newscope.ScopeName = "Architecture and Design";
                                    newscope.rate = 15;
                                    scopeFactor += newscope.rate;
                                    break;

                                case "4": // Build
                                    newscope.ScopeName = "Build";
                                    newscope.rate = 20;
                                    scopeFactor += newscope.rate;
                                    break;
                                case "5": // Test
                                    newscope.ScopeName = "Test";
                                    newscope.rate = 15;
                                    scopeFactor += newscope.rate;
                                    break;
                                case "6": // Deployment
                                    newscope.ScopeName = "Deployment Factors";
                                    newscope.rate = (float)(db.DeploymentEnvironments.Where(s => s.projectId == p_id).Count() * 2.5);
                                    scopeFactor += newscope.rate;
                                    break;
                                case "7": // Project Management Factor
                                    newscope.ScopeName = "Project Management Factor";
                                    newscope.rate = 15;
                                    scopeFactor += newscope.rate;
                                    break;
                                case "8": // Change Management Factor
                                    newscope.ScopeName = "Change Management Factor";
                                    newscope.rate = 5;
                                    scopeFactor += newscope.rate;
                                    break;
                                case "9": // Training and Service Introduction Factor
                                    newscope.ScopeName = "Training and Service Introduction Factor";
                                    newscope.rate = 5;
                                    scopeFactor += newscope.rate;
                                    break;
                            }
                            listscope.Add(newscope);

                        }

                        string projectComplexity = myproject.complexityId.ToString();
                        // TODO: read the ID from the table or cache

                        if (projectComplexity == "1")  // 1=Simple
                            totalWorkItemsEffort = (totalWorkItemsEffort * 75) / 100;  // bring the estimate 25% down
                        else if (projectComplexity == "2") // 2=Medium = default
                            totalWorkItemsEffort = totalWorkItemsEffort;  // Do nothing
                        else if (projectComplexity == "3")
                            totalWorkItemsEffort = (totalWorkItemsEffort * 125) / 100;  // push the estimate 25% up


                        string projectTeamStructure = myproject.teamStructureId.ToString();
                        // TODO: read the ID from the table or cache

                        if (projectTeamStructure == "1")  // 1=Single Site (default)
                            totalWorkItemsEffort = totalWorkItemsEffort;  // do nothing
                        else if (projectTeamStructure == "2") // 2=Multi Site (All Onshore)
                            totalWorkItemsEffort = (totalWorkItemsEffort * 120) / 100;  // push the estimate 20% up
                        else if (projectTeamStructure == "3") // 3= Multi Site (Onshore / Offshore)
                            totalWorkItemsEffort = (totalWorkItemsEffort * 135) / 100;  // push the estimate 35% up


                        string projectResourceSkillLevel = myproject.resourcesSkillLevelId.ToString();
                        // TODO: read the ID from the table or cache

                        if (projectResourceSkillLevel == "1")  // 1=Need Upskilling
                            totalWorkItemsEffort = (totalWorkItemsEffort * 125) / 100;  // push the estimate 25% up
                        else if (projectResourceSkillLevel == "2") // 2=Average = default
                            totalWorkItemsEffort = totalWorkItemsEffort;  // Do nothing
                        else if (projectResourceSkillLevel == "3")  // 3=Highly Skilled
                            totalWorkItemsEffort = (totalWorkItemsEffort * 75) / 100;  // pull the estimate 25% down

                        //lblTotalEstimateWithoutContingency.Text = "Total Estimate (without&nbsp;&nbsp; contingency):" + Math.Round(totalWorkItemsEffort, 1).ToString() + " &nbsp; Days"; // Total estimate without contingency

                        Session["TotnoCo"] = Math.Round(totalWorkItemsEffort, 1).ToString();

                        int perc = Convert.ToInt16(myproject.contingencyPercent.ToString()) + 100;
                        totalWorkItemsEffort = (totalWorkItemsEffort * perc) / 100;
                        Session["TotwithCo"] = Math.Round(totalWorkItemsEffort, 1);
                        Response.Write("Total Estimate (without contingency):");
                        Response.Write("\t" + Session["TotnoCo"].ToString() + " Days");
                        Response.Write("\n");
                        Response.Write("Total Estimate (with " + myproject.contingencyPercent.ToString() + "% contingency):");
                        Response.Write("\t" + Math.Round(totalWorkItemsEffort, 1) + " Days");
                        Response.Write("\n");
                        Response.Write("\n");


                        foreach (var scopeItem in listscope)
                        {


                            Response.Write(scopeItem.ScopeName + ": \t" + (Math.Round((scopeItem.rate * Math.Round(totalWorkItemsEffort, 1) / scopeFactor), 1).ToString() + "\n"));

                        }
                        Response.Write("Total: \t" + Math.Round(totalWorkItemsEffort, 1));
                    }
                }
              

                          
          
                Response.End();
            }
        }
    }
}
