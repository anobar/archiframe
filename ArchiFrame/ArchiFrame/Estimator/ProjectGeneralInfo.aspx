﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectGeneralInfo.aspx.cs" Inherits="ArchiFrame.Estimator.ProjectGeneralInfo" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentHeader" runat="server">
     <link href="../StyleSheets/ProjectGeneralInfo.css" rel="stylesheet" />
    <script type='text/javascript'>

        function GetType() {

            alert("fg");

            PageMethods.FillType();

        }

    

    </script>
</asp:Content>


<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="server">
     <div id="leftnav">
              <ul class="menu" >
		            <li class="item1" ><a  href="/Default.aspx"> Home</a>
		
		            </li>
		            <li class="item2"  >  <a class="active" href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a class="selected" href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a  href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			     
                                </ul>
		            </li>
		            <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                                   <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                      </ul>
		            </li>  
		         <li class="item5"  ><a href="/Estimator/Setting.aspx">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  
	               </ul>

	             
          </div>
    <div class="rightnav">
    
          <div class="reg">
       <div class="body_header_style">
        Project Information
      <%-- <div class="divexcel">
       
           <asp:ImageButton runat="server"  ID="btnexcel"  OnClick="btnexcel_Click" Height="50px" ImageUrl="~/Images/excel.png" Width="50px"  />
         </div>--%>
            
                     </div>
              <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                     <ContentTemplate>
    <asp:Wizard ID="EstimatorWizard"  runat="server" ActiveStepIndex="0" Height="330px" Width="700px" OnFinishButtonClick="EstimatorWizard_FinishButtonClick" DisplaySideBar="False" OnNextButtonClick="EstimatorWizard_NextButtonClick" >
  
         <NavigationButtonStyle CssClass="btnholder" />
         <StartNextButtonStyle CssClass="cssbutton" />
         <StepNextButtonStyle  CssClass="cssbutton"  />
         <StepPreviousButtonStyle CssClass="cssbutton1" />
        <FinishCompleteButtonStyle  CssClass="cssbutton" />
        <FinishPreviousButtonStyle CssClass="cssbutton1" />
      
         <SideBarStyle BorderColor="Black" />
      
         <WizardSteps>
  
        
             
            <asp:WizardStep ID="WizardStep1" runat="server"  title="General">
              
          
                
                <br/> 
                   
              
                  Select a project from the list
                  <asp:DropDownList ID="ddlProjects" runat="server" AutoPostBack="True" class="drop_down_list_style" OnSelectedIndexChanged="ddlProjects_SelectedIndexChanged" ViewStateMode="Enabled">
                </asp:DropDownList>Or
                <asp:HyperLink runat="server" ID="AddnewProject" ViewStateMode="Disabled" CssClass="underline" EnableTheming="False">Add New Project</asp:HyperLink>
                     
                
                <br />
                   <br />
                Project Name<br />
                   <br />
                <asp:TextBox ID="txtProjectName" runat="server" CssClass="textbox" ViewStateMode="Enabled"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProjectName" CssClass="field_validation_error" ErrorMessage="&lt;img src='../images/error.png' /&gt;"></asp:RequiredFieldValidator>
                <asp:Label ID="lblerr" runat="server" CssClass="field_validation_error"></asp:Label>
                <br />
                   <br />
              
                   <br />
             <%-- <asp:DropDownList ID="cbo" runat="server" TabIndex="2" CssClass="chzn-select" Width="313px" ViewStateMode="Enabled" AutoPostBack="True" OnSelectedIndexChanged="cbo_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;<asp:CustomValidator ID="CustomValidatorddlProjectType" runat="server" ControlToValidate="cbo" CssClass="field_validation_error" ErrorMessage="Please Select a Type." OnServerValidate="DdlValidator"></asp:CustomValidator>
                <br />
               --%>
               </div>
             <div class="regleftscope">
                   Project Type
                <br />
                <asp:CheckBoxList ID="CheckListTypes" runat="server">
                </asp:CheckBoxList>
                      </div>
                <div class="regerr" >
                    <asp:Label ID="lblTypeErr" runat="server" Visible="False" CssClass="field_validation_error">></asp:Label>
                    </div>
                </asp:WizardStep>

            <asp:WizardStep ID="WizardStep2" runat="server" title="Scope">
                 <div class="regleftscope">
                Specify the scope of the project:<br /><asp:CheckBoxList ID="chkListProjectScope" runat="server" AutoPostBack="true" Class="check_box_list_style" OnSelectedIndexChanged="chkListProjectScope_SelectedIndexChanged">
                </asp:CheckBoxList>
                <br />
                     </div>
                <div class="regerr" >
                    <asp:Label runat="server" ID="lblScopeErr" Visible="false"  CssClass="field_validation_error"></asp:Label>
                      </div>
                <div class="regrightscope" >
                    <asp:Label ID="lblEnv" runat="server"   Text="Specify  environment(s) you will be deploying the project to:" Visible="False"></asp:Label>
                <br />
                <asp:CheckBoxList ID="chkListEnvironments" runat="server"  Class="check_box_list_style" Visible="False">
                </asp:CheckBoxList>
                    </div>
            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep3" runat="server" Title="Assumptions" >
                Contingency (%)<br />
                <asp:TextBox ID="txtContingency" runat="server" Width="145px" TextMode="Number"  CssClass="textbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtContingency" CssClass="field_validation_error" ErrorMessage="The Project Contingency is required" />

                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                ControlToValidate="txtContingency"
                ErrorMessage="Please enter a number between 0-99."
                ValidationExpression="^\d{1,2}$"
                    CssClass="field_validation_error" >

                </asp:RegularExpressionValidator>
                <br />
                Project Overal Complexity<br />
                <asp:DropDownList ID="ddlProjectComplexity" runat="server" class="drop_down_list_style">
                </asp:DropDownList>
                <asp:CustomValidator id="CustomValidatorddlProjectComplexity" runat="server" 
                      OnServerValidate="DdlValidator" 
                      ControlToValidate="ddlProjectComplexity" 
                      ErrorMessage="Please Select a complexity for project."
                    CssClass="field_validation_error">

                    </asp:CustomValidator>
          

                <br />
                <br />
                Resources Skills<br />
                <asp:DropDownList ID="ddlResourcesSkills" runat="server" class="drop_down_list_style">
                </asp:DropDownList>
                   <asp:CustomValidator id="CustomValidatorddlResourcesSkills" runat="server" 
                      OnServerValidate="DdlValidator" 
                      ControlToValidate="ddlResourcesSkills" 
                      ErrorMessage="Please Select a source skill for project."
                       CssClass="field_validation_error">
                    </asp:CustomValidator>
                <br />
                <br />
                Team Structure<br />
                <asp:RadioButtonList ID="rdoListTeamStructure" runat="server" Class="check_box_list_style">
                </asp:RadioButtonList>
<%--                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="rdoListTeamStructure" CssClass="field_validation_error" 
                    ErrorMessage="Please select team structure" />--%>


            </asp:WizardStep>
           
        </WizardSteps>
           <HeaderTemplate>
               <ul id="wizHeader">
                   <asp:Repeater ID="SideBarList" runat="server">
                       <ItemTemplate>
                           <li><a class="<%# GetClassForWizardStep(Container.DataItem) %>" title="<%#Eval("Name")%>">
                               <%# Eval("Name")%></a> </li>
                       </ItemTemplate>
                   </asp:Repeater>
                  
               </ul>
           </HeaderTemplate>
       <NavigationStyle CssClass="btnholder" />
    </asp:Wizard>
                              </ContentTemplate>
                 </asp:UpdatePanel>
</div>
    </div>

</asp:Content>

<asp:Content runat="server" ID="footerContent" ContentPlaceHolderID="Footer">
    </asp:Content>
