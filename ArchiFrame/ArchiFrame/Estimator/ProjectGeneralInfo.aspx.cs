﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using ArchiFrame.Class;
using System.Web.Security;
namespace ArchiFrame.Estimator
{

    //static string GetAllWorkItemComplexityQuery = "";

     

    public partial class ProjectGeneralInfo : System.Web.UI.Page
    {
      
        public enum PageMode
        {
            Insert,
            Update
        }

        private ArchiFrameEntities db = new ArchiFrameEntities();
        private Guid id;
        private Project Myproject;
        private List<ProjectScope> MyProjectScope = new List<ProjectScope>();
        private List<DeploymentEnvironment> MyProjectEnvironment = new List<DeploymentEnvironment>();
        private List<ProjectType> ProjectTypes = new List<ProjectType>();
        Client client = new Client();
        protected void Page_Load(object sender, EventArgs e)
        {
            AddnewProject.NavigateUrl = Request.RawUrl;
            MembershipUser user = Membership.GetUser();
            string UserID = user.ProviderUserKey.ToString();
            id = Guid.Parse(UserID);
            client.ID = (from s in db.User_Profile
                         where s.UserId == id
                         select s.UserCompany_Id).First();
           

            if (!IsPostBack)
            {
                Session["mode"] = "insert";
                InitiatePage();
                EstimatorWizard.ActiveStepIndex = 0; // go to the first step of the wizard

             
            }
            else
            {
                Myproject = (Project)Session["Project"];
                          
            }

            EstimatorWizard.PreRender += new EventHandler(Wizard1_PreRender); 
        }
        protected void DdlValidator(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (args.Value != "-1");
        }
        private void InitiatePage()
        {
            txtProjectName.Text = string.Empty;
            txtContingency.Text = string.Empty;
            this.BindGeneralInfoPage();  
         
       
          
        }

        private void BindGeneralInfoPage()
        {

            Client client = new Client();
            client.ID = (from s in db.User_Profile
                        where s.UserId == id
                        select s.UserCompany_Id).First();
            Session["id"] = client.ID;
            //add data to ddlprojects dropdown 
            var queryddlProjects = (from s in db.Projects
                                     where s.clientId == client.ID
                                     select new { s.id, s.name });
            List<itemFordpl> listItemddlProjects = new List<itemFordpl>();

            foreach (var x in queryddlProjects)
            {
                itemFordpl li=new itemFordpl();
                li.ID=x.id;
                li.Name=x.name;
                listItemddlProjects.Add(li);
            }

            itemFordpl temp = new itemFordpl();
            temp.ID = -1;
            temp.Name = "[Select]";
            listItemddlProjects.Add(temp);
            ddlProjects.DataSource = listItemddlProjects;
            ddlProjects.DataTextField = "Name";
            ddlProjects.DataValueField = "ID";
            ddlProjects.DataBind();
            ddlProjects.SelectedValue = "-1";
         
            //add data to ddlproject type


          CheckListTypes.DataSource = (from s in db.Ref_ProjectType
                                         where s.ClienId == client.ID | s.IsSystemTemplate == true
                                         select new { s.id, s.name }).ToList();
          CheckListTypes.DataTextField = "name";
          CheckListTypes.DataValueField = "id";
          CheckListTypes.DataBind();
          CheckListTypes.ClearSelection();

            //get list of  scopes
            chkListProjectScope.DataSource = (from s in db.Ref_ProjectScope
                                              select s).ToList();
            chkListProjectScope.DataTextField = "name";
            chkListProjectScope.DataValueField = "id";
            chkListProjectScope.DataBind();
            chkListProjectScope.ClearSelection();



            //get list of environments

            chkListEnvironments.DataSource = (from s in db.Ref_DeploymentEnvironment
                                              select s).ToList();
            chkListEnvironments.DataTextField = "name";
            chkListEnvironments.DataValueField = "id";
            chkListEnvironments.DataBind();
            chkListEnvironments.ClearSelection();

            lblEnv.Visible = false;
            chkListEnvironments.Visible = false;
            // get list of complexity

            List<itemFordpl> listProjectComplexity = new List<itemFordpl>();
            var queryProjectComplexity = (from s in db.Ref_ProjectComplexity
                                       select new { s.id, s.name });
            foreach (var x in queryProjectComplexity)
            {
                itemFordpl li = new itemFordpl();
                li.ID = x.id;
                li.Name = x.name;
                listProjectComplexity.Add(li);
            }

            ddlProjectComplexity.DataSource = listProjectComplexity;
            ddlProjectComplexity.DataTextField = "Name";
            ddlProjectComplexity.DataValueField = "ID";
            ddlProjectComplexity.DataBind();
            ddlProjectComplexity.Items.Insert(0, new ListItem("[select]", "-1"));
            ddlProjectComplexity.SelectedIndex = 0;
            // get list of Resources Skill Level

            List<itemFordpl> listResourcesSkillLevel = new List<itemFordpl>();
            var queryResourcesSkillLevel = (from s in db.Ref_ResourcesSkillLevel
                                          select new { s.id, s.name });
            foreach (var x in queryResourcesSkillLevel)
            {
                itemFordpl li = new itemFordpl();
                li.ID = x.id;
                li.Name = x.name;
                listResourcesSkillLevel.Add(li);
            }

            ddlResourcesSkills.DataSource = listResourcesSkillLevel;
            ddlResourcesSkills.DataTextField = "Name";
            ddlResourcesSkills.DataValueField = "ID";
            ddlResourcesSkills.DataBind();
            ddlResourcesSkills.Items.Insert(0, new ListItem("[select]", "-1"));
            ddlResourcesSkills.SelectedIndex = 0;


            // get list of Team Structure
            List<itemFordpl> listTeamStructure = new List<itemFordpl>();
            var querylistTeamStructure= (from s in db.Ref_TeamStructure
                                          select new { s.id, s.name });
            foreach (var x in querylistTeamStructure)
            {
                itemFordpl li = new itemFordpl();
                li.ID = x.id;
                li.Name = x.name;
                listTeamStructure.Add(li);
            }

            rdoListTeamStructure.DataSource = listTeamStructure;
            rdoListTeamStructure.DataTextField = "Name";
            rdoListTeamStructure.DataValueField = "ID";
            rdoListTeamStructure.DataBind();
            rdoListTeamStructure.SelectedIndex = 0;
         
        }

        private void BindScopePage()
        {
            //get list of project scopes
            MyProjectScope = (from s in db.ProjectScopes
                             where s.projectId == Myproject.id
                             select s).ToList();


            if (MyProjectScope.Where(s => s.projectScopeId == 6).Count() != 0)
            {
                lblEnv.Visible = true;
                chkListEnvironments.Visible = true;
            }

            foreach (ListItem  item in chkListProjectScope.Items )
            {
        
                if (MyProjectScope.Where(s =>s.projectScopeId.ToString() ==item.Value).Count() !=0)
                {
                   
                    item.Selected = true;
                }
              
            }
            //get list of project Environment

            MyProjectEnvironment = (from s in db.DeploymentEnvironments
                                    where s.projectId == Myproject.id
                                    select s).ToList();
         

            foreach (ListItem item in chkListEnvironments.Items)
            {

                if (MyProjectEnvironment.Where(s => s.environmentId.ToString() == item.Value).Count() != 0)
                {
                    item.Selected = true;
                }
              
            }



          
        }

        private void BindAssumptionPage()
        {
            txtContingency.Text = Myproject.contingencyPercent.ToString();
            ddlProjectComplexity.SelectedValue = (db.Projects.Where(s => s.id == Myproject.id).Select(s => s.complexityId).First()).ToString();
            ddlResourcesSkills.SelectedValue = (db.Projects.Where(s => s.id == Myproject.id).Select(s => s.resourcesSkillLevelId).First()).ToString();
            rdoListTeamStructure.SelectedValue = (db.Projects.Where(s => s.id == Myproject.id).Select(s => s.teamStructureId).First()).ToString();
         
        }

     
        protected void EstimatorWizard_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (Page.IsValid)
            {        
            

            try
            {
                Project prj; 
                if ( Session["mode"].ToString() == "insert")
                {
                    prj = new Project();
                    prj.name = txtProjectName.Text;
                    prj.complexityId = Convert.ToInt32(ddlProjectComplexity.SelectedValue);
                    prj.contingencyPercent = Convert.ToByte(txtContingency.Text);

                 //   prj.projectTypeId = Convert.ToInt32(cbo.SelectedValue);
                    prj.teamStructureId = Convert.ToInt32(rdoListTeamStructure.SelectedValue);
                    prj.resourcesSkillLevelId = Convert.ToInt32(ddlResourcesSkills.SelectedValue);
                    prj.clientId = (int)Session["id"];
                    if (db.Projects.Count() >1)
                    {
                        prj.id = db.Projects.Max(i => i.id) + 1;  
                    }
                    else
                    {
                        prj.id = 1;
                    }
                   
                    db.Projects.Add(prj);
                   
                }
                else
	            {


                     prj = (from s in db.Projects
                               where s.id == Myproject.id
                               select s).FirstOrDefault();
                    
                prj.name = txtProjectName.Text;
                prj.complexityId = Convert.ToInt32(ddlProjectComplexity.SelectedValue);
                prj.contingencyPercent = Convert.ToByte(txtContingency.Text);
                prj.teamStructureId = Convert.ToInt32(rdoListTeamStructure.SelectedValue);
                prj.resourcesSkillLevelId = Convert.ToInt32(ddlResourcesSkills.SelectedValue);


                List<ProjectType> typList = (from s in db.ProjectTypes
                                             where s.projectId == Myproject.id
                                             select s).ToList();
                foreach (ProjectType typ in typList)
                {
                    db.ProjectTypes.Remove(typ);
                }
              



                List<DeploymentEnvironment> envlist = (from s in db.DeploymentEnvironments
                                                       where s.projectId == Myproject.id
                                                       select s).ToList();
                foreach (DeploymentEnvironment Env in envlist)
                {
                    db.DeploymentEnvironments.Remove(Env);
                }

                List<ProjectScope> prjlist = (from s in db.ProjectScopes
                                              where s.projectId == prj.id
                                              select s).ToList();
                foreach (ProjectScope prjl in prjlist)
                {
                    db.ProjectScopes.Remove(prjl);
                }
                }
              
                foreach (ListItem item in CheckListTypes.Items)
                {
                    if (item.Selected)
                    {
                        ProjectType py = new ProjectType();
                        py.projectId = prj.id;
                        py.typeId = Convert.ToInt32(item.Value);
                        db.ProjectTypes.Add(py);

                        //get WorkItems related to project type
                        var queryWorkTypelist = (from s in db.WorkType_ProjectType
                                                 where s.ProjectTypeId == py.typeId 
                                                 select new {s.TypeId}).ToList();
                        //get and add workItems details
                        foreach (var itemType in queryWorkTypelist)
                        {

                            var QueryIems = (from s in db.Ref_WorkItemTypeEffort
                                             where s.workItemTypeId == itemType.TypeId
                                             select s).ToList();
                            foreach (var effort in QueryIems)
                            {
                                Ref_User_WorkItemTypeEffort neweffort= new Ref_User_WorkItemTypeEffort();
                                neweffort.workItemTypeId = effort.workItemTypeId;
                                neweffort.workItemComplexityId = effort.workItemComplexityId;
                                neweffort.Effort = effort.defaultEffort;
                                neweffort.isNew = effort.isNew;
                                neweffort.userId = client.ID;
                                neweffort.IsSystem = effort.IsSystem;
                                db.Ref_User_WorkItemTypeEffort.Add(neweffort);
                            }
                        
                        }
                       

                    }
               

                }
                bool HasDeploy = false;
                foreach (ListItem item in chkListProjectScope.Items )
	            {

                    if (item.Text=="Deployment" & item.Selected)
                    
                        HasDeploy = true;
                   
                    if (item.Selected)
                    {
                    ProjectScope ps=new ProjectScope();
                        ps.projectId=prj.id;
                        ps.projectScopeId = Convert.ToInt32(item.Value);
                        db.ProjectScopes.Add(ps);
                    } 
	            }
                if (HasDeploy)
                {
                    
                
                foreach (ListItem item in chkListEnvironments.Items)
                {
                    if (item.Selected)
                    {
                        DeploymentEnvironment ps = new DeploymentEnvironment();
                        ps.projectId = prj.id;
                        ps.environmentId = Convert.ToInt32(item.Value);
                        db.DeploymentEnvironments.Add(ps);
                    }
                }

                }
               

                  
                db.SaveChanges();
                Response.Redirect(HttpContext.Current.Request.Url.Scheme
                                    + "://"
                                    + HttpContext.Current.Request.Url.Authority
                                    + HttpContext.Current.Request.ApplicationPath + "Estimator/ProjectWorkItems.aspx?id=" + prj.id);
            }
            catch (Exception)
            {
                
                throw;
            }
            
           
        }
        }
      

        protected void ddlProjects_SelectedIndexChanged(object sender, EventArgs e)
        {

        
            
            int projid= Convert.ToInt32(ddlProjects.SelectedValue);
            if (projid!=-1)
             {
               lblerr.Text = "";
               Session["mode"] = "Update";
           
            Myproject = (from s in db.Projects
                         where s.id == projid
                        select s).First();
            Session["Project"] = Myproject;
            string selectedProjectId = ddlProjects.SelectedValue;

            txtProjectName.Text = db.Projects.Where(s => s.id == Myproject.id).Select(s => s.name).First();
           // cbo.SelectedValue = (db.Projects.Where(s => s.id == Myproject.id).Select(s => s.projectTypeId).First()).ToString();
            ProjectTypes = (from s in db.ProjectTypes
                                    where s.projectId == Myproject.id
                                    select s).ToList();


            foreach (ListItem item in CheckListTypes.Items)
            {

                if (ProjectTypes.Where(s => s.typeId.ToString() == item.Value).Count() != 0)
                {
                    item.Selected = true;
                }

            }
                
                BindScopePage();
            BindAssumptionPage();

            }
            else
            {
                InitiatePage();
                
                return;   
            }
          
        }


    

   
       
        protected void Wizard1_PreRender(object sender, EventArgs e)
        {
            Repeater SideBarList = EstimatorWizard.FindControl("HeaderContainer").FindControl("SideBarList") as Repeater;
            SideBarList.DataSource = EstimatorWizard.WizardSteps;
            SideBarList.DataBind();

        }

        protected string GetClassForWizardStep(object wizardStep)
        {
            WizardStep step = wizardStep as WizardStep;

            if (step == null)
            {
                return "";
            }
            int stepIndex = EstimatorWizard.WizardSteps.IndexOf(step);

            if (stepIndex < EstimatorWizard.ActiveStepIndex)
            {
                return "prevStep";
            }
            else if (stepIndex > EstimatorWizard.ActiveStepIndex)
            {
                return "nextStep";
            }
            else
            {
                return "currentStep";
            }
        }

         void Click()
        {
            lblerr.Text = "";
            InitiatePage();
            Session["mode"] = "insert";
            txtProjectName.Focus();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            lblerr.Text = "";
            InitiatePage();
            Session["mode"] = "insert";         
            txtProjectName.Focus();
        
        }

        protected void EstimatorWizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (e.CurrentStepIndex == 0 & Session["mode"].ToString()=="insert")
            {

                try
                {
                    int p_id=(int)Session["id"];


                    if (db.Projects.Where(s => s.name == txtProjectName.Text.Trim() & s.clientId == p_id).Count() > 0)
                    {
                        throw new Exception("Project name already exist .");
                    }
                    else
                    {
                        lblerr.Text = "";    
                    }
                }
                catch (Exception ex)
                {

                    lblerr.Text = ex.Message;
                    e.Cancel=true;
                }
                
             
	            
            }

            if (e.CurrentStepIndex == 0 & CheckListTypes.SelectedValue.Count()==0)
            {
                try
                {
                  throw new Exception("Project should  have atleast one type");
                }
                catch (Exception ex)
                {
                    lblTypeErr.Visible = true;
                    lblTypeErr.Text = ex.Message;
                    e.Cancel = true;
                }
             
                        
            }
                
   


         
        
            if (e.CurrentStepIndex == 1)
            {

                try
                {
                    int numSelected = 0;
                    int numEnselected = 0;
                        foreach (ListItem li in chkListProjectScope.Items)
                        {
                        if (li.Selected)
                        {
                        numSelected = numSelected + 1;
                                if (li.Text=="Deployment")
                                {
                                    foreach (ListItem lii in chkListEnvironments.Items)
                                    {
                                        if (lii.Selected)
                                        {
                                            numEnselected += 1;
                                        }
                                    }
                                    if (numEnselected==0)
                                    {
                                       throw new Exception("Atleast one Environment should be selected");  
                                    }
                                }
                         }
                        }
                        if (numSelected == 0)
                        {
                            throw new Exception("Atleast one scope should be selected");
                        }
                        else
                        {
                            lblScopeErr.Visible=false;
                        }
             
                }
                catch (Exception ex)
                {
                    lblScopeErr.Visible = true;
                    lblScopeErr.Text = ex.Message;
                    e.Cancel = true;
                }



            }
        }

        protected void chkListProjectScope_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lblScopeErr.Visible)
                lblScopeErr.Visible = false;
            foreach (ListItem li in chkListProjectScope.Items)
            {
                if (li.Text=="Deployment" & li.Selected)
                {
                    lblEnv.Visible = true;
                    chkListEnvironments.Visible = true;
                   
                }
                if (li.Text == "Deployment" & !li.Selected)
                {
                    lblEnv.Visible = false;
                    chkListEnvironments.Visible = false;
                }
               
            }
       
        }

        //protected void btnexcel_Click(object sender, EventArgs e)
        //{
        //    string strFilename = txtProjectName.Text+"info.xls";
        //    string attachment = "attachment; filename=" + strFilename;
        //    Response.ClearContent();
        //    Response.AddHeader("content-disposition", attachment);
        //    Response.ContentType = "application/vnd.ms-excel";
           
        //    string strenv = string.Empty;


        //    Response.Write("Project General Information");
        //    Response.Write("\n");
        //    Response.Write( "Project Name:");
        //    Response.Write("\t" + txtProjectName.Text);
        //     Response.Write("\n");
        //     Response.Write("Project Type: ");
        //     Response.Write("\t" + cbo.SelectedItem.Text);
        //    Response.Write("\n");
        //    Response.Write("Project Overal Complexity:");
        //    Response.Write("\t" + ddlProjectComplexity.SelectedItem.Text);
        //    Response.Write("\n");
        //    Response.Write("Project Contingency:");
        //    Response.Write("\t" + txtContingency.Text);
        //    Response.Write("\n");
        //    Response.Write("Resource Skill Set: ");
        //    Response.Write("\t" + ddlResourcesSkills.SelectedItem.Text);
        //   Response.Write("\n");
        //   Response.Write("Team Structure:");
        //   Response.Write("\t" + rdoListTeamStructure.SelectedItem.Text);
        //    Response.Write("\n");
        //    Response.Write("\n");
        //    Response.Write("Project Scope");
        //    Response.Write("\n");
        
        //    foreach (ListItem item in chkListProjectScope.Items)
        //    {
        //        if (item.Selected &item.Text!="Deployment" )
        //        {
        //            Response.Write("\t" + item.Text);
        //            Response.Write("\n");
               
                   
        //        }
        //        if (item.Selected & item.Text == "Deployment")
        //        {
        //             strenv = "\t Deployment \n Deployment Environments \n";

        //            foreach (ListItem itemenv in chkListEnvironments.Items)
        //            {
                  
        //                if (itemenv.Selected)
        //                {
        //                    strenv += "\t "+itemenv.Text +"\n";
        //                }
        //            }

        //        }
        //    }
        //    if (!String.IsNullOrEmpty(strenv))
        //    {
        //         Response.Write(strenv);
        //    }
      
              
        //    Response.End();
        //}
    

        
  
    
         


     
     

   

   

    

        

    }
                
}