﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectWorkItems.aspx.cs" Inherits="ArchiFrame.Estimator.ProjectWorkItems" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">

       
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="MainContent" runat="server">
    
     <div id="leftnav">
               
                  <ul class="menu" >
		            <li class="item1" ><a  href="/Default.aspx"> Home</a>
		
		            </li>
		            <li class="item2"  >  <a class="active" href="/Estimator/ProjectGeneralInfo.aspx">Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a class="selected" href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			       
                             </ul>
		            </li>
		           <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                                   <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                    
                                 </ul>
		            </li>  
		               <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  

	               </ul>
          </div>

     <div class="rightnav">
    
          <div class="reg">
                 <div class="body_header_style">
                      Project Work Items
                                          </div>
                     <%--<div class="rightholder">
                      
                     </div>
                             <div class="divexcel">
       
              <asp:ImageButton runat="server"  ID="ImageButton1"  OnClick="btnexcel_Click" Height="50px" ImageUrl="~/Images/excel.png" Width="50px"  />
              </div>
                        </div>--%>
            
               

   
    Select project from the list
                         <asp:Label ID="lblerr" runat="server" Visible="False" CssClass="field_validation_error"></asp:Label>
    <br />
                            <br />
    <asp:DropDownList ID="ddlProjects" runat="server" OnSelectedIndexChanged="ddlProjects_SelectedIndexChanged" AutoPostBack="true" class="drop_down_list_style"></asp:DropDownList>
     <asp:CustomValidator id="CustomValidatorddlProject" runat="server" 
                      OnServerValidate="DdlValidator" 
                      ControlToValidate="ddlProjects" 
                      ErrorMessage="Please Select a Project."
                     CssClass="field_validation_error">
                    </asp:CustomValidator>
                      <%--   <asp:DropDownList ID="DDLTypes" runat="server" OnSelectedIndexChanged="DDLTypes_SelectedIndexChanged" AutoPostBack="True">
                         </asp:DropDownList>--%>
          <br />
    <asp:GridView ID="GridView1" runat="server"     CssClass="mGrid" AllowSorting="True" AutoGenerateColumns="False" OnRowEditing = "EditWorkItem" OnRowDataBound = "WorkItemRowDataBound" OnRowUpdating = "UpdateWorkItem" OnRowCancelingEdit = "CancelEdit" OnRowDeleting="DeleteWorkItem" DataKeyNames = "id" CellPadding="4" ForeColor="#333333" GridLines="None" ShowFooter="True" OnRowCommand="GridView1_RowCommand" OnSorting="GridView1_Sorting">
        <AlternatingRowStyle BackColor="White"/>

        <Columns>
             <asp:TemplateField HeaderText="Project Type"  SortExpression="Project Type" >
                    <ItemTemplate>
                    <asp:Label ID="lblWorkItemprojectType" runat="server" Text='<%# Eval("projectTypeName")%>'></asp:Label>
           
                </ItemTemplate>
                         
                <EditItemTemplate>
                        <asp:Label ID="lblWorkItemProjectTypeHidden" runat="server" Text='<%# Eval("projectTypeName")%>' Visible = "false"></asp:Label>
                         
                   <asp:DropDownList ID="ddlProjectTypeEdit" runat="server" AutoPostBack="true"  class="drop_down_list_Grid "></asp:DropDownList>
                
                </EditItemTemplate>

                <FooterTemplate>
                <asp:DropDownList ID="ddlProjectTypeAdd" runat="server" AutoPostBack="true"  class="drop_down_list_Grid "></asp:DropDownList>
                </FooterTemplate>
              </asp:TemplateField>

            <asp:TemplateField HeaderText="TYPE"  SortExpression="Type" >
                    <ItemTemplate>
                    <asp:Label ID="lblWorkItemType" runat="server" Text='<%# Eval("name")%>'></asp:Label>
                </ItemTemplate>
                         
                <EditItemTemplate>
                        <asp:Label ID="lblWorkItemTypeHidden" runat="server" Text='<%# Eval("name")%>' Visible = "false"></asp:Label>
                    <asp:DropDownList ID="ddlTypeEdit" runat="server" AutoPostBack="true"  class="drop_down_list_Grid "></asp:DropDownList>
                </EditItemTemplate>

                <FooterTemplate>
                <asp:DropDownList ID="ddlTypeAdd" runat="server" AutoPostBack="true" class="drop_down_list_Grid "></asp:DropDownList>
                </FooterTemplate>
              </asp:TemplateField>
            

            <asp:TemplateField HeaderText="COMPLEXITY" SortExpression="Complexity" >
                    <ItemTemplate>
                    <asp:Label ID="lblWorkItemComplexity" runat="server" Text='<%# Eval("complexity")%>'></asp:Label>
                </ItemTemplate>
                       
                <EditItemTemplate>
                        <asp:Label ID="lblWorkItemComplexityHidden" runat="server" Text='<%# Eval("complexity")%>' Visible = "false"></asp:Label>
                    <asp:DropDownList ID="ddlComplexity" runat="server" class="drop_down_list_Grid "></asp:DropDownList>
                </EditItemTemplate>
               <FooterTemplate>
                <asp:DropDownList ID="ddlComplexityAdd" runat="server" class="drop_down_list_Grid "></asp:DropDownList>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="DESCRIPTION" SortExpression="Description">
                    <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("description")%>'></asp:Label>
                </ItemTemplate>

                <EditItemTemplate>
                        <asp:Label ID="lblDescriptionHidden" runat="server" Text='<%# Eval("description")%>' Visible = "false"></asp:Label>
                    <asp:TextBox ID="txtDescription" runat="server" ></asp:TextBox>
                </EditItemTemplate>
             <FooterTemplate>
              <asp:TextBox ID="txtDescriptionAdd" runat="server" ></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="QUANTITY" >
                    <ItemTemplate>
                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("quantity")%>'></asp:Label>
                </ItemTemplate>
                        
                <EditItemTemplate>
                        <asp:Label ID="lblQuantityHidden" runat="server" Text='<%# Eval("quantity")%>' Visible = "false"></asp:Label>
                    <asp:TextBox ID="txtQuantity" runat="server" onkeypress="CheckNumeric(event)" Width="20px"></asp:TextBox>
                </EditItemTemplate>
             <FooterTemplate>
              <asp:TextBox ID="txtQuantityAdd" runat="server"  onkeypress="CheckNumeric(event)" Text="1" Width="20px"></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="New" >
                    <ItemTemplate>
                    <asp:CheckBox ID="lblIsNew" runat="server" Enabled="false" Checked='<%# Eval("IsNew")%>'></asp:CheckBox>
                </ItemTemplate>
                        
                <EditItemTemplate>
                        <asp:Label ID="lblIsNewHidden" runat="server" Checked='<%# Eval("IsNew")%>' Visible = "false"></asp:Label>
                    <asp:CheckBox ID="chkIsNew" runat="server" Checked='<%# Eval("IsNew")%>'></asp:CheckBox>
                </EditItemTemplate>
             <FooterTemplate>
              <asp:CheckBox ID="chkIsNewAdd" runat="server" Checked="true" ></asp:CheckBox>
                </FooterTemplate>
            </asp:TemplateField>

               <asp:TemplateField HeaderText="InScope" >
                    <ItemTemplate>
                    <asp:CheckBox ID="lblInScope" runat="server" Enabled="false" Checked='<%# Eval("InScope")%>'></asp:CheckBox>
                </ItemTemplate>
                        
                <EditItemTemplate>
                        <asp:Label ID="lblInScopeHidden" runat="server" Checked='<%# Eval("InScope")%>' Visible = "false"></asp:Label>
                    <asp:CheckBox ID="chkInScope" runat="server" Checked='<%# Eval("InScope")%>'></asp:CheckBox>
                </EditItemTemplate>
             <FooterTemplate>
              <asp:CheckBox ID="chkInScopeAdd" runat="server" Checked="true" ></asp:CheckBox>
                </FooterTemplate>
            </asp:TemplateField>
               <asp:TemplateField HeaderText="SOURCE" >

           <ItemTemplate>
                    <asp:Label ID="lblSource" runat="server" Text='<%# Eval("sourcename")%>'></asp:Label>
                </ItemTemplate>
                        
                    <EditItemTemplate>
                        <asp:Label ID="lblsourceshow" runat="server" Text='<%# Eval("sourcename")%>'></asp:Label>
                    <asp:DropDownList ID="ddlsource" runat="server" class="drop_down_list_Grid "></asp:DropDownList>
                </EditItemTemplate>
                <FooterTemplate>
                    
                <asp:DropDownList ID="ddlsourceAdd" runat="server" class="drop_down_list_Grid "></asp:DropDownList><br />
                        <asp:LinkButton ID="sysButton" runat="server" Text="Add New System" 
               CommandName="Addsys" CssClass="underline"/>                    
                </FooterTemplate>

                    </asp:TemplateField>
            <asp:TemplateField HeaderText="TARGET" >
                <ItemTemplate>
                    <asp:Label ID="lbltarget" runat="server" Text='<%# Eval("targetname")%>'></asp:Label>
                </ItemTemplate>
           <EditItemTemplate>
                    <asp:Label ID="lbltargetshow" runat="server" Text='<%# Eval("targetname")%>'></asp:Label>
                   <asp:DropDownList ID="ddltargetEdit" runat="server" class="drop_down_list_Grid "></asp:DropDownList>
                </EditItemTemplate>
                   <FooterTemplate>
                <asp:DropDownList ID="ddltargetAdd" runat="server" class="drop_down_list_Grid "></asp:DropDownList>
                </FooterTemplate>

                    </asp:TemplateField>
            <asp:TemplateField>
    <EditItemTemplate>
        <asp:ImageButton ID="btnUpdate" runat="server" CommandName="Update"   ImageUrl="../Images/save.png" ToolTip="Update" />
        <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel"   ImageUrl="../Images/cancel.png" ToolTip="Cancel" CausesValidation="false" />
    </EditItemTemplate>
    <ItemTemplate>
        <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit"  ImageUrl="../Images/edit.png" ToolTip="Edit"  />
        <asp:ImageButton ID="btnDelete" runat="server" CommandName="Delete"   ImageUrl="../Images/delete.png" ToolTip="Delete" />
    </ItemTemplate>
               <FooterTemplate>
                <asp:Button ID="BtnAdd" runat="server"  CommandName="Add" Text="Add item" CssClass="cssbutton"></asp:Button>
                </FooterTemplate>
</asp:TemplateField>

          <%--  <asp:CommandField ShowEditButton="True" ShowDeleteButton="true" />--%>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#C8c8c8" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True"  />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />


    </asp:GridView>
           <asp:Label ID="lblErGrid" runat="server" Text="" CssClass="field_validation_error"></asp:Label>
       <div id="sysdiv" class='warn' runat="server" visible="false"> 
             <label class="lblsmall">System Name :</label>
             <asp:TextBox runat="server" ID="txtSys"  CssClass="lblsmall" />
            <br />
            <asp:Label runat="server" ID="lblsyserr" Text="" CssClass="custom-field-validation-error "></asp:Label>
            <br />
            <asp:Button ID="btnSave" runat="server" Text="Save"    CssClass="cssbutton" OnClick="btnSave_Click" />

           <asp:Button ID="Btncancel" runat="server" Text="Cancel"  CssClass="cssbutton"  OnClick="Btncancel_Click" />
            <br />
   
              </div>

                   
             
   
    </div>
         <div class="btnholder">

            <div class="btnholderleft">
                <asp:Button ID="btnEditProjectInfo" runat="server" OnClick="btnEditProjectInfo_Click" Text="Edit Project" CssClass="cssbutton"/>

            </div>
             <div class="btnholderight">
    <asp:Button ID="btnEstimate" runat="server" OnClick="btnEstimate_Click" Text="Estimate" CssClass="cssbutton"/>
            </div>
         </div>
      
              </div>
   </asp:Content>                     
    

     
  




<asp:Content ID="footerContentt"  runat="server" ContentPlaceHolderID="Footer">
    </asp:Content>