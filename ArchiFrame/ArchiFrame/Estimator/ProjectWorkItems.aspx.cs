﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ArchiFrame.Class;
using System.Web.Security;
namespace ArchiFrame.Estimator
{
    public partial class ProjectWorkItems : System.Web.UI.Page
    {
    
        static string PROJECT_ID;
        private ArchiFrameEntities db = new ArchiFrameEntities();
        private Guid id;
        private Project Myproject;
        Client client = new Client();
        List<GridItem> GridSource = new List<GridItem>();
        protected void Page_Load(object sender, EventArgs e)
        {
            lblsyserr.Text = "";
            MembershipUser user = Membership.GetUser();
            string UserID = user.ProviderUserKey.ToString();
            id = Guid.Parse(UserID);
            client.ID = (from s in db.User_Profile
                         where s.UserId == id
                         select s.UserCompany_Id).First();
            Session["id"] = client.ID;
            if (!IsPostBack)
            {
              
                 this.BindProjectDropDownList();        
                
                PROJECT_ID = Request.QueryString.Get("id");
                if (!string.IsNullOrEmpty(PROJECT_ID))
                {
                    // Select the right project in Project Dropdown List
                    ddlProjects.ClearSelection();
                    ddlProjects.Items.FindByValue(PROJECT_ID).Selected = true;
                    fillFormAfterProjectSelected(PROJECT_ID);
                }
            }
            else
            {
                lblerr.Visible = false;
                        if (Request.Form["__EVENTTARGET"].Contains("ddlProjectTypeEdit"))
                {
                   int RowNum= (int)Session["RowNumber"] ;

                   int Prjtyp = Convert.ToInt32((GridView1.Rows[RowNum].FindControl("ddlProjectTypeEdit") as DropDownList).SelectedItem.Value);
                   DropDownList ddlTypeEdit = GridView1.Rows[RowNum].FindControl("ddlTypeEdit") as DropDownList;
                   ddlTypeEdit.DataSource = (from s in db.WorkType_ProjectType
                                             join p in db.Ref_WorkItemType on s.TypeId equals p.id
                                             where s.ProjectTypeId == Prjtyp
                                             select new { id = p.id, name = p.name }).ToList();
                   ddlTypeEdit.DataTextField = "name";
                   ddlTypeEdit.DataValueField = "id";
                   ddlTypeEdit.DataBind();
                   ddlTypeEdit.Items.Insert(0, new ListItem("[select]", "-1"));
                   ddlTypeEdit.SelectedIndex = 0;
                }
                if (Request.Form["__EVENTTARGET"].Contains("ddlProjectType"))
                {
                    int Prjtyp = Convert.ToInt32((GridView1.FooterRow.FindControl("ddlProjectTypeAdd") as DropDownList).SelectedItem.Value);
                    DropDownList ddlTypeAdd = GridView1.FooterRow.FindControl("ddlTypeAdd") as DropDownList;
                    ddlTypeAdd.DataSource = (from s in db.WorkType_ProjectType
                                             join p in db.Ref_WorkItemType on s.TypeId equals p.id
                                             where s.ProjectTypeId == Prjtyp
                                             select new { id = p.id, name = p.name }).ToList();
                    ddlTypeAdd.DataTextField = "name";
                    ddlTypeAdd.DataValueField = "id";
                    ddlTypeAdd.DataBind();
                    ddlTypeAdd.Items.Insert(0, new ListItem("[select]", "-1"));
                    ddlTypeAdd.SelectedIndex = 0;
                }
                if (Request.Form["__EVENTTARGET"].Contains("ddlTypeAdd"))
	            {
                    int typ = Convert.ToInt32((GridView1.FooterRow.FindControl("ddlTypeAdd") as DropDownList).SelectedItem.Value);
                    DropDownList ddltargetAdd = GridView1.FooterRow.FindControl("ddltargetAdd") as DropDownList;
                    if (db.Ref_WorkItemType.Where(s => s.id == typ).Select(s => s.systemable).First() != 1)
                    {
                        
                        ddltargetAdd.Visible=false;
                    }
                    else
                    {
                        ddltargetAdd.Visible = true;
                    }
	            }
                if (Request.Form["__EVENTTARGET"].Contains("ddlTypeEdit"))
                {
                    int RowNum = (int)Session["RowNumber"];
                    int typ = Convert.ToInt32((GridView1.Rows[RowNum].FindControl("ddlTypeEdit") as DropDownList).SelectedItem.Value);
                    DropDownList ddltargetEdit = GridView1.Rows[RowNum].FindControl("ddltargetEdit") as DropDownList;
                    if (db.Ref_WorkItemType.Where(s => s.id == typ).Select(s => s.systemable).First() != 1)
                    {

                        ddltargetEdit.Visible = false;
                    }
                    else
                    {
                        ddltargetEdit.Visible = true;
                    }
                }
             
            }
        }
      
        protected void DdlValidator(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (args.Value != "-1");
        } 

        private void BindProjectDropDownList()
        {

            var queryddlProject = (from s in db.Projects
                                   where s.clientId == client.ID
                                   select new { s.id, s.name });
            List<itemFordpl> listItemddlProjects = new List<itemFordpl>();

            foreach (var x in queryddlProject)
            {
                itemFordpl li = new itemFordpl();
                li.ID = x.id;
                li.Name = x.name;
                listItemddlProjects.Add(li);
            }

            itemFordpl temp = new itemFordpl();
            temp.ID = -1;
            temp.Name = "[Select]";
            listItemddlProjects.Add(temp);
            ddlProjects.DataSource = listItemddlProjects;
            ddlProjects.DataTextField = "Name";
            ddlProjects.DataValueField = "ID";
            ddlProjects.DataBind();
            ddlProjects.SelectedValue = "-1";
         

        }

     
        private void BindWorkItemGridView()
        {
            if (!string.IsNullOrEmpty(PROJECT_ID))
            {
                int id = Convert.ToInt32(PROJECT_ID);
       
                int projectid = db.Projects.Where(s => s.id == id).Select(s => s.id).First();
       
                var query = (from s in db.WorkItems
                             join p in db.Ref_WorkItemType on s.typeId equals p.id
                                           join q in db.WorkItemComplexities on s.complexityId equals q.id
                                           where s.projectId == projectid
                                           select new { p.name, s.id, q.complexity, s.description, s.quantity, p.systemable, s.isNew, s.inScope ,s.projectType}).ToList();
                //if (DDLTypes.SelectedValue!="-1")
                //    {
                //        int typ = Convert.ToInt32(DDLTypes.SelectedValue);
                //        query = query.FindAll(s => s.projectType == typ);
                //}
               
                
                if (query.Count >0)
                {

                    Session["noresult"] = "1";
             

                    foreach (var item in query)
                    {
                        GridItem newItem = new GridItem();
                        newItem.id = item.id;
                        newItem.name = item.name;
                        newItem.quantity = item.quantity;
                        newItem.description = item.description;
                        newItem.complexity = item.complexity;
                        newItem.sourceid = db.workitemsystems.Where(s => s.workitemid == newItem.id).Select(s => s.systemsourceid).First();
                        newItem.sourcename = db.Systemees.Where(s => s.id == newItem.sourceid).Select(s => s.name).First();
                        newItem.IsNew = (bool)item.isNew;
                        newItem.inScope = (bool)item.inScope;
                        newItem.projectTypeId = item.projectType;
                        newItem.projectTypeName = db.Ref_ProjectType.Where(s => s.id == newItem.projectTypeId).Select(s => s.name).First();
                        if (item.systemable == 1)
                        {
                                                      
                            if (db.workitemsystems.Where(s => s.workitemid == newItem.id).Select(s => s.systemtargetid).Count() != 0)
                            {
                                newItem.targid = db.workitemsystems.Where(s => s.workitemid == newItem.id).Select(s => s.systemtargetid).FirstOrDefault();
                                newItem.targetname = db.Systemees.Where(s => s.id == newItem.targid).Select(s => s.name).First();
                            }
                           
                        }
                        else
                        {
                           
                            newItem.targetname = "--------";
                        }
                        GridSource.Add(newItem);

                    }
                    GridView1.DataSource = GridSource;
                    GridView1.DataBind();
                }
                else
                {
                    Session["noresult"] = null;

                        

                        GridItem newItem = new GridItem();
                        newItem.id =-1;
                        newItem.name = "";
                        newItem.quantity = 0;
                        newItem.description ="";
                        newItem.complexity = "";
                      
                        GridSource.Add(newItem);
                        GridView1.DataSource = GridSource;
                        GridView1.DataBind();
                    foreach (GridViewRow row in GridView1.Rows)
                       {

                          ImageButton btnEdit =(ImageButton)row.FindControl("btnEdit") ;
                          btnEdit.Visible = false;
                        ImageButton btnDelete=(ImageButton)row.FindControl("btnDelete") ;
                        btnDelete.Visible = false;
                        Label lblQuantity = (Label)row.FindControl("lblQuantity");
                        lblQuantity.Visible = false;
                        }
                  
                }
            }


        }
   

        protected void EditWorkItem(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            Session["RowNumber"] = GridView1.EditIndex;
            BindWorkItemGridView();
 
        }

        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindWorkItemGridView();
        }

        protected void WorkItemRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                int prj = Convert.ToInt32(PROJECT_ID);   
                DropDownList ddlProjectTypeAdd = (DropDownList)e.Row.FindControl("ddlProjectTypeAdd");

                ddlProjectTypeAdd.DataSource = (from s in db.ProjectTypes
                                                join p in db.Ref_ProjectType on s.typeId equals p.id
                                                 where s.projectId == prj
                                                select new { projectTypeId = s.typeId, ProjectTypeName = p.name }).ToList();
                ddlProjectTypeAdd.DataTextField = "ProjectTypeName";
                ddlProjectTypeAdd.DataValueField = "projectTypeId";
                ddlProjectTypeAdd.DataBind();
                ddlProjectTypeAdd.Items.Insert(0, new ListItem("[select]", "-1"));
                ddlProjectTypeAdd.SelectedIndex = 0;

                DropDownList ddlComplexityAdd = (DropDownList)e.Row.FindControl("ddlComplexityAdd");
                ddlComplexityAdd.DataSource = (from s in db.Ref_ProjectComplexity
                                               select new { id = s.id, complexity = s.name }).ToList();

                ddlComplexityAdd.DataTextField = "complexity";
                ddlComplexityAdd.DataValueField = "id";
                ddlComplexityAdd.DataBind();
                ddlComplexityAdd.Items.Insert(0, new ListItem("[select]", "-1"));
                ddlComplexityAdd.SelectedIndex = 0;

                DropDownList ddlTypeAdd = (DropDownList)e.Row.FindControl("ddlTypeAdd");

                //ddlTypeAdd.DataSource = (from s in db.WorkItemTypes
                //                      select s).ToList();
                //int prjType = Convert.ToInt32(ddlProjectTypeAdd.SelectedValue);
                //ddlTypeAdd.DataSource = (from s in db.WorkType_ProjectType
                //                         join p in db.WorkItemTypes on s.TypeId equals p.id
                //                         where s.ProjectTypeId == prjType
                //                         select new { id = p.id, name = p.name }).ToList();
                //ddlTypeAdd.DataTextField = "name";
                //ddlTypeAdd.DataValueField = "id";
                //ddlTypeAdd.DataBind();
                //ddlTypeAdd.Items.Insert(0, new ListItem("[select]", "-1"));
                //ddlTypeAdd.SelectedIndex = 0;
              

                DropDownList ddlsourceAdd = (DropDownList)e.Row.FindControl("ddlsourceAdd");
                DropDownList ddltargetAdd = (DropDownList)e.Row.FindControl("ddltargetAdd");
                ddlsourceAdd.DataTextField = "name";
                ddlsourceAdd.DataValueField = "id";
                ddlsourceAdd.DataSource = (from s in db.Systemees
                                           where s.client_id == client.ID
                                        select s).ToList();
                ddlsourceAdd.DataBind();
                ddlsourceAdd.Items.Insert(0, new ListItem("[select]", "-1"));
                ddlsourceAdd.SelectedIndex = 0;
                ddltargetAdd.DataTextField = "name";
                ddltargetAdd.DataValueField = "id";
                ddltargetAdd.DataSource = (from s in db.Systemees
                                           where s.client_id == client.ID
                                        select s).ToList();
                ddltargetAdd.DataBind();
                ddltargetAdd.Items.Insert(0, new ListItem("[select]", "-1"));
                ddltargetAdd.SelectedIndex = 0;

            }
             if (e.Row.RowType == DataControlRowType.DataRow && GridView1.EditIndex == e.Row.RowIndex)
                {

                     int prj = Convert.ToInt32(PROJECT_ID);   
                    DropDownList ddlProjectType = (DropDownList)e.Row.FindControl("ddlProjectTypeEdit");
                    ddlProjectType.DataSource = (from s in db.ProjectTypes
                                                 join p in db.Ref_ProjectType on s.typeId equals p.id
                                                 where s.projectId == prj
                                                 select new { projectTypeId = s.typeId, projectTypeName = p.name }).ToList();
                    ddlProjectType.DataTextField = "projectTypeName";
                    ddlProjectType.DataValueField = "projectTypeId";
                    ddlProjectType.DataBind();
                    DropDownList ddlComplexity = (DropDownList)e.Row.FindControl("ddlComplexity");
                    ddlComplexity.DataSource = (from s in db.Ref_ProjectComplexity
                                                select new { id = s.id, complexity = s.name }).ToList();
                                 
                    ddlComplexity.DataTextField = "complexity";
                    ddlComplexity.DataValueField = "id";
                    ddlComplexity.DataBind();

                   
                    DropDownList ddlTypeEdit = (DropDownList)e.Row.FindControl("ddlTypeEdit");
                    ddlTypeEdit.DataSource = (from s in db.Ref_WorkItemType
                                              select s).ToList();
                    int prjType = Convert.ToInt32(ddlProjectType.SelectedValue);
                    ddlTypeEdit.DataSource = (from s in db.WorkType_ProjectType
                                              join p in db.Ref_WorkItemType on s.TypeId equals p.id
                                              where s.ProjectTypeId == prjType
                                              select new { id = p.id, name = p.name }).ToList();
                    ddlTypeEdit.DataTextField = "name";
                    ddlTypeEdit.DataValueField = "id";
                    ddlTypeEdit.DataBind();
                  
                    TextBox txtDescription = (TextBox)e.Row.FindControl("txtDescription");
                    txtDescription.Text = (e.Row.FindControl("lblDescriptionHidden") as Label).Text;

                    TextBox txtQuantity = (TextBox)e.Row.FindControl("txtQuantity");
                    txtQuantity.Text = (e.Row.FindControl("lblQuantityHidden") as Label).Text;
                    ddlProjectType.Items.FindByText((e.Row.FindControl("lblWorkItemProjectTypeHidden") as Label).Text).Selected = true;
                    ddlTypeEdit.Items.FindByText((e.Row.FindControl("lblWorkItemTypeHidden") as Label).Text).Selected = true;
                    ddlComplexity.Items.FindByText((e.Row.FindControl("lblWorkItemComplexityHidden") as Label).Text).Selected = true;
                    int workItemId = (int)GridView1.DataKeys[e.Row.RowIndex].Value;
                    int typeId = db.WorkItems.Where(s => s.id == workItemId).Select(s => s.typeId).FirstOrDefault();

                    Label lblsourceshow = (Label)e.Row.FindControl("lblsourceshow");
                    Label lbltargetshow = (Label)e.Row.FindControl("lbltargetshow");
                    DropDownList ddlsource = (DropDownList)e.Row.FindControl("ddlsource");
                    DropDownList ddltargetEdit = (DropDownList)e.Row.FindControl("ddltargetEdit");

                    ddlsource.DataSource = (from s in db.Systemees
                                            where s.client_id == client.ID
                                            select s).ToList();
                    ddlsource.DataTextField = "name";
                    ddlsource.DataValueField = "id";
                    ddlsource.DataBind();
                   
                    ddlsource.Items.FindByText(lblsourceshow.Text).Selected = true;


                    ddltargetEdit.DataSource = (from s in db.Systemees
                                            where s.client_id == client.ID
                                            select s).ToList();

                    ddltargetEdit.DataTextField = "name";
                    ddltargetEdit.DataValueField = "id";
                    ddltargetEdit.DataBind();

                    if (db.Ref_WorkItemType.Where(s => s.id == typeId).Select(s => s.systemable).First() == 1)
                    {
                        ddltargetEdit.Items.FindByText((e.Row.FindControl("lbltargetshow") as Label).Text).Selected = true;
                        lblsourceshow.Visible = false;
                        ddlsource.Visible = true;
                        lbltargetshow.Visible = false;
                        ddltargetEdit.Visible = true;

                    }
                    else
                    {
                        lblsourceshow.Visible = false;
                        ddlsource.Visible = true;
                        //ddlsource.DataTextField = "name";
                        //ddlsource.DataValueField = "id";
                        //ddlsource.DataBind();

                        lbltargetshow.Visible = true;

                        ddltargetEdit.Visible = false;

                    }
                  

                }
                
         }

        protected void UpdateWorkItem(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                int projectType = Convert.ToInt32((GridView1.Rows[e.RowIndex].FindControl("ddlProjectTypeEdit") as DropDownList).SelectedItem.Value);
                int type = Convert.ToInt32((GridView1.Rows[e.RowIndex].FindControl("ddlTypeEdit") as DropDownList).SelectedItem.Value);
            int complexity = Convert.ToInt32((GridView1.Rows[e.RowIndex].FindControl("ddlComplexity") as DropDownList).SelectedItem.Value);
            string description = (GridView1.Rows[e.RowIndex].FindControl("txtDescription") as TextBox).Text;
            TextBox txtQuantity = GridView1.Rows[e.RowIndex].FindControl("txtQuantity") as TextBox;
            if (txtQuantity.Text.Trim() == "")
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Please Enter Quantity');", true);
                return;
            }

            int quantity = Convert.ToInt32(txtQuantity.Text);
            int workItemId =(int) GridView1.DataKeys[e.RowIndex].Value;
            bool isnew = (GridView1.Rows[e.RowIndex].FindControl("chkIsNew") as CheckBox).Checked;
            bool inscope = (GridView1.Rows[e.RowIndex].FindControl("chkInScope") as CheckBox).Checked;
          
            WorkItem myworkItem = (from s in db.WorkItems
                                   where s.id == workItemId
                                   select s).First();
            myworkItem.typeId = type;
            myworkItem.complexityId = complexity;
            myworkItem.description = description;
            myworkItem.quantity = quantity;
            myworkItem.projectId = Convert.ToInt32(PROJECT_ID);
            myworkItem.isNew = (bool)isnew;
            myworkItem.inScope = (bool)inscope;
            myworkItem.projectType = projectType;
            workitemsystem mysysItem = (from s in db.workitemsystems
                                        where s.workitemid == workItemId
                                        select s).First();
            mysysItem.systemsourceid = Convert.ToInt32((GridView1.Rows[e.RowIndex].FindControl("ddlsource") as DropDownList).SelectedItem.Value);
            DropDownList ddltargetEdit = GridView1.Rows[e.RowIndex].FindControl("ddltargetEdit") as DropDownList;
            if (ddltargetEdit.Visible)
            {
                mysysItem.systemtargetid = Convert.ToInt32(ddltargetEdit.SelectedItem.Value);
            }
            else
            {
                mysysItem.systemtargetid = -1;
            }
            db.SaveChanges();
            Response.Redirect(Request.Url.AbsolutePath + "?id=" + PROJECT_ID);
            }
            catch (Exception)
            {

                throw;
            }
           
        }


        protected void DeleteWorkItem(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int workItemId = (int)GridView1.DataKeys[e.RowIndex].Value;
                WorkItem wrk = db.WorkItems.Where(s => s.id == workItemId).First();
                db.WorkItems.Remove(wrk);
                workitemsystem wrkItemsys = db.workitemsystems.Where(s => s.workitemid == workItemId).First();
                db.workitemsystems.Remove(wrkItemsys);
                db.SaveChanges();

            }
            catch (Exception)
            {

                throw;
            }
           
                Response.Redirect(Request.Url.AbsolutePath + "?id=" + PROJECT_ID);
          
           
        }


       
      

        protected void btnEditProjectInfo_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProjectGeneralInfo.aspx?id="+ PROJECT_ID);
        }

        protected void btnEstimate_Click(object sender, EventArgs e)
        {
            string projectId = ddlProjects.SelectedValue;
            //if (projectId == string.Empty || projectId == "-1")
            //{
            //    throw new Exception("Select project from list");
            //}
            if (Page.IsValid)
            {
                Response.Redirect(HttpContext.Current.Request.Url.Scheme
                                    + "://"
                                    + HttpContext.Current.Request.Url.Authority
                                    + HttpContext.Current.Request.ApplicationPath + "Estimator/EstimateResult.aspx?id=" + projectId);   
            }
            
        }

        protected void ddlProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblErGrid.Text = string.Empty;
            //BindTypeDropDownList();
            fillFormAfterProjectSelected(ddlProjects.SelectedValue);

        }

        private void fillFormAfterProjectSelected(string projectId)
        {
            lblerr.Visible = false;
            try
            {
                string selectedProjectId = projectId;

                if (selectedProjectId == "-1")
                {
                    PROJECT_ID = "-1";
                  //  ClearForm();
                    Response.Redirect(Request.RawUrl);
               
                }
                else
                {
                    
                    PROJECT_ID = selectedProjectId;
                    Session["PROJECT_ID"] = PROJECT_ID;
                    BindWorkItemGridView();
                }
            }
            catch (Exception ex)
            {
                lblerr.Visible = true;
                lblerr.Text = ex.Message;
                // TODO: handle exception
                ClearForm();
            }
        }

        private void ClearForm()
        {
            GridView1.DataSource = null;
            GridView1.DataBind();

            ddlProjects.Items[0].Selected = true;
           


        }
        void SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }


        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName=="Addsys")
            {
                sysdiv.Visible = true;
             
            
            }
       
            if (e.CommandName=="Add")
            {
                      
                
                WorkItem newWrkItem = new WorkItem();
                lblErGrid.Text = "";
                try
                {
                 //Add Work Item
                    newWrkItem.projectId = Convert.ToInt32(PROJECT_ID);
                    newWrkItem.description = (GridView1.FooterRow.FindControl("txtDescriptionAdd") as TextBox).Text;
                    TextBox txtQuantityAdd = (TextBox)GridView1.FooterRow.FindControl("txtQuantityAdd");
                    if (txtQuantityAdd.Text.Trim() == "")
                    {
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Please Enter Quantity');", true);
                        lblErGrid.Text = "Please Enter Quantity";
                        return;
                    }

                    newWrkItem.quantity = Convert.ToInt32((GridView1.FooterRow.FindControl("txtQuantityAdd") as TextBox).Text);

                    DropDownList ddlProjectTypeAdd = (DropDownList)(GridView1.FooterRow.FindControl("ddlProjectTypeAdd"));
                    if (ddlProjectTypeAdd.SelectedValue == "-1")
                    {
                       // Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Please Select a type');", true);
                        lblErGrid.Text = "Please Select a type";
                        return;
                      
                    }
                    newWrkItem.typeId = Convert.ToInt32((GridView1.FooterRow.FindControl("ddlTypeAdd") as DropDownList).SelectedValue);
                    DropDownList ddlTypeAdd = GridView1.FooterRow.FindControl("ddlTypeAdd") as DropDownList;

                    if (ddlTypeAdd.SelectedValue == "-1")
                    {
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Please specify item complexity');", true);
                        lblErGrid.Text = "Please specify Project Type";
                        return;

                    }

                    DropDownList ddlComplexityAdd = GridView1.FooterRow.FindControl("ddlComplexityAdd") as DropDownList;

                    if (ddlComplexityAdd.SelectedValue == "-1")
                    {
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Please specify item complexity');", true);
                        lblErGrid.Text = "Please specify item complexity";
                        return;
                   
                    }
                    newWrkItem.projectType = Convert.ToInt32((GridView1.FooterRow.FindControl("ddlProjectTypeAdd") as DropDownList).SelectedValue);
                    newWrkItem.complexityId = Convert.ToInt32((GridView1.FooterRow.FindControl("ddlComplexityAdd") as DropDownList).SelectedValue);
                    newWrkItem.isNew = (bool)(GridView1.FooterRow.FindControl("chkIsNewAdd") as CheckBox).Checked;
                    newWrkItem.inScope = (bool)(GridView1.FooterRow.FindControl("chkInScopeAdd") as CheckBox).Checked;
                    workitemsystem newSys = new workitemsystem();
                    DropDownList ddlsourceAdd = (DropDownList)(GridView1.FooterRow.FindControl("ddlsourceAdd"));
                    if (ddlsourceAdd.SelectedValue == "-1")
                    {
                       // Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Please specify item source');", true);
                        lblErGrid.Text = "Please specify item source";
                        return;
                    }
                    newSys.systemsourceid = Convert.ToInt32((GridView1.FooterRow.FindControl("ddlsourceAdd") as DropDownList).SelectedValue);

                    DropDownList ddltargetAdd = (DropDownList)GridView1.FooterRow.FindControl("ddltargetAdd");


                    if (ddltargetAdd.Visible  & ddltargetAdd.SelectedValue == "-1")
                    {
                       // Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Please specify item target');", true);
                        lblErGrid.Text = "Please specify item target";
                        return;
                    }
                    db.WorkItems.Add(newWrkItem);
                    db.SaveChanges();
                    //Add Work Item System
                   
                    newSys.workitemid = newWrkItem.id;
                    newSys.systemtargetid = Convert.ToInt32((GridView1.FooterRow.FindControl("ddltargetAdd") as DropDownList).SelectedValue);

                    db.workitemsystems.Add(newSys);
                    db.SaveChanges();

                }
        
                catch (Exception)
                {
                    //Role back
                    db.WorkItems.Remove(newWrkItem);
                    db.SaveChanges();
                }
             //   Response.Redirect("../Estimator/EstimateResult.aspx/?id=" + PROJECT_ID);
                Response.Redirect(Request.Url.AbsolutePath + "?id=" + PROJECT_ID);
            }
           
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
          
            //List<GridItem> GridSource = (List<GridItem>)GridView1.DataSource;
            //switch (e.SortExpression)
            //    {
            //        case "Type":
            //             GridSource.OrderBy(x => x.name);
            //            break;
            //        case "Description":
            //            GridSource.OrderBy(x => x.description);
            //            break; 
            //        case "Complexity":
            //            GridSource.OrderBy(x => x.complexity);
            //            break;
            //    }
            //GridView1.DataSource = GridSource;
            //GridView1.DataBind();
          
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblsyserr.Text = "";
            int i_d = (int)Session["id"];
         if (!String.IsNullOrEmpty(txtSys.Text))
         {
            if (db.Systemees.Where(s => s.name == txtSys.Text & s.client_id == i_d).Count() == 0)
            {

                try
                {
                    Systemee sys = new Systemee();
                    sys.name = txtSys.Text;
                    sys.client_id = (int)Session["id"];
                    db.Systemees.Add(sys);
                    db.SaveChanges();
                    DropDownList ddlsourceAdd = (DropDownList)(GridView1.FooterRow.FindControl("ddlsourceAdd"));
                    ddlsourceAdd.DataTextField = "name";
                    ddlsourceAdd.DataValueField = "id";
                    ddlsourceAdd.DataSource = (from s in db.Systemees
                                               where s.client_id == client.ID
                                               select s).ToList();
                    ddlsourceAdd.DataBind();
                    ddlsourceAdd.Items.Insert(0, new ListItem("[select]", "-1"));
                    ddlsourceAdd.SelectedIndex = 0;
                    DropDownList ddltargetAdd = (DropDownList)(GridView1.FooterRow.FindControl("ddltargetAdd"));
                    ddltargetAdd.DataTextField = "name";
                    ddltargetAdd.DataValueField = "id";
                    ddltargetAdd.DataSource = (from s in db.Systemees
                                               where s.client_id == client.ID
                                               select s).ToList();
                    ddltargetAdd.DataBind();
                    ddltargetAdd.Items.Insert(0, new ListItem("[select]", "-1"));
                    ddltargetAdd.SelectedIndex = 0;
                    sysdiv.Visible = false;
                }
                catch (Exception ex)
                {

                    lblsyserr.Text = ex.Message;
                }
               
               
            }
            else
            {
                lblsyserr.Text = "The System Already Exist.";
            }
         }
         else
	        {
                lblsyserr.Text = "Please Enter System Name.";
	        }

        }

        protected void Btncancel_Click(object sender, EventArgs e)
        {
            lblsyserr.Text = "";
            sysdiv.Visible = false;
        }

        //protected void btnexcel_Click(object sender, EventArgs e)
        //{
           
        //    string strFilename = "EmpDetails.xls";
        //    string attachment = "attachment; filename=" + strFilename;
        //    Response.ClearContent();
        //    Response.AddHeader("content-disposition", attachment);
        //    Response.ContentType = "application/vnd.ms-excel";
        //    string tab = string.Empty;
        //    int prid = Convert.ToInt32(PROJECT_ID);
        //    var query = (from s in db.WorkItems
        //                 join p in db.WorkItemTypes on s.typeId equals p.id
        //                 join q in db.WorkItemComplexities on s.complexityId equals q.id
        //                 where s.projectId == prid
        //                 select new { p.name, s.id, q.complexity, s.description, s.quantity, p.systemable, s.isNew, s.inScope }).ToList();

            
        //    Response.Write(tab + "Type");
        //    tab = "\t";
        //    Response.Write(tab + "Complexity");
        //    tab = "\t";
        //    Response.Write(tab + "Description");
        //    tab = "\t";
        //    Response.Write(tab + "Quantity");
        //    tab = "\t";
        //    Response.Write(tab + "New ?");
        //    tab = "\t";
        //    Response.Write(tab + "In Scope ?");
        //    tab = "\t";
        //    Response.Write(tab + "Source System");
        //    tab = "\t";
        //    Response.Write(tab + "Target  System");
        //    tab = "\t";
        //    Response.Write("\n");
        //    foreach (var item in query)
        //    {
        //            tab = "";
        //       Response.Write(tab + item.name);
        //        tab = "\t";
        //        Response.Write(tab + item.complexity);
        //         tab = "\t";
        //         Response.Write(tab + item.description);
        //         tab = "\t";
        //         Response.Write(tab + item.quantity);
        //         tab = "\t";
        //         Response.Write(tab + item.isNew);
        //         tab = "\t";
        //         Response.Write(tab + item.inScope);
        //         tab = "\t";
        //        int s_id=db.workitemsystems.Where(s => s.workitemid == item.id).Select(s => s.systemsourceid).First();
        //         Response.Write(tab + db.Systemees.Where(s => s.id == s_id).Select(s => s.name).First())  ;
        //         tab = "\t";
        //         int t_id = db.workitemsystems.Where(s => s.workitemid == item.id).Select(s => s.systemtargetid).First();
        //         if (t_id!=-1)
        //         {
        //             Response.Write(tab + db.Systemees.Where(s => s.id == t_id).Select(s => s.name).First());
                     
        //         }
        //         tab = "\t";
        //         Response.Write("\n");
        //    }
           
      
        //    Response.End();
           
        //}

        //protected void DDLTypes_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindWorkItemGridView();
        //}

      
      

        
        
    }
}