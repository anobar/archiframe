﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportSys.aspx.cs" Inherits="ArchiFrame.Estimator.ReportSys" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="leftnav">
               
                  <ul class="menu" >
		            <li class="item1" ><a  href="/Default.aspx"> Home</a>
		
		            </li>
		            <li class="item2"  >  <a  href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a  href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			           
                             </ul>
		            </li>
		          <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a  class="active" href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a  class="selected" href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                 <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>

                      </ul>
		            </li>  
		          <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  

	               </ul>
          </div>
     <div class="rightnav">
     <div class="reg">
                 <div class="body_header_style">
                      Report - Work Items 
                 </div>
         
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                     <ContentTemplate>
     <div class="Wholediv">
                 Select project :

                         <asp:DropDownList ID="ddlProjects" runat="server" AutoPostBack="true" class="drop_down_list_style" OnSelectedIndexChanged="ddlProjects_SelectedIndexChanged">
                         </asp:DropDownList>
                
   
      <asp:CustomValidator id="CustomValidatorddlProject" runat="server" 
                      OnServerValidate="DdlValidator" 
                      ControlToValidate="ddlProjects" 
                      ErrorMessage="Please Select a Project."
                     CssClass="field_validation_error">
                    </asp:CustomValidator>
         </div>
      <div class="leftdiv">
               Work item types :
                 <asp:CheckBoxList ID="ChkProjectType" runat="server" ></asp:CheckBoxList>
             </div>
         <div class="rightdiv" >
             Systems:
             <asp:CheckBoxList ID="ChkProjectSys" runat="server"></asp:CheckBoxList>
          </div>
      
      </div>   
              </ContentTemplate>
                 </asp:UpdatePanel>

           <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                     <ContentTemplate>
                      
          <div class="Wholediv">
             <asp:Button ID="btnsubmit" runat="server"  Text="Submit" CssClass="cssbutton" OnClick="btnsubmit_Click"/>
           
                <asp:GridView ID="repGrid" runat="server"  CssClass="mGrid" AllowSorting="True" AutoGenerateColumns="False"  DataKeyNames = "id" CellPadding="4" ForeColor="#333333" GridLines="None" ShowFooter="True">
        <AlternatingRowStyle BackColor="White" />
       
        <Columns>
           <%--   <asp:TemplateField HeaderText="TYPE"  SortExpression="Type" >
                    <ItemTemplate>
                    <asp:Label ID="lblWorkItemprojectType" runat="server" Text='<%# Eval("ProjectType")%>'></asp:Label>
                </ItemTemplate>
                         
               
              </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="TYPE"  SortExpression="Type" >
                    <ItemTemplate>
                    <asp:Label ID="lblWorkItemType" runat="server" Text='<%# Eval("name")%>'></asp:Label>
                </ItemTemplate>
                         
               
              </asp:TemplateField>
            

            <asp:TemplateField HeaderText="COMPLEXITY" SortExpression="Complexity" >
                    <ItemTemplate>
                    <asp:Label ID="lblWorkItemComplexity" runat="server" Text='<%# Eval("complexity")%>'></asp:Label>
                </ItemTemplate>
                       
               
            </asp:TemplateField>

            <asp:TemplateField HeaderText="DESCRIPTION" SortExpression="Description">
                    <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("description")%>'></asp:Label>
                </ItemTemplate>

                
            </asp:TemplateField>

            <asp:TemplateField HeaderText="QUANTITY" >
                    <ItemTemplate>
                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("quantity")%>'></asp:Label>
                </ItemTemplate>
                        
                
            </asp:TemplateField>            
            <asp:TemplateField HeaderText="New" >
                    <ItemTemplate>
                    <asp:CheckBox ID="lblIsNew" runat="server" Enabled="false" Checked='<%# Eval("IsNew")%>'></asp:CheckBox>
                </ItemTemplate>
            
            </asp:TemplateField>

               <asp:TemplateField HeaderText="InScope" >
                    <ItemTemplate>
                    <asp:CheckBox ID="lblInScope" runat="server" Enabled="false" Checked='<%# Eval("InScope")%>'></asp:CheckBox>
                </ItemTemplate>
                        
              
            </asp:TemplateField>
               <asp:TemplateField HeaderText="SOURCE" >

           <ItemTemplate>
                    <asp:Label ID="lblSource" runat="server" Text='<%# Eval("sourcename")%>'></asp:Label>
                </ItemTemplate>
                        
                 

                    </asp:TemplateField>
            <asp:TemplateField HeaderText="TARGET" >
                <ItemTemplate>
                    <asp:Label ID="lbltarget" runat="server" Text='<%# Eval("targetname")%>'></asp:Label>
                </ItemTemplate>
          

                    </asp:TemplateField>
        

          <%--  <asp:CommandField ShowEditButton="True" ShowDeleteButton="true" />--%>
        </Columns>
       <EditRowStyle BackColor="#2461BF" />
   <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#C8c8c8" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True"  />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />

    </asp:GridView>
             </div>  
                </ContentTemplate>
                 </asp:UpdatePanel>
                   
     </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
</asp:Content>
