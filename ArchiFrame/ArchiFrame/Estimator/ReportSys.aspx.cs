﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using ArchiFrame.Class;

namespace ArchiFrame.Estimator
{
    public partial class ReportSys : System.Web.UI.Page
    {
        static string PROJECT_ID;
        private ArchiFrameEntities db = new ArchiFrameEntities();
        private Guid id;
       Client client = new Client();
        protected void Page_Load(object sender, EventArgs e)
        {
          
          
            if (!IsPostBack)
            {

              
                //this.BindWorkItemGridView();
                MembershipUser user = Membership.GetUser();
                string UserID = user.ProviderUserKey.ToString();
                id = Guid.Parse(UserID);
                client.ID = (from s in db.User_Profile
                             where s.UserId == id
                             select s.UserCompany_Id).First();
                Session["id"] = client.ID;
                this.BindProjectDropDownList();
              
            }
            else
            {
               
            }

        }
        protected void DdlValidator(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (args.Value != "-1");
        }
        private void BindProjectDropDownList()
        {
            var queryddlProject = (from s in db.Projects
                                   where s.clientId == client.ID
                                   select new { s.id, s.name });
            List<itemFordpl> listItemddlProjects = new List<itemFordpl>();

            foreach (var x in queryddlProject)
            {
                itemFordpl li = new itemFordpl();
                li.ID = x.id;
                li.Name = x.name;
                listItemddlProjects.Add(li);
            }

            itemFordpl temp = new itemFordpl();
            temp.ID = -1;
            temp.Name = "[Select]";
            listItemddlProjects.Add(temp);
            ddlProjects.DataSource = listItemddlProjects;
            ddlProjects.DataTextField = "Name";
            ddlProjects.DataValueField = "ID";
            ddlProjects.DataBind();
            ddlProjects.SelectedValue = "-1";



        }
        private void BindProjectType()
        {
            if (!String.IsNullOrEmpty(PROJECT_ID))
            {

              int p_id =Convert.ToInt32(PROJECT_ID);
              var queryddlProjectType = (from s in db.WorkItems
                                         join p in db.Ref_WorkItemType on s.typeId equals p.id
                                       where s.projectId == p_id
                                   select new { s.typeId, p.name }).Distinct().ToList();

              ChkProjectType.DataSource = queryddlProjectType;
              ChkProjectType.DataTextField = "name";
              ChkProjectType.DataValueField = "typeId";
              ChkProjectType.DataBind();
              ChkProjectType.ClearSelection();
          
            }
          

        }

        private void BindSystems()
        {
            if (!String.IsNullOrEmpty(PROJECT_ID))
            {

                int p_id = Convert.ToInt32(PROJECT_ID);
                var queryddlProjectType = (from s in db.WorkItems
                                           join p in db.workitemsystems on s.id equals p.workitemid
                                            join q in db.Systemees on  p.systemsourceid equals q.id 
                                            where s.projectId == p_id
                                           select new { q.id, q.name }).Union(from s in db.WorkItems
                                            join p in db.workitemsystems on s.id equals p.workitemid
                                            join q in db.Systemees on p.systemtargetid equals q.id
                                            where s.projectId == p_id
                                            select new { q.id, q.name }) .Distinct().ToList();
              
                ChkProjectSys.DataSource = queryddlProjectType;
                ChkProjectSys.DataTextField = "name";
                ChkProjectSys.DataValueField = "id";
                ChkProjectSys.DataBind();
                ChkProjectSys.ClearSelection();
            }
       


        }

        protected void ddlProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlProjects.SelectedValue!="-1")
            {
                PROJECT_ID = ddlProjects.SelectedValue;
                BindProjectType();
                foreach(ListItem li in ChkProjectType.Items)
                {
                    li.Selected = true;
                }

                BindSystems();
                foreach(ListItem li in ChkProjectSys.Items)
                {
                    li.Selected = true;

                }

                repGrid.DataSource = null;
                repGrid.DataBind();
               
            }
            else
            {
                ChkProjectType.DataSource = null;
                ChkProjectType.DataBind();
                ChkProjectType.Items.Clear();
               ChkProjectSys.DataSource = null;
                ChkProjectSys.DataBind();
                ChkProjectSys.Items.Clear();
                repGrid.DataSource = null;
                repGrid.DataBind();
            }
            
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
         
            List<int> ListTypes = new List<int>();
            List<int> ListSystems = new List<int>();
                List<GridItem> GridSource = new List<GridItem>();
            foreach (ListItem ItemType in ChkProjectType.Items)
	            {
                if (ItemType.Selected)
	                {
		               ListTypes.Add(Convert.ToInt32(ItemType.Value));
	                }
		 
	            }
            foreach (ListItem ItemType in ChkProjectSys.Items)
            {
                if (ItemType.Selected)
                {
                    ListSystems.Add(Convert.ToInt32(ItemType.Value));
                }

            }
              int p_id = Convert.ToInt32(PROJECT_ID);
              var query=(from s in db.WorkItems
                         join p in db.Ref_WorkItemType on s.typeId equals p.id
               join q in db.WorkItemComplexities on s.complexityId equals q.id
               join m in db.workitemsystems on s.id equals m.workitemid
                where s.projectId == p_id &  ListTypes.Contains(p.id)
               select new { p.name , s.id, q.complexity, s.description, s.quantity, p.systemable, s.isNew, s.inScope,m.systemsourceid,m.systemtargetid}).ToList();
             foreach (var item in query)
              {
                  if (ListSystems.Contains(item.systemsourceid) || ListSystems.Contains(item.systemtargetid) )
                  {
                      GridItem newItem = new GridItem();
                      newItem.id = item.id;
                      newItem.name = item.name;
                      newItem.quantity = item.quantity;
                      newItem.description = item.description;
                      newItem.complexity = item.complexity;
                      newItem.sourceid = item.systemsourceid;
                      newItem.sourcename = db.Systemees.Where(s => s.id == newItem.sourceid).Select(s => s.name).First();
                      newItem.IsNew = (bool)item.isNew;
                      newItem.inScope = (bool)item.inScope;
                      newItem.targid = item.systemtargetid;
                      if (newItem.targid != -1)
                      {
                           newItem.targetname = db.Systemees.Where(s => s.id == newItem.targid).Select(s => s.name).First();;
                      }
                      else
                      {
                          newItem.targetname = "-------";
                      }
                      GridSource.Add(newItem);
                  }
                  repGrid.DataSource = GridSource;
                  repGrid.DataBind();
                
             }
        }
    }
}