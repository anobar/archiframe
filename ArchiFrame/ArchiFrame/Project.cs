//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ArchiFrame
{
    using System;
    using System.Collections.Generic;
    
    public partial class Project
    {
        public Project()
        {
            this.DeploymentEnvironments = new HashSet<DeploymentEnvironment>();
            this.ProjectScopes = new HashSet<ProjectScope>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<int> parnetProjectId { get; set; }
        public int clientId { get; set; }
        public Nullable<int> projectTypeId { get; set; }
        public Nullable<int> complexityId { get; set; }
        public Nullable<int> resourcesSkillLevelId { get; set; }
        public Nullable<int> teamStructureId { get; set; }
        public Nullable<byte> contingencyPercent { get; set; }
        public Nullable<System.DateTime> startDate { get; set; }
        public Nullable<System.DateTime> endDate { get; set; }
        public Nullable<short> numberOfEnvironments { get; set; }
    
        public virtual ICollection<DeploymentEnvironment> DeploymentEnvironments { get; set; }
        public virtual ICollection<ProjectScope> ProjectScopes { get; set; }
    }
}
