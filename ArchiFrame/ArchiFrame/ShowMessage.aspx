﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShowMessage.aspx.cs" Inherits="ArchiFrame.ShowMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
       <div id="leftnav">
               
                  <ul class="menu" >
		            <li class="item1"   ><a  href="/Default.aspx" > Home</a>
		
		            </li>
		            <li class="item2" >  <a href="/Estimator/ProjectGeneralInfo.aspx" >Estimate </a>
			            <ul >
				            <li class="subitem1"  ><a href="/Estimator/ProjectGeneralInfo.aspx">General Information </a></li>
			                 <li class="subitem1" ><a href="/Estimator/ProjectWorkItems.aspx">Work Items</a></li>
				        <li class="subitem1" ><a href="/Estimator/EstimateResult.aspx">Estimate results</a></li>
			          
                        </ul>
		            </li>
		            <li class="item3"  ><a href="/Account/Manage.aspx"">Profile </a>
		                   <ul >
		       	<li class="subitem1"  ><a href="/Account/Manage.aspx">Change Password </a></li>
                      </ul>
		            </li>
		        <li class="item4"  ><a href="/Estimator/ReportSys.aspx">Report </a>
		                   <ul >
		         <li class="subitem1" ><a href="/Estimator/ReportSys.aspx" >Work Items</a></li>
                 <li class="subitem1" ><a href="/Estimator/ExportToExcel.aspx" >Export Project To Excel</a></li>
                                 <li class="item5"  ><a href="#">Setting </a>
                  <ul >
		              <li class="subitem1" ><a href="/Estimator/ManageProject.aspx" >Manage Project Templates</a></li>
                      <li class="subitem1" ><a href="#" >Systems</a></li>
                      </ul>
		            </li>  
                      </ul>
		            </li>  
		        

	               </ul>
          </div>

    <div class="rightnav"  >
        <div id="middlenav"> 

            <asp:Label ID="lblMessage" runat="server" Text="Label"></asp:Label>
              </div>
        
        </div>
</asp:Content>
<asp:Content runat="server" ID="footerContent" ContentPlaceHolderID="Footer">
    </asp:Content>