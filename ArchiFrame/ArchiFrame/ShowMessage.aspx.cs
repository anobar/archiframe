﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArchiFrame
{
    public partial class ShowMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["msg"].ToString()=="1")
            {
                lblMessage.Text = " Your registration has been completed.An activation link has been sent to your email. ";
            }
            else
            {
                lblMessage.Text = " Your new password  has been  sent to your email. ";
            }
        }
    }
}