﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ArchiFrame.Class
{
    public static class CommonStatic
    {
        public static string CnnString = ConfigurationManager.ConnectionStrings["ArchiFrameConnectionString"].ConnectionString;
        public static string GET_ALL_PROJECT_BY_CLIENT_QUERY = "SELECT [id],[name],[parnetProjectId],[clientId],[projectTypeId],[complexityId],[resourcesSkillLevelId],[teamStructureId],[contingencyPercent],[startDate],[endDate] FROM [Project] WHERE clientId=@clientId";
        public static string GET_ALL_PROJECT_BY_PROJECT_ID_QUERY = "SELECT [id],[name],[parnetProjectId],[clientId],[projectTypeId],[complexityId],[resourcesSkillLevelId],[teamStructureId],[contingencyPercent],[startDate],[endDate] FROM [Project] WHERE id=@id";
        public static string GET_ALL_REF_PROJECT_TYPE_QUERY = "SELECT [id],[name] FROM [Ref_ProjectType]";
        public static string GET_ALL_REF_PROJECT_COMPLEXITY_QUERY = "SELECT [id],[name] FROM [Ref_ProjectComplexity]";
        public static string GET_ALL_REF_RESOURCES_SKILL_LEVEL_QUERY = "SELECT [id],[name] FROM [Ref_ResourcesSkillLevel]";
        public static string GET_ALL_REF_TEAM_STRUCTURE_QUERY = "SELECT [id],[name] FROM [Ref_TeamStructure]";
        public static string GET_ALL_REF_PROJECT_SCOPE_QUERY = "SELECT [id],[name] FROM [Ref_ProjectScope]";
        public static string GET_ALL_REF_DEPLOYMENT_ENVIRONMENT_QUERY = "SELECT [id],[name] FROM [Ref_DeploymentEnvironment]";
        public static string GET_ALL_PROJECT_SCOPE_BY_PROJECT_QUERY = "SELECT [id],[projectId],[projectScopeId] FROM ProjectScope WHERE projectId=@projectId";
        public static string GET_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_QUERY = "SELECT [id],[projectId], [environmentId] FROM [DeploymentEnvironment] WHERE projectId=@projectId";
        public static string ADD_PROJECT_QUERY = "INSERT INTO [dbo].[Project] ([name],[parnetProjectId],[clientId],[projectTypeId],[complexityId],[resourcesSkillLevelId],[teamStructureId],[contingencyPercent]) VALUES (@name, @parnetProjectId, @clientId, @projectTypeId, @complexityId, @resourcesSkillLevelId, @teamStructureId, @contingencyPercent); SELECT CAST(scope_identity() AS int)";
        public static string ADD_PROJECT_SCOPE_QUERY = "INSERT INTO [dbo].[ProjectScope] ([projectId], [projectScopeId]) VALUES (@projectId, @projectScopeId)";
        public static string ADD_DEPLOYMENT_ENVIRONMENT_QUERY = "INSERT INTO [dbo].[DeploymentEnvironment] ([projectId], [environmentId]) VALUES (@projectId, @environmentId)";
        public static string UPDATE_PROJECT_QUERY = "UPDATE [Project] SET [name] = @name, [projectTypeId] = @projectTypeId, [complexityId] = @complexityId, [resourcesSkillLevelId] = @resourcesSkillLevelId, [teamStructureId] = @teamStructureId ,[contingencyPercent] = @contingencyPercent WHERE id=@id";
        public static string DELETE_ALL_PROJECT_SCOPE_BY_PROJECT_ID_QUERY = "DELETE FROM [ProjectScope] WHERE projectId=@projectId";
        public static string DELETE_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_ID_QUERY = "DELETE FROM [DeploymentEnvironment] WHERE projectId=@projectId";
        public static string DELETE_WORK_ITEM_QUERY = "Delete from WorkItem where id=@id";
        public static string GET_ALL_WORK_ITEM_COMPLEXITY_QUERY = "SELECT [id], [complexity] FROM [WorkItemComplexity]";
        public static string GET_ALL_WORK_ITEM_TYPE_QUERY = "SELECT [name], [id] FROM [WorkItemType]";
        public static string GET_ALL_JOINT_WORK_ITEM_QUERY = "select WorkItemType.name, WorkItemComplexity.complexity, WorkItem.id, WorkItem.description, Workitem.quantity from WorkItem inner join WorkItemType on WorkItemType.id = WorkItem.typeId inner join WorkItemComplexity on WorkItemComplexity.id = WorkItem.complexityId WHERE projectId=@projectId";
        public static string UPDATE_WORK_ITEM_QUERY = "UPDATE WorkItem SET typeId = @typeId, complexityId = @complexityId, description=@description, quantity=@quantity where id=@id";
        public static string ADD_WORK_ITEM_QUERY = "INSERT INTO WorkItem  ([ProjectId] ,[typeId] ,[complexityId], [description],[quantity]) VALUES (@projectId, @typeId, @complexityId, @description, @quantity);";
        public static string ADD_CLIENT_QUERY   = "INSERT INTO [dbo].[Client] ([companyName]) VALUES (@companyName); SELECT CAST(scope_identity() AS int)";
        public static string UPDATE_USERS_QUERY = "UPDATE Users SET FirstName=@firstName, LastName=@LastName, clientId= @clientId where UserId=@userId";



        public static DataTable GetData(SqlCommand cmd)
        {
            string strConnString = CommonStatic.CnnString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        return dt; //asdf

                    }
                }
            }
        }

    }
}