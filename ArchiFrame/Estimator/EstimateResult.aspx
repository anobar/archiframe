﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EstimateResult.aspx.cs" Inherits="ArchiFrame.Estimator.EstimateResult" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
    <div class="body_header_style">
        Estimation Result
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../StyleSheets/EstimateResultStyleSheet.css" rel="stylesheet" />

    <div class="body_content_style">

        <table id="estimate_table" >
            <tr id="estimate_info">
                <td >
                    Total Estimate (incl. Contingency): &nbsp; <b><asp:Label ID="lblTotalEstimateDaysVal" runat="server"></asp:Label> </b> &nbsp; Days
                    &nbsp; &nbsp; &nbsp; 
                    Contingency: &nbsp; <b> <asp:Label ID="lblContingencyPercent" runat="server" CssClass="label_style"></asp:Label> </b> %
                    <br />
                    Total Estimate (without contingency): &nbsp; <b><asp:Label ID="lblTotalEstimateWithoutContingency" runat="server"></asp:Label> </b> &nbsp; Days

                </td>
                <td>
                    <asp:TreeView ID="trvDetailedEstimate" runat="server" CssClass="tree_view_style" NodeStyle-CssClass="tree_view_style_node" RootNodeStyle-CssClass="tree_view_style_root"
                        ParentNodeStyle-CssClass="tree_view_style_parent" LeafNodeStyle-CssClass="tree_view_style_leaf" SelectedNodeStyle-CssClass="tree_view_style_selected" HoverNodeStyle-CssClass="tree_view_style_hover" ></asp:TreeView>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    
                </td>
            </tr>
            <tr id="project_info">
                <td>
                    Project Name: &nbsp; <b><asp:Label ID="lblProjectName" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Project Type: &nbsp; <b><asp:Label ID="lblProjectType" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Project Overal Complexity: &nbsp; <b><asp:Label ID="lblProjectOveralComplexity" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Resource Skill Set: &nbsp; <b><asp:Label ID="lblResourceSkill" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Team Structure: &nbsp; <b><asp:Label ID="lblTeamStructure" runat="server" CssClass="label_style"></asp:Label></b>
                </td>
                <td>
                    Scope: &nbsp; <b><asp:Label ID="lblProjectScope" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />
                    Deployment Environments: &nbsp; <b><asp:Label ID="lblDeploymentEnvironments" runat="server" CssClass="label_style"></asp:Label></b>
                    <br />

                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>
