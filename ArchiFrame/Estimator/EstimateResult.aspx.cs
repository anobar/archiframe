﻿using ArchiFrame.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArchiFrame.Estimator
{
    public partial class EstimateResult : System.Web.UI.Page
    {
        string PROJECT_ID = string.Empty;

        static string CnnString = ConfigurationManager.ConnectionStrings["ArchiFrameConnectionString"].ConnectionString;

        static string GET_ALL_REF_WORK_ITEM_TYPE_EFFORT_QUERY = "SELECT [id],[workItemTypeId], [workItemComplexityId], [isNew], [defaultEffort], [customEffort] FROM [dbo].[Ref_WorkItemTypeEffort]";
        static string GET_ALL_WORK_ITEM_BY_PROJECT_ID_QUERY = "SELECT [id] ,[projectId], [typeId],[complexityId],[description],[quantity],[inScope],[isNew] FROM [dbo].[WorkItem] WHERE [projectId]=@projectId";
        static string GET_ALL_PROJECT_SCOPE_BY_PROJECT_QUERY = "SELECT [id], [projectId], [projectScopeId] from [dbo].[ProjectScope] WHERE projectId = @projectId";
        static string GET_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_QUERY = "SELECT [id],[projectId], [environmentId] FROM [DeploymentEnvironment] WHERE projectId=@projectId";
        static string GET_ALL_REF_PROJECT_SCOPE_QUERY = "SELECT [id], [name] from [dbo].[Ref_ProjectScope]";
        static string GET_PROJECT_BY_ID_QUERY = "SELECT * from [dbo].[Project] WHERE [id]=@projectId";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                // TODO: uncomment following to apply authentication

                //if (!Request.IsAuthenticated)
                //{
                //    Response.Redirect("~/Account/Login?returnUrl="
                //        + Request.Url.LocalPath);
                //}

                //TODO: Fix projectid
                PROJECT_ID = Request.QueryString.Get("id");
                if (string.IsNullOrEmpty(PROJECT_ID))
                {
                    lblTotalEstimateDaysVal.Text = "No project has been selected; please try again.";
                    return;
                }

                FillProjectInfo();
                FillEstimate();

                // this is a test
                //TreeNode nd = new TreeNode("Root");
                //trvDetailedEstimate.Nodes.Add(nd);

                //nd = new TreeNode("parent 1");
                //trvDetailedEstimate.Nodes[0].ChildNodes.Add(nd);

                //nd = new TreeNode("parent 2");
                //trvDetailedEstimate.Nodes[0].ChildNodes.Add(nd);

                //nd = new TreeNode("child 1");
                //trvDetailedEstimate.Nodes[0].ChildNodes[1].ChildNodes.Add(nd);

                

            }
            catch (Exception ex)
            {
 
            }
            
        }

        private void FillEstimate()
        {      float totalWorkItemsEffort = 0;

            // Get all WorkItemTypeEffort
            string RefWorkItemTypeEffortQuery = GET_ALL_REF_WORK_ITEM_TYPE_EFFORT_QUERY;
            SqlCommand cmd = new SqlCommand(RefWorkItemTypeEffortQuery);
            DataTable RefWorkItemTypeEfforttDT = CommonStatic.GetData(cmd);

   

            string strConnString = CnnString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                string query = GET_ALL_WORK_ITEM_BY_PROJECT_ID_QUERY;
                using (SqlCommand getAllWorkItemByProjectCmd = new SqlCommand(query))
                {
                    // Get All WorkItems for the current project
                    getAllWorkItemByProjectCmd.Connection = con;
                    getAllWorkItemByProjectCmd.Parameters.AddWithValue("@projectId", PROJECT_ID);
                    con.Open();
                    SqlDataReader dataReader = getAllWorkItemByProjectCmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        IDataRecord ii = (IDataRecord)dataReader;
                        string typeId = ii["typeId"].ToString();
                        string complexityId = ii["complexityId"].ToString();
                        string isNew = ii["isNew"].ToString();
                        string quantity = ii["quantity"].ToString();

                        foreach (DataRow dr in RefWorkItemTypeEfforttDT.Rows)
                        {
                            string workItemTypeId = dr["workItemTypeId"].ToString();
                            string workItemComplexityId = dr["workItemComplexityId"].ToString();
                            string workItemIsNew = dr["isNew"].ToString();
                            string defaultEffort = dr["defaultEffort"].ToString();


                            if (workItemTypeId == typeId && workItemComplexityId == complexityId && workItemIsNew == isNew)
                            {
                                totalWorkItemsEffort += float.Parse(defaultEffort) * float.Parse(quantity);
                                break;
                            }
                        }

                    }
                    con.Close();
                }

                // TODO: project scope is now calculated flat, need to make it more sophisticated by applying proper rules

                // Get Scope of current project
                SqlCommand getProjectScopeCmd = new SqlCommand(GET_ALL_PROJECT_SCOPE_BY_PROJECT_QUERY);
                getProjectScopeCmd.Parameters.AddWithValue("@projectId", PROJECT_ID);
                DataTable projectScopeDT = CommonStatic.GetData(getProjectScopeCmd);



                // Get [Ref_ProjectScope]
                SqlCommand getRefProjectScopeCmd = new SqlCommand(GET_ALL_REF_PROJECT_SCOPE_QUERY);
                SqlConnection refCon = new SqlConnection(CnnString);
                refCon.Open();
                getRefProjectScopeCmd.Connection = refCon;
                SqlDataReader getRefProjectScopeDR = getRefProjectScopeCmd.ExecuteReader();

                float scopeFactor = 0;

                //1	Planning and Scoping
                //2	Requirement Gathering and Analysis
                //3	Architecture and Design
                //4	Build
                //5	Test
                //6	Deployment
                //7	Project Management
                //8	Change Management
                //9	Training and Service Introduction

                // Scope Factor Constants:
                const float planning_factor = 5;
                const float requirement_gathering_and_analysis_factor = 10;
                const float architecture_and_design_factor = 15;
                const float build_factor = 20;
                const float test_factor = 15;
                float deployment_factor = 0; // deployment_factor will be updated below
                const float project_management_factor = 15;
                const float change_management_factor = 5;
                const float training_and_service_intro_factor = 5;



                List<string> inScopeTempList = new List<string>();
                

                while (getRefProjectScopeDR.Read())
                {

                    foreach (DataRow scopeRow in projectScopeDT.Rows)
                    {
                        if (scopeRow["projectScopeId"].ToString() == getRefProjectScopeDR["id"].ToString())
                        {
                            switch (scopeRow["projectScopeId"].ToString())
                            {
                                case "1": // Planning and Scoping
                                    scopeFactor += planning_factor;
                                    inScopeTempList.Add("planning_and_scoping");
                                    break;
                                case "2": // Requirement Gathering and Analysis
                                    scopeFactor += requirement_gathering_and_analysis_factor;
                                    inScopeTempList.Add("requirement_gathering_and_analysis");
                                    break;
                                case "3": // Architecture and Design
                                    scopeFactor += architecture_and_design_factor;
                                    inScopeTempList.Add("architecture_and_design");
                                    break;
                                case "4": // Build
                                    scopeFactor += build_factor;
                                    inScopeTempList.Add("build");
                                    break;
                                case "5": // Test
                                    scopeFactor += test_factor;
                                    inScopeTempList.Add("test");
                                    break;
                                case "6": // Deployment
                                    // get number of deployment environments
                                    // Get DEPLOYMENT ENVIRONMENT
                                    SqlCommand getDeploymentEnvCmd = new SqlCommand(GET_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_QUERY);
                                    getDeploymentEnvCmd.Parameters.AddWithValue("@projectId", PROJECT_ID);
                                    DataTable getDeploymentEnvDT = CommonStatic.GetData(getDeploymentEnvCmd);
                                    deployment_factor = Convert.ToSingle(getDeploymentEnvDT.Rows.Count * 2.5);
                                    scopeFactor += deployment_factor;
                                    inScopeTempList.Add("deployment");
                                    break;
                                case "7": // Project Management
                                    scopeFactor += project_management_factor;
                                    inScopeTempList.Add("project_management");
                                    break;
                                case "8": // Change Mgmt
                                    scopeFactor += change_management_factor;
                                    inScopeTempList.Add("change_management");
                                    break;
                                case "9": // Training and Service Intro
                                    scopeFactor += training_and_service_intro_factor;
                                    inScopeTempList.Add("training_and_service_intro");
                                    break;
                            }
                        }
                    }
                }


                refCon.Close();

                // calculate the total factor in relation to other in-scope items
                // inScopeList will be used later to calculate the break-down of total estimate for each scope item

                Dictionary<string, float> inScopeList = new Dictionary<string, float>();  //name-value pair of scope / total factor in relation to other in-scope items

                foreach (string scopeItem in inScopeTempList)
                {                
                    switch (scopeItem)
                    {
                        case "planning_and_scoping":
                            inScopeList.Add("planning_and_scoping", (planning_factor * 100) / scopeFactor);
                            break;
                        case "requirement_gathering_and_analysis":
                            inScopeList.Add("requirement_gathering_and_analysis", (requirement_gathering_and_analysis_factor * 100) / scopeFactor);
                            break;
                        case "architecture_and_design":
                            inScopeList.Add("architecture_and_design", (architecture_and_design_factor * 100) / scopeFactor);
                            break;
                        case "build":
                            inScopeList.Add("build", (build_factor * 100) / scopeFactor);
                            break;
                        case "test":
                            inScopeList.Add("test", (test_factor * 100) / scopeFactor);
                            break;
                        case "deployment":
                            inScopeList.Add("deployment", (deployment_factor * 100) / scopeFactor);
                            break;
                        case "project_management":
                            inScopeList.Add("project_management", (project_management_factor * 100) / scopeFactor);
                            break;
                        case "change_management":
                            inScopeList.Add("change_management", (change_management_factor * 100) / scopeFactor);
                            break;
                        case "training_and_service_intro":
                            inScopeList.Add("training_and_service_intro", (training_and_service_intro_factor * 100) / scopeFactor);
                            break;
                    }
                }

                totalWorkItemsEffort = (totalWorkItemsEffort * scopeFactor) / 100;

                // Get the details of the project
                SqlCommand getProjectbyIdCmd = new SqlCommand(GET_PROJECT_BY_ID_QUERY);
                getProjectbyIdCmd.Parameters.AddWithValue("@projectId", PROJECT_ID);
                DataTable projectDT = CommonStatic.GetData(getProjectbyIdCmd);

                string projectComplexity = projectDT.Rows[0]["complexityId"].ToString();
                // TODO: read the ID from the table or cache

                if (projectComplexity == "1")  // 1=Simple
                    totalWorkItemsEffort = (totalWorkItemsEffort * 75) / 100;  // bring the estimate 25% down
                else if (projectComplexity == "2") // 2=Medium = default
                    totalWorkItemsEffort = totalWorkItemsEffort;  // Do nothing
                else if (projectComplexity == "3")
                    totalWorkItemsEffort = (totalWorkItemsEffort * 125) / 100;  // push the estimate 25% up


                string projectTeamStructure = projectDT.Rows[0]["teamStructureId"].ToString();
                // TODO: read the ID from the table or cache

                if (projectTeamStructure == "1")  // 1=Single Site (default)
                    totalWorkItemsEffort = totalWorkItemsEffort;  // do nothing
                else if (projectTeamStructure == "2") // 2=Multi Site (All Onshore)
                    totalWorkItemsEffort = (totalWorkItemsEffort * 120) / 100;  // push the estimate 20% up
                else if (projectTeamStructure == "3") // 3= Multi Site (Onshore / Offshore)
                    totalWorkItemsEffort = (totalWorkItemsEffort * 135) / 100;  // push the estimate 35% up


                string projectResourceSkillLevel = projectDT.Rows[0]["resourcesSkillLevelId"].ToString();
                // TODO: read the ID from the table or cache

                if (projectResourceSkillLevel == "1")  // 1=Need Upskilling
                    totalWorkItemsEffort = (totalWorkItemsEffort * 125) / 100;  // push the estimate 25% up
                else if (projectResourceSkillLevel == "2") // 2=Average = default
                    totalWorkItemsEffort = totalWorkItemsEffort;  // Do nothing
                else if (projectResourceSkillLevel == "3")  // 3=Highly Skilled
                    totalWorkItemsEffort = (totalWorkItemsEffort * 75) / 100;  // pull the estimate 25% down

                lblTotalEstimateWithoutContingency.Text = totalWorkItemsEffort.ToString(); // Total estimate without contingency

                string projectContingencyPercent = projectDT.Rows[0]["contingencyPercent"].ToString();
                int perc = Convert.ToInt16(projectContingencyPercent) + 100;
                totalWorkItemsEffort = (totalWorkItemsEffort * perc) / 100;

                lblTotalEstimateDaysVal.Text = totalWorkItemsEffort.ToString(); // total estimate

                TreeNode nd = new TreeNode("Total: " + totalWorkItemsEffort.ToString());
                trvDetailedEstimate.Nodes.Add(nd);

                // Breakdown of total estimate for each scope item
                string tempStr = string.Empty;
                foreach (KeyValuePair<string, float> scopeItem in inScopeList)
                {
                    float breakdownEstimate = (scopeItem.Value * totalWorkItemsEffort) / 100;
                    //tempStr += scopeItem.Key + ": " + breakdownEstimate.ToString() + "\r\n";
                    
                    nd = new TreeNode(scopeItem.Key + ": " + breakdownEstimate.ToString());
                    trvDetailedEstimate.Nodes[0].ChildNodes.Add(nd);

                }


                //nd = new TreeNode("parent 1");
                //trvDetailedEstimate.Nodes[0].ChildNodes.Add(nd);

                //nd = new TreeNode("parent 2");
                //trvDetailedEstimate.Nodes[0].ChildNodes.Add(nd);

                //nd = new TreeNode("child 1");
                //trvDetailedEstimate.Nodes[0].ChildNodes[1].ChildNodes.Add(nd);
            }
        }

        private void FillProjectInfo()
        {
            // Get project
            string qry = CommonStatic.GET_ALL_PROJECT_BY_PROJECT_ID_QUERY;
            SqlCommand cmd = new SqlCommand(qry);
            cmd.Parameters.AddWithValue("@id", PROJECT_ID);
            DataTable dtProjectByProjectId = CommonStatic.GetData(cmd);

            // Set project name
            lblProjectName.Text = dtProjectByProjectId.Rows[0]["name"].ToString();

            Dictionary<string, string> REF_PROJECT_TYPE_KEYVAL = (Dictionary<string, string>)Application["REF_PROJECT_TYPE_KEYVAL"];
            string projectTypeName = string.Empty;
            REF_PROJECT_TYPE_KEYVAL.TryGetValue(dtProjectByProjectId.Rows[0]["projectTypeId"].ToString(), out projectTypeName);
            lblProjectType.Text = projectTypeName;


            // Get project scope
            qry = CommonStatic.GET_ALL_PROJECT_SCOPE_BY_PROJECT_QUERY;
            cmd = new SqlCommand(qry);
            cmd.Parameters.AddWithValue("@projectId", PROJECT_ID);
            DataTable dtProjectScopeByProject = CommonStatic.GetData(cmd);
            string projectScopeList = "<ul>";
            Dictionary<string, string> REF_PROJECT_SCOPE_KEYVAL = (Dictionary<string, string>)Application["REF_PROJECT_SCOPE_KEYVAL"];
            string projectScopeName = string.Empty;
            foreach (DataRow dr in dtProjectScopeByProject.Rows)
            {

                REF_PROJECT_SCOPE_KEYVAL.TryGetValue(dr["projectScopeId"].ToString(), out projectScopeName);
                projectScopeList = projectScopeList + "<li>" + projectScopeName + "</li>";
            }
            projectScopeList += "</ul>";
            lblProjectScope.Text = projectScopeList;

            // Get deloy env
            qry = CommonStatic.GET_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_QUERY;
            cmd = new SqlCommand(qry);
            cmd.Parameters.AddWithValue("@projectId", PROJECT_ID);
            DataTable dtDeployEnv = CommonStatic.GetData(cmd);
            string deployEnvList = "<ul>";
            Dictionary<string, string> REF_DEPLOYMENT_ENVIRONMENT_KEYVAL = (Dictionary<string, string>)Application["REF_DEPLOYMENT_ENVIRONMENT_KEYVAL"];
            string deployEnvName = string.Empty;
            foreach (DataRow dr in dtDeployEnv.Rows)
            {

                REF_DEPLOYMENT_ENVIRONMENT_KEYVAL.TryGetValue(dr["environmentId"].ToString(), out deployEnvName);
                deployEnvList = deployEnvList + "<li>" + deployEnvName + "</li>";
            }
            deployEnvList += "</ul>";
            lblDeploymentEnvironments.Text = deployEnvList;

            // Set contingency %
            lblContingencyPercent.Text = dtProjectByProjectId.Rows[0]["contingencyPercent"].ToString();

            // Set project overal complexity
            Dictionary<string, string> REF_PROJECT_COMPLEXITY_KEYVAL = (Dictionary<string, string>)Application["REF_PROJECT_COMPLEXITY_KEYVAL"];
            string complexityName = string.Empty;
            REF_PROJECT_COMPLEXITY_KEYVAL.TryGetValue(dtProjectByProjectId.Rows[0]["complexityId"].ToString(), out complexityName);
            lblProjectOveralComplexity.Text = complexityName;

            // Set project resource skill
            Dictionary<string, string> REF_RESOURCES_SKILL_KEYVAL = (Dictionary<string, string>)Application["REF_RESOURCES_SKILL_KEYVAL"];
            string resourcesSkillName = string.Empty;
            REF_RESOURCES_SKILL_KEYVAL.TryGetValue(dtProjectByProjectId.Rows[0]["resourcesSkillLevelId"].ToString(), out resourcesSkillName);
            lblResourceSkill.Text = resourcesSkillName;

            // Set project team structure
            Dictionary<string, string> REF_TEAM_STRUCTURE_KEYVAL = (Dictionary<string, string>)Application["REF_TEAM_STRUCTURE_KEYVAL"];
            string teamStructureName = string.Empty;
            REF_TEAM_STRUCTURE_KEYVAL.TryGetValue(dtProjectByProjectId.Rows[0]["teamStructureId"].ToString(), out teamStructureName);
            lblTeamStructure.Text = teamStructureName;

        }

    }
}