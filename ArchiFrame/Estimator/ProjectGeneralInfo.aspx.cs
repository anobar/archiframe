﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ArchiFrame.Class;
using System.Web.Security;
namespace ArchiFrame.Estimator
{

    //static string GetAllWorkItemComplexityQuery = "";

     

    public partial class ProjectGeneralInfo : System.Web.UI.Page
    {
        static string ClinetId = "1"; // TODO: change
        static string PROJECT_ID;
        //static string CnnString = ConfigurationManager.ConnectionStrings["ArchiFrameConnectionString"].ConnectionString;
        //static string GET_ALL_PROJECT_BY_CLIENT_QUERY = "SELECT [id],[name],[parnetProjectId],[clientId],[projectTypeId],[complexityId],[resourcesSkillLevelId],[teamStructureId],[contingencyPercent],[startDate],[endDate] FROM [Project] WHERE clientId=@clientId";
        //static string GET_ALL_REF_PROJECT_TYPE_QUERY = "SELECT [id],[name] FROM [Ref_ProjectType]";
        //static string GET_ALL_REF_PROJECT_COMPLEXITY_QUERY = "SELECT [id],[name] FROM [Ref_ProjectComplexity]";
        //static string GET_ALL_REF_RESOURCES_SKILL_LEVEL_QUERY = "SELECT [id],[name] FROM [Ref_ResourcesSkillLevel]";
        //static string GET_ALL_REF_TEAM_STRUCTURE_QUERY = "SELECT [id],[name] FROM [Ref_TeamStructure]";
        //static string GET_ALL_REF_PROJECT_SCOPE_QUERY = "SELECT [id],[name] FROM [Ref_ProjectScope]";
        //static string GET_ALL_REF_DEPLOYMENT_ENVIRONMENT_QUERY = "SELECT [id],[name] FROM [Ref_DeploymentEnvironment]";
        //static string GET_ALL_PROJECT_SCOPE_BY_PROJECT_QUERY = "SELECT [id],[projectId],[projectScopeId] FROM ProjectScope WHERE projectId=@projectId";
        //static string GET_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_QUERY = "SELECT [id],[projectId], [environmentId] FROM [DeploymentEnvironment] WHERE projectId=@projectId";
        //static string ADD_PROJECT_QUERY = "INSERT INTO [dbo].[Project] ([name],[parnetProjectId],[clientId],[projectTypeId],[complexityId],[resourcesSkillLevelId],[teamStructureId],[contingencyPercent]) VALUES (@name, @parnetProjectId, @clientId, @projectTypeId, @complexityId, @resourcesSkillLevelId, @teamStructureId, @contingencyPercent); SELECT CAST(scope_identity() AS int)";
        //static string ADD_PROJECT_SCOPE_QUERY = "INSERT INTO [dbo].[ProjectScope] ([projectId], [projectScopeId]) VALUES (@projectId, @projectScopeId)";
        //static string ADD_DEPLOYMENT_ENVIRONMENT_QUERY = "INSERT INTO [dbo].[DeploymentEnvironment] ([projectId], [environmentId]) VALUES (@projectId, @environmentId)";
        //static string UPDATE_PROJECT_QUERY = "UPDATE [Project] SET [name] = @name, [projectTypeId] = @projectTypeId, [complexityId] = @complexityId, [resourcesSkillLevelId] = @resourcesSkillLevelId, [teamStructureId] = @teamStructureId ,[contingencyPercent] = @contingencyPercent WHERE id=@id";
        //static string DELETE_ALL_PROJECT_SCOPE_BY_PROJECT_ID_QUERY = "DELETE FROM [ProjectScope] WHERE projectId=@projectId";
        //static string DELETE_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_ID_QUERY = "DELETE FROM [DeploymentEnvironment] WHERE projectId=@projectId";
        static DataTable DT_ALL_PROJECTS_BY_CLIENT;
        static PageMode PAGE_MODE;
        static Dictionary<string, string> REF_PROJECT_SCOPE_KEYVAL = new Dictionary<string, string>();
        static Dictionary<string, string> REF_DEPLOYMENT_ENVIRONMENT_KEYVAL = new Dictionary<string, string>();

        public enum PageMode
        {
            Insert,
            Update
        }

        //static KeyValuePair<string, string> RefProjectScope;

      //  static List<typeof(KeyValuePair<string, string>)> list; // = new List<typeof(i)>();
        private ArchiFrameEntities db = new ArchiFrameEntities();
        private Guid id;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            MembershipUser user = Membership.GetUser();
            string UserID = user.ProviderUserKey.ToString();
             id = Guid.Parse(UserID);
            // TODO: uncomment following to apply authentication

            //if(!Request.IsAuthenticated)
            //{
            //    Response.Redirect("~/Account/Login?returnUrl="
            //        + Request.Url.LocalPath);
            //}

            if (!IsPostBack)
            {
                InitiatePage();
                EstimatorWizard.ActiveStepIndex = 0; // go to the first step of the wizard
                PAGE_MODE = PageMode.Insert; // set page mode to insert
                // TODO: Gray out chkListEnvironment if deploymet check box is not checked in chkListProjectScope

               // PROJECT_ID = Request.QueryString.Get("id");
                //if (!string.IsNullOrEmpty(PROJECT_ID))
                //{
                //    // Select the right project in Project Dropdown List
                //    ddlProjects.ClearSelection();
                //    ddlProjects.Items.FindByValue(PROJECT_ID).Selected = true;
                //    fillFormAfterProjectSelected(PROJECT_ID);
                //}
            }

            EstimatorWizard.PreRender += new EventHandler(Wizard1_PreRender); 
        }

        private void InitiatePage()
        {
            txtProjectName.Text = string.Empty;
            txtContingency.Text = string.Empty;
            if (PAGE_MODE==PageMode.Update)
            {
                this.BindGeneralInfoPage();  
            }
       
            //this.BindAssumptionPage();
            //this.BindScopePage();
        }

        private void BindGeneralInfoPage()
        {

            Client client = new Client();
            client.ID = (from s in db.User_Profile
                        where s.UserId == id
                        select s.UserCompany_Id).First();


            //add data to ddlprojects dropdown 
            var queryddlProjects = (from s in db.Projects
                                     where s.clientId == client.ID
                                     select new { s.id, s.name });
            List<itemFordpl> listItemddlProjects = new List<itemFordpl>();

            foreach (var x in queryddlProjects)
            {
                itemFordpl li=new itemFordpl();
                li.ID=x.id;
                li.Name=x.name;
                listItemddlProjects.Add(li);
            }

            List<itemFordpl> listItemsddlProjects = new List<itemFordpl>();
            ddlProjects.DataSource = listItemsddlProjects;
            ddlProjects.DataTextField = "Name";
            ddlProjects.DataValueField = "ID";
            ddlProjects.DataBind();
            //add data to ddlproject type

            var queryddlProjectType = (from s in db.Ref_ProjectType
                                       select new { s.id, s.name });
            List<itemFordpl> listddlProjectType = new List<itemFordpl>();
            ddlProjectType.DataSource = listItemsddlProjects;
            ddlProjectType.DataTextField = "Name";
            ddlProjectType.DataValueField = "ID";
            ddlProjectType.DataBind();
          

            //string clientId = "1"; // TODO: read from Cookie OR Session State


            //string qry = CommonStatic.GET_ALL_PROJECT_BY_CLIENT_QUERY;
            //SqlCommand cmd = new SqlCommand(qry);
            //cmd.Parameters.AddWithValue("@clientId", clientId);
            //DT_ALL_PROJECTS_BY_CLIENT = CommonStatic.GetData(cmd);
            //ddlProjects.DataSource = DT_ALL_PROJECTS_BY_CLIENT;
            //ddlProjects.DataTextField = "name";
            //ddlProjects.DataValueField = "id";
            //ddlProjects.DataBind();
            //ddlProjects.Items.Insert(0, new ListItem("[select]", "-1"));
            //ddlProjects.SelectedIndex = 0;

            //qry = CommonStatic.GET_ALL_REF_PROJECT_TYPE_QUERY;
            //cmd = new SqlCommand(qry);
            //ddlProjectType.DataSource = CommonStatic.GetData(cmd);
            //ddlProjectType.DataTextField = "name";
            //ddlProjectType.DataValueField = "id";
            //ddlProjectType.DataBind();
            //ddlProjectType.Items.Insert(0, new ListItem("[select]", "-1"));
            //ddlProjectType.SelectedIndex = 0;
        }

        private void BindScopePage()
        {
            string qry = CommonStatic.GET_ALL_REF_PROJECT_SCOPE_QUERY;
            SqlCommand cmd = new SqlCommand(qry);
            DataTable dtRefProjectScope = CommonStatic.GetData(cmd);
            chkListProjectScope.DataSource = dtRefProjectScope;
            chkListProjectScope.DataTextField = "name";
            chkListProjectScope.DataValueField = "id";
            chkListProjectScope.DataBind();
            chkListProjectScope.ClearSelection();

            REF_PROJECT_SCOPE_KEYVAL.Clear();
            foreach (DataRow dr in dtRefProjectScope.Rows)
            {
                REF_PROJECT_SCOPE_KEYVAL.Add(dr["id"].ToString(), dr["name"].ToString());
            }

            qry = CommonStatic.GET_ALL_REF_DEPLOYMENT_ENVIRONMENT_QUERY;
            cmd = new SqlCommand(qry);
            DataTable dtRefDeploymentEnvironment = CommonStatic.GetData(cmd);
            chkListEnvironments.DataSource = dtRefDeploymentEnvironment;
            chkListEnvironments.DataTextField = "name";
            chkListEnvironments.DataValueField = "id";
            chkListEnvironments.DataBind();
            chkListEnvironments.ClearSelection();
            REF_DEPLOYMENT_ENVIRONMENT_KEYVAL.Clear();
            foreach (DataRow dr in dtRefDeploymentEnvironment.Rows)
            {
                REF_DEPLOYMENT_ENVIRONMENT_KEYVAL.Add(dr["id"].ToString(), dr["name"].ToString());
            }
        }

        private void BindAssumptionPage()
        {
            string projectComplexityQuery = CommonStatic.GET_ALL_REF_PROJECT_COMPLEXITY_QUERY;
            SqlCommand cmd = new SqlCommand(projectComplexityQuery);
            ddlProjectComplexity.DataSource = CommonStatic.GetData(cmd);
            ddlProjectComplexity.DataTextField = "name";
            ddlProjectComplexity.DataValueField = "id";
            ddlProjectComplexity.DataBind();
            ddlProjectComplexity.Items.Insert(0, new ListItem("[select]", "-1"));
            ddlProjectComplexity.SelectedIndex = 0;

            string resourcesSkillsQuery = CommonStatic.GET_ALL_REF_RESOURCES_SKILL_LEVEL_QUERY;
            cmd = new SqlCommand(resourcesSkillsQuery);
            ddlResourcesSkills.DataSource = CommonStatic.GetData(cmd);
            ddlResourcesSkills.DataTextField = "name";
            ddlResourcesSkills.DataValueField = "id";
            ddlResourcesSkills.DataBind();
            ddlResourcesSkills.Items.Insert(0, new ListItem("[select]", "-1"));
            ddlResourcesSkills.SelectedIndex = 0;

            string teamStructureQuery = CommonStatic.GET_ALL_REF_TEAM_STRUCTURE_QUERY;
            cmd = new SqlCommand(teamStructureQuery);
            rdoListTeamStructure.DataSource = CommonStatic.GetData(cmd);
            rdoListTeamStructure.DataTextField = "name";
            rdoListTeamStructure.DataValueField = "id";
            rdoListTeamStructure.DataBind();
            rdoListTeamStructure.ClearSelection();
        }

        //private DataTable GetData(SqlCommand cmd)
        //{
        //    string strConnString = CommonStatic.CnnString;
        //    using (SqlConnection con = new SqlConnection(strConnString))
        //    {
        //        using (SqlDataAdapter sda = new SqlDataAdapter())
        //        {
        //            cmd.Connection = con;
        //            sda.SelectCommand = cmd;
        //            using (DataTable dt = new DataTable())
        //            {
        //                sda.Fill(dt);
        //                return dt;
        //            }
        //        }
        //    }
        //}

        protected void EstimatorWizard_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            string projectId = AddUpdateProjectandProjectScope_Transaction();
            if (projectId == string.Empty)
            {
                throw new Exception("Project did not added / updated");
            }
            Response.Redirect("ProjectWorkItems.aspx?id="+projectId);
            //Response.Redirect(Request.Url.AbsoluteUri);
        }

        private string AddUpdateProjectandProjectScope_Transaction()
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            string retVal = string.Empty;

            try
            {
                if (PAGE_MODE == PageMode.Insert)
                {
                    string strConnString = CommonStatic.CnnString;
                    con = new SqlConnection(strConnString);
                    con.Open();

                    // Adding Project as part of Atomic Transaction
                    string query = CommonStatic.ADD_PROJECT_QUERY;
                    transaction = con.BeginTransaction();
                    SqlCommand cmd = new SqlCommand(query, con, transaction);
                    GetProjectAddParam(ref cmd);
                    int projectId = (Int32)cmd.ExecuteScalar();
                    retVal = projectId.ToString();

                    // Adding Project Scope as part of Atomic Transaction
                    query = CommonStatic.ADD_PROJECT_SCOPE_QUERY;
                    cmd = new SqlCommand(query, con, transaction);
                    //GetProjectScopeAddParam(ref cmd);
                    foreach (ListItem lst in chkListProjectScope.Items)
                    {
                        if (lst.Selected)
                        {
                            cmd.Parameters.Clear();

                            cmd.Parameters.AddWithValue("@projectId", projectId);
                            cmd.Parameters.AddWithValue("@projectScopeId", lst.Value);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    // Commit the transaction.
                    transaction.Commit();
                }
                else if (PAGE_MODE == PageMode.Update)
                {
                    if (ddlProjects.SelectedValue == "-1")
                        throw new Exception("Select a project from the list");

                    string strConnString = CommonStatic.CnnString;
                    con = new SqlConnection(strConnString);
                    con.Open();

                    // Updating Project as part of Atomic Transaction
                    string query = CommonStatic.UPDATE_PROJECT_QUERY;
                    transaction = con.BeginTransaction();
                    SqlCommand cmd = new SqlCommand(query, con, transaction);
                    GetProjectUpdateParam(ref cmd);
                    int numberOfRowsAffected = cmd.ExecuteNonQuery();
                    if (numberOfRowsAffected != 1)
                    {
                        transaction.Rollback();
                        throw new Exception("Project update failed");
                    }

                    // Updating Project Scope as part of Atomic Transaction: 
                    // 1. DELETE_ALL_PROJECT_SCOPE_BY_PROJECT_ID 
                    query = CommonStatic.DELETE_ALL_PROJECT_SCOPE_BY_PROJECT_ID_QUERY;
                    cmd = new SqlCommand(query, con, transaction);
                    //GetProjectScopeDeleteParam(ref cmd);
                    cmd.Parameters.AddWithValue("@projectId", ddlProjects.SelectedValue);
                    cmd.ExecuteNonQuery();

                    // 2. ADD_PROJECT_SCOPE
                    query = CommonStatic.ADD_PROJECT_SCOPE_QUERY;
                    cmd = new SqlCommand(query, con, transaction);
                    //GetProjectScopeAddParam(ref cmd);
                    foreach (ListItem lst in chkListProjectScope.Items)
                    {
                        if (lst.Selected)
                        {
                            cmd.Parameters.Clear();

                            cmd.Parameters.AddWithValue("@projectId", ddlProjects.SelectedValue);
                            cmd.Parameters.AddWithValue("@projectScopeId", lst.Value);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    // Updating Deployment Environment as part of Atomic Transaction: 
                    // 1. DELETE_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_ID_QUERY 
                    query = CommonStatic.DELETE_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_ID_QUERY;
                    cmd = new SqlCommand(query, con, transaction);
                    //GetDeploymentEnvironmentDeleteParam(ref cmd);
                    cmd.Parameters.AddWithValue("@projectId", ddlProjects.SelectedValue);
                    cmd.ExecuteNonQuery();

                    // 2. ADD_DEPLOYMENT_ENVIRONMENT
                    query = CommonStatic.ADD_DEPLOYMENT_ENVIRONMENT_QUERY;
                    cmd = new SqlCommand(query, con, transaction);
                  //  GetDeploymentEnvironmentAddParam(ref cmd);
                    foreach (ListItem lst in chkListEnvironments.Items)
                    {
                        if (lst.Selected)
                        {
                            cmd.Parameters.Clear();

                            cmd.Parameters.AddWithValue("@projectId", ddlProjects.SelectedValue);
                            cmd.Parameters.AddWithValue("@environmentId", lst.Value);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    // Commit the transaction.
                    transaction.Commit();
                    retVal = ddlProjects.SelectedValue;
                }
               
            }

            catch
            {
                // Roll back the transaction. 
                transaction.Rollback();
            }
            con.Close();
            return retVal;
        }

        private void GetProjectAddParam(ref SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue("@name", txtProjectName.Text.Trim());
            cmd.Parameters.AddWithValue("@parnetProjectId", DBNull.Value);
            cmd.Parameters.AddWithValue("@clientId", ClinetId);
            cmd.Parameters.AddWithValue("@projectTypeId", ddlProjectType.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@complexityId", ddlProjectComplexity.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@resourcesSkillLevelId", ddlResourcesSkills.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@teamStructureId", rdoListTeamStructure.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@contingencyPercent", txtContingency.Text.Trim());
        }

        private void GetProjectUpdateParam(ref SqlCommand cmd)
        {
            cmd.Parameters.AddWithValue("@name", txtProjectName.Text.Trim());
            cmd.Parameters.AddWithValue("@projectTypeId", ddlProjectType.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@complexityId", ddlProjectComplexity.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@resourcesSkillLevelId", ddlResourcesSkills.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@teamStructureId", rdoListTeamStructure.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@contingencyPercent", txtContingency.Text.Trim());
            cmd.Parameters.AddWithValue("@id", ddlProjects.SelectedValue);
        }

        //private void GetProjectScopeAddParam(ref SqlCommand cmd)
        //{
        //    cmd.Parameters.AddWithValue("@name", txtProjectName.Text.Trim());
        //    cmd.Parameters.AddWithValue("@parnetProjectId", DBNull.Value);
        //    cmd.Parameters.AddWithValue("@clientId", ClinetId);
        //    cmd.Parameters.AddWithValue("@projectTypeId", ddlProjectType.SelectedItem.Value);
        //    cmd.Parameters.AddWithValue("@complexityId", ddlProjectComplexity.SelectedItem.Value);
        //    cmd.Parameters.AddWithValue("@resourcesSkillLevelId", ddlResourcesSkills.SelectedItem.Value);
        //    cmd.Parameters.AddWithValue("@teamStructureId", rdoListTeamStructure.SelectedItem.Value);
        //    cmd.Parameters.AddWithValue("@contingencyPercent", txtContingency.Text.Trim());
        //}

        //private void GetProjectScopeDeleteParam(ref SqlCommand cmd)
        //{
        //    cmd.Parameters.AddWithValue("@projectId", ddlProjects.SelectedValue);
        //}

        //private void GetDeploymentEnvironmentDeleteParam(ref SqlCommand cmd)
        //{
        //    cmd.Parameters.AddWithValue("@projectId", ddlProjects.SelectedValue);
        //}

        protected void ddlProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
           // fillFormAfterProjectSelected(ddlProjects.SelectedValue);
        }

        private void fillFormAfterProjectSelected(string projectId)
        {
            try
            {
                string selectedProjectId = projectId;

                if (selectedProjectId == "-1")
                {
                    InitiatePage();
                    return;
                }
                DataColumn[] keyColumns = new DataColumn[1];
                keyColumns[0] = DT_ALL_PROJECTS_BY_CLIENT.Columns["id"];
                DT_ALL_PROJECTS_BY_CLIENT.PrimaryKey = keyColumns;
                txtProjectName.Text = DT_ALL_PROJECTS_BY_CLIENT.Rows.Find(selectedProjectId)["name"].ToString();

                ddlProjectType.ClearSelection();
                ddlProjectType.Items.FindByValue(DT_ALL_PROJECTS_BY_CLIENT.Rows.Find(selectedProjectId)["projectTypeId"].ToString()).Selected = true;


                // Get all ProjectScope By Project
                string qry = CommonStatic.GET_ALL_PROJECT_SCOPE_BY_PROJECT_QUERY;
                SqlCommand cmd = new SqlCommand(qry);
                cmd.Parameters.AddWithValue("@projectId", selectedProjectId);
                DataTable dtProjectScopeByProject = CommonStatic.GetData(cmd);

                chkListProjectScope.ClearSelection();
                foreach (DataRow dr in dtProjectScopeByProject.Rows)
                {
                    string currentScopeId = dr["projectScopeId"].ToString();
                    if (REF_PROJECT_SCOPE_KEYVAL.ContainsKey(currentScopeId))
                    {
                        chkListProjectScope.Items.FindByValue(currentScopeId).Selected = true;
                    }
                }

                // Get all Deploy Environment By Project
                qry = CommonStatic.GET_ALL_DEPLOYMENT_ENVIRONMENT_BY_PROJECT_QUERY;
                cmd = new SqlCommand(qry);
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@projectId", selectedProjectId);
                DataTable dtDeploymentEnvironmentByProject = CommonStatic.GetData(cmd);

                chkListEnvironments.ClearSelection();
                foreach (DataRow dr in dtDeploymentEnvironmentByProject.Rows)
                {
                    string currentEnvId = dr["environmentId"].ToString();
                    if (REF_DEPLOYMENT_ENVIRONMENT_KEYVAL.ContainsKey(currentEnvId))
                    {
                        chkListEnvironments.Items.FindByValue(currentEnvId).Selected = true;
                    }
                }

                txtContingency.Text = DT_ALL_PROJECTS_BY_CLIENT.Rows.Find(selectedProjectId)["contingencyPercent"].ToString();

                // Set Project Complexity Dropdown List
                ddlProjectComplexity.ClearSelection();
                string complexityId = DT_ALL_PROJECTS_BY_CLIENT.Rows.Find(selectedProjectId)["complexityId"].ToString();
                if (complexityId != string.Empty)
                    ddlProjectComplexity.Items.FindByValue(complexityId).Selected = true;

                // Set Resource Skills Dropdown List
                ddlResourcesSkills.ClearSelection();
                string resourceSkillId = DT_ALL_PROJECTS_BY_CLIENT.Rows.Find(selectedProjectId)["resourcesSkillLevelId"].ToString();
                if (resourceSkillId != string.Empty)
                    ddlResourcesSkills.Items.FindByValue(resourceSkillId).Selected = true;

                // Set Team Structure RadioButton List

                rdoListTeamStructure.ClearSelection();
                string teamStructureId = DT_ALL_PROJECTS_BY_CLIENT.Rows.Find(selectedProjectId)["teamStructureId"].ToString();
                if (teamStructureId != string.Empty)
                    rdoListTeamStructure.Items.FindByValue(teamStructureId).Selected = true;

                // set page mode to UPDATE
                PAGE_MODE = PageMode.Update;
            }
            catch (Exception ex)
            {
                // TODO: handle exception
                InitiatePage();
            }
        }

        protected void lbtnAddNewProject_Click(object sender, EventArgs e)
        {
            InitiatePage();
            PAGE_MODE = PageMode.Insert; // set page mode to insert
           // txtProjectName.Focus();
        }

        protected void WizardStep1_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (!Page.IsValid)
            {
                e.Cancel = true;
            }
        }
        protected void Wizard1_PreRender(object sender, EventArgs e)
        {
            Repeater SideBarList = EstimatorWizard.FindControl("HeaderContainer").FindControl("SideBarList") as Repeater;
            SideBarList.DataSource = EstimatorWizard.WizardSteps;
            SideBarList.DataBind();

        }

        protected string GetClassForWizardStep(object wizardStep)
        {
            WizardStep step = wizardStep as WizardStep;

            if (step == null)
            {
                return "";
            }
            int stepIndex = EstimatorWizard.WizardSteps.IndexOf(step);

            if (stepIndex < EstimatorWizard.ActiveStepIndex)
            {
                return "prevStep";
            }
            else if (stepIndex > EstimatorWizard.ActiveStepIndex)
            {
                return "nextStep";
            }
            else
            {
                return "currentStep";
            }
        }

        protected void EstimatorWizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {

            if (EstimatorWizard.ActiveStepIndex == 0)
            {
                

                if (RadioWhatToDo.SelectedItem.ToString() == "Existing Project")
                {
                    PAGE_MODE = PageMode.Update;
                    Client client = new Client();
                    client.ID = (from s in db.User_Profile
                                 where s.UserId == id
                                 select s.UserCompany_Id).First();
                    int searchForClient = (from s in db.Projects
                                           where s.clientId == client.ID
                                           select s).Count();
                    if (searchForClient == 0)
                    {
                        LblErrorWiz1.Text = "There is no project in your profile please select a new project";

                        e.Cancel = true;
                    }
                    else
                    {
                        PAGE_MODE = PageMode.Insert; 
                    }
    
                }
                else
                {
                    LblErrorWiz1.Text = "";
                }
            }
              
            
          
        }

    }
                
}