﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ArchiFrame.Class;

namespace ArchiFrame.Estimator
{
    public partial class ProjectWorkItems : System.Web.UI.Page
    {
      //  static string WorkItemConnectionString = ConfigurationManager.ConnectionStrings["ArchiFrameConnectionString"].ConnectionString;
        static DataTable DT_ALL_PROJECTS_BY_CLIENT;
        static string PROJECT_ID;

        protected void Page_Load(object sender, EventArgs e)
        {
            // TODO: uncomment following to apply authentication

            //if (!Request.IsAuthenticated)
            //{
            //    Response.Redirect("~/Account/Login?returnUrl="
            //        + Request.Url.LocalPath);
            //}

            //PROJECT_ID = Request.QueryString.Get("id");
            //if (string.IsNullOrEmpty(PROJECT_ID))
            //    Response.Redirect("ProjectGeneralInfo");

            if (!IsPostBack)
            {
                this.BindProjectDropDownList();
                this.BindWorkItemGridView();
                this.BindWorkItemAddControls();

                PROJECT_ID = Request.QueryString.Get("id");
                if (!string.IsNullOrEmpty(PROJECT_ID))
                {
                    // Select the right project in Project Dropdown List
                    ddlProjects.ClearSelection();
                    ddlProjects.Items.FindByValue(PROJECT_ID).Selected = true;
                    fillFormAfterProjectSelected(PROJECT_ID);
                }
            }
        }

        //private void BindWorkItemGridView()
        //{
        //    string GridViewQuery = GetAllJointWorkItemQuery;
        //    SqlCommand cmd = new SqlCommand(GridViewQuery);
        //    GridView1.DataSource = CommonStatic.GetData(cmd);
        //    GridView1.DataBind();
        //}
        //private void BindWorkItemAddControls()
        //{
        //    string complexityQuery = GetAllWorkItemComplexityQuery;
        //    SqlCommand cmd = new SqlCommand(complexityQuery);
        //    ddlAddComplexity.DataSource = CommonStatic.GetData(cmd);
        //    ddlAddComplexity.DataTextField = "complexity";
        //    ddlAddComplexity.DataValueField = "id";
        //    ddlAddComplexity.DataBind();

        //    string typeQuery = GetAllWorkItemTypeQuery;
        //    cmd = new SqlCommand(typeQuery);
        //    ddlAddType.DataSource = CommonStatic.GetData(cmd);
        //    ddlAddType.DataTextField = "name";
        //    ddlAddType.DataValueField = "id";
        //    ddlAddType.DataBind();
        //}

        //private DataTable CommonStatic.GetData(SqlCommand cmd)
        //{
        //    string strConnString = CommonStatic.CnnString;
        //    using (SqlConnection con = new SqlConnection(strConnString))
        //    {
        //        using (SqlDataAdapter sda = new SqlDataAdapter())
        //        {
        //            cmd.Connection = con;
        //            sda.SelectCommand = cmd;
        //            using (DataTable dt = new DataTable())
        //            {
        //                sda.Fill(dt);
        //                return dt;
        //            }
        //        }
        //    }
        //}

        private void BindProjectDropDownList()
        {


            string clientId = "1"; // TODO: read from Cookie OR Session State

            string qry = CommonStatic.GET_ALL_PROJECT_BY_CLIENT_QUERY;
            SqlCommand cmd = new SqlCommand(qry);
            cmd.Parameters.AddWithValue("@clientId", clientId);
            DT_ALL_PROJECTS_BY_CLIENT = CommonStatic.GetData(cmd);
            ddlProjects.DataSource = DT_ALL_PROJECTS_BY_CLIENT;
            ddlProjects.DataTextField = "name";
            ddlProjects.DataValueField = "id";
            ddlProjects.DataBind();
            ddlProjects.Items.Insert(0, new ListItem("[select]", "-1"));
            ddlProjects.SelectedIndex = 0;


        }
        private void BindWorkItemGridView()
        {
            if (!string.IsNullOrEmpty(PROJECT_ID))
            {
                string GridViewQuery = CommonStatic.GET_ALL_JOINT_WORK_ITEM_QUERY;
                SqlCommand cmd = new SqlCommand(GridViewQuery);
                cmd.Parameters.AddWithValue("@projectId", PROJECT_ID);
                GridView1.DataSource = CommonStatic.GetData(cmd);
                GridView1.DataBind();
            }

        }
        private void BindWorkItemAddControls()
        {
            string complexityQuery = CommonStatic.GET_ALL_WORK_ITEM_COMPLEXITY_QUERY;
            SqlCommand cmd = new SqlCommand(complexityQuery);
            ddlAddComplexity.DataSource = CommonStatic.GetData(cmd);
            ddlAddComplexity.DataTextField = "complexity";
            ddlAddComplexity.DataValueField = "id";
            ddlAddComplexity.DataBind();
            ddlAddComplexity.Items.Insert(0, new ListItem("[select]", "-1"));
            ddlAddComplexity.SelectedIndex = 0;

            string typeQuery = CommonStatic.GET_ALL_WORK_ITEM_TYPE_QUERY;
            cmd = new SqlCommand(typeQuery);
            ddlAddType.DataSource = CommonStatic.GetData(cmd);
            ddlAddType.DataTextField = "name";
            ddlAddType.DataValueField = "id";
            ddlAddType.DataBind();
            ddlAddType.Items.Insert(0, new ListItem("[select]", "-1"));
            ddlAddType.SelectedIndex = 0;
        }

        protected void EditWorkItem(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindWorkItemGridView();
        }

        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindWorkItemGridView();
        }

        protected void WorkItemRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && GridView1.EditIndex == e.Row.RowIndex)
            {
                DropDownList ddlComplexity = (DropDownList)e.Row.FindControl("ddlComplexity");
                string getWorkItemComplexityQuery = CommonStatic.GET_ALL_WORK_ITEM_COMPLEXITY_QUERY;
                SqlCommand getWorkItemComplexityCmd = new SqlCommand(getWorkItemComplexityQuery);
                ddlComplexity.DataSource = CommonStatic.GetData(getWorkItemComplexityCmd);
                ddlComplexity.DataTextField = "complexity";
                ddlComplexity.DataValueField = "id";
                ddlComplexity.DataBind();

                DropDownList ddlType = (DropDownList)e.Row.FindControl("ddlType");
                string getWorkItemTypeQuery = CommonStatic.GET_ALL_WORK_ITEM_TYPE_QUERY;
                SqlCommand getWorkItemTypeCmd = new SqlCommand(getWorkItemTypeQuery);
                ddlType.DataSource = CommonStatic.GetData(getWorkItemTypeCmd);
                ddlType.DataTextField = "name";
                ddlType.DataValueField = "id";
                ddlType.DataBind();

                TextBox txtDescription = (TextBox)e.Row.FindControl("txtDescription");
                txtDescription.Text = (e.Row.FindControl("lblDescriptionHidden") as Label).Text;

                TextBox txtQuantity = (TextBox)e.Row.FindControl("txtQuantity");
                txtQuantity.Text = (e.Row.FindControl("lblQuantityHidden") as Label).Text;
                ddlType.Items.FindByText((e.Row.FindControl("lblWorkItemTypeHidden") as Label).Text).Selected = true;
                ddlComplexity.Items.FindByText((e.Row.FindControl("lblWorkItemComplexityHidden") as Label).Text).Selected = true;
            }
        }

        protected void UpdateWorkItem(object sender, GridViewUpdateEventArgs e)
        {
            string type = (GridView1.Rows[e.RowIndex].FindControl("ddlType") as DropDownList).SelectedItem.Value;
            string complexity = (GridView1.Rows[e.RowIndex].FindControl("ddlComplexity") as DropDownList).SelectedItem.Value;
            string description = (GridView1.Rows[e.RowIndex].FindControl("txtDescription") as TextBox).Text;
            string quantity = (GridView1.Rows[e.RowIndex].FindControl("txtQuantity") as TextBox).Text;
            string workItemId = GridView1.DataKeys[e.RowIndex].Value.ToString();
            string strConnString = CommonStatic.CnnString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                string query = CommonStatic.UPDATE_WORK_ITEM_QUERY;
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@typeId", type);
                    cmd.Parameters.AddWithValue("@complexityId", complexity);
                    cmd.Parameters.AddWithValue("@description", description);
                    cmd.Parameters.AddWithValue("@quantity", quantity);
                    cmd.Parameters.AddWithValue("@id", workItemId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    Response.Redirect(Request.Url.AbsolutePath + "?id=" + PROJECT_ID);

         
                }
            }
        }


        protected void DeleteWorkItem(object sender, GridViewDeleteEventArgs e)
        {
            string workItemId = GridView1.DataKeys[e.RowIndex].Value.ToString();

            string strConnString = CommonStatic.CnnString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                string query = CommonStatic.DELETE_WORK_ITEM_QUERY;
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;

                    cmd.Parameters.AddWithValue("@id", workItemId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                    Response.Redirect(Request.Url.AbsolutePath + "?id=" + PROJECT_ID);
                }
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //string workItemId = GridView1.DataKeys[e.RowIndex].Value.ToString();

            string strConnString = CommonStatic.CnnString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                string query = CommonStatic.ADD_WORK_ITEM_QUERY;
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    // TODO Set Project ID 
                    cmd.Parameters.AddWithValue("@projectId", PROJECT_ID);

                    cmd.Parameters.AddWithValue("@typeId", ddlAddType.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@complexityId", ddlAddComplexity.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@description", txtAddDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@quantity", txtAddQuantity.Text.Trim());
                    //cmd.Parameters.AddWithValue("@id", workItemId);

                    con.Open();
                    int addedId = cmd.ExecuteNonQuery();
                    con.Close();
                    Response.Redirect(Request.Url.AbsolutePath + "?id=" + PROJECT_ID);
                }
            }
        }

        protected void btnEditProjectInfo_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProjectGeneralInfo.aspx?id="+ PROJECT_ID);
        }

        protected void btnEstimate_Click(object sender, EventArgs e)
        {
            string projectId = ddlProjects.SelectedValue;
            if (projectId == string.Empty || projectId == "-1")
            {
                throw new Exception("Select project from list");
            }
            Response.Redirect("EstimateResult.aspx?id=" + projectId);
        }

        protected void ddlProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillFormAfterProjectSelected(ddlProjects.SelectedValue);
        }

        private void fillFormAfterProjectSelected(string projectId)
        {
            try
            {
                string selectedProjectId = projectId;

                if (selectedProjectId == "-1")
                {
                    PROJECT_ID = "-1";
                    ClearForm();
                    return;
                }
                else
                {
                    PROJECT_ID = selectedProjectId;
                    BindWorkItemGridView();
                }
            }
            catch (Exception ex)
            {
                // TODO: handle exception
                ClearForm();
            }
        }

        private void ClearForm()
        {
            GridView1.DataSource = null;
            GridView1.DataBind();

            txtAddDescription.Text = string.Empty;
            txtAddQuantity.Text = string.Empty;
            ddlProjects.Items[0].Selected = true;
            ddlAddComplexity.Items[0].Selected = true;
            ddlAddType.Items[0].Selected = true;
            //DataColumn[] keyColumns = new DataColumn[1];
            //keyColumns[0] = DT_ALL_PROJECTS_BY_CLIENT.Columns["id"];
            //DT_ALL_PROJECTS_BY_CLIENT.PrimaryKey = keyColumns;
            //ddlProjects.Items.FindByValue("-1").Selected = true;


        }

    }
}