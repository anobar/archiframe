﻿using ArchiFrame.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArchiFrame
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // loading commonly used look-up objects in the application.
           
           Dictionary<string, string> REF_PROJECT_SCOPE_KEYVAL = new Dictionary<string, string>();
           Dictionary<string, string> REF_DEPLOYMENT_ENVIRONMENT_KEYVAL = new Dictionary<string, string>();
           Dictionary<string, string> REF_PROJECT_TYPE_KEYVAL = new Dictionary<string, string>();
           Dictionary<string, string> REF_PROJECT_COMPLEXITY_KEYVAL = new Dictionary<string, string>();
           Dictionary<string, string> REF_RESOURCES_SKILL_KEYVAL = new Dictionary<string, string>();
           Dictionary<string, string> REF_TEAM_STRUCTURE_KEYVAL = new Dictionary<string, string>();

            

           // Loading GET_ALL_REF_PROJECT_SCOPE_QUERY
            string qry = CommonStatic.GET_ALL_REF_PROJECT_SCOPE_QUERY;
            SqlCommand cmd = new SqlCommand(qry);
            DataTable dt = CommonStatic.GetData(cmd);

            REF_PROJECT_SCOPE_KEYVAL.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                REF_PROJECT_SCOPE_KEYVAL.Add(dr["id"].ToString(), dr["name"].ToString());
            }

            // Loading GET_ALL_REF_DEPLOYMENT_ENVIRONMENT_QUERY
            qry = CommonStatic.GET_ALL_REF_DEPLOYMENT_ENVIRONMENT_QUERY;
            cmd = new SqlCommand(qry);
            dt = CommonStatic.GetData(cmd);
            REF_DEPLOYMENT_ENVIRONMENT_KEYVAL.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                REF_DEPLOYMENT_ENVIRONMENT_KEYVAL.Add(dr["id"].ToString(), dr["name"].ToString());
            }

            // Loading GET_ALL_REF_PROJECT_TYPE_QUERY
            qry = CommonStatic.GET_ALL_REF_PROJECT_TYPE_QUERY;
            cmd = new SqlCommand(qry);
            dt = CommonStatic.GetData(cmd);
            REF_PROJECT_TYPE_KEYVAL.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                REF_PROJECT_TYPE_KEYVAL.Add(dr["id"].ToString(), dr["name"].ToString());
            }

            // Loading GET_ALL_REF_PROJECT_COMPLEXITY_QUERY
            qry = CommonStatic.GET_ALL_REF_PROJECT_COMPLEXITY_QUERY;
            cmd = new SqlCommand(qry);
            dt = CommonStatic.GetData(cmd);
            REF_PROJECT_COMPLEXITY_KEYVAL.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                REF_PROJECT_COMPLEXITY_KEYVAL.Add(dr["id"].ToString(), dr["name"].ToString());
            }

            // Loading GET_ALL_REF_RESOURCES_SKILL_LEVEL_QUERY
            qry = CommonStatic.GET_ALL_REF_RESOURCES_SKILL_LEVEL_QUERY;
            cmd = new SqlCommand(qry);
            dt = CommonStatic.GetData(cmd);
            REF_RESOURCES_SKILL_KEYVAL.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                REF_RESOURCES_SKILL_KEYVAL.Add(dr["id"].ToString(), dr["name"].ToString());
            }

            // Loading GET_ALL_REF_TEAM_STRUCTURE_QUERY
            qry = CommonStatic.GET_ALL_REF_TEAM_STRUCTURE_QUERY;
            cmd = new SqlCommand(qry);
            dt = CommonStatic.GetData(cmd);
            REF_TEAM_STRUCTURE_KEYVAL.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                REF_TEAM_STRUCTURE_KEYVAL.Add(dr["id"].ToString(), dr["name"].ToString());
            }

            


            Application.Add("REF_PROJECT_SCOPE_KEYVAL", REF_PROJECT_SCOPE_KEYVAL);
            Application.Add("REF_DEPLOYMENT_ENVIRONMENT_KEYVAL", REF_PROJECT_SCOPE_KEYVAL);
            Application.Add("REF_PROJECT_TYPE_KEYVAL", REF_PROJECT_TYPE_KEYVAL);
            Application.Add("REF_PROJECT_COMPLEXITY_KEYVAL", REF_PROJECT_COMPLEXITY_KEYVAL);
            Application.Add("REF_RESOURCES_SKILL_KEYVAL", REF_RESOURCES_SKILL_KEYVAL);
            Application.Add("REF_TEAM_STRUCTURE_KEYVAL", REF_TEAM_STRUCTURE_KEYVAL);
        }
    }
}