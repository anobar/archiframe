﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="ArchiFrame.Account.Register1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
    <div class="body_header_style">
        Sign Up 
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
                       <div class="body_content_style">
                                First Name
                                <asp:TextBox runat="server" ID="txtFirstName" />
                                <asp:RequiredFieldValidator ID="fieldValFirstName" runat="server" ControlToValidate="txtFirstName"
                                    CssClass="field-validation-error" ErrorMessage="The first name field is required." />
                                <br />
                                Last Name
                                <asp:TextBox runat="server" ID="txtLastName" />
                                <asp:RequiredFieldValidator ID="fieldValLastName" runat="server" ControlToValidate="txtLastName"
                                    CssClass="field-validation-error" ErrorMessage="The last name field is required." />

                                <br />
                                Email address (Username)
                                <asp:TextBox runat="server" ID="txtEmail" TextMode="Email" />
                                <asp:RequiredFieldValidator ID="fieldValEmail" runat="server" ControlToValidate="txtEmail"
                                    CssClass="field-validation-error" ErrorMessage="The email address field is required." />
                                <asp:RegularExpressionValidator ID="fieldValEmailRegex" runat="server"  
                                    CssClass="field-validation-error" ErrorMessage="Invalid email address" ControlToValidate="txtEmail"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                <br />
                                Password
                                <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" />
                                <asp:RequiredFieldValidator ID="fieldValPassword" runat="server" ControlToValidate="txtPassword"
                                    CssClass="field-validation-error" ErrorMessage="The password field is required." />
                                <asp:RegularExpressionValidator ID="fieldValPasswordRegex" runat="server"  
                                    CssClass="field-validation-error" ErrorMessage="Password length must be between 6 to 12 characters" ControlToValidate="txtPassword"
                                    ValidationExpression="^[a-zA-Z0-9'@&#.\s]{6,12}$" />
        
                                <br />
                                Confirm password
                                <asp:TextBox runat="server" ID="txtConfirmPassword" TextMode="Password" />
                                <asp:RequiredFieldValidator ID="fieldValConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                                     CssClass="field-validation-error" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                                <asp:CompareValidator ID="compareValConfirmPassword" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword"
                                     CssClass="field-validation-error" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />

                                <br />
                                Company Name
                                <asp:TextBox runat="server" ID="txtCompanyName" />
                                <asp:RequiredFieldValidator ID="fieldValCompanyName" runat="server" ControlToValidate="txtCompanyName"
                                    CssClass="field-validation-error" ErrorMessage="The company name field is required." />

                           <br />
                           <asp:Label ID="lblErrorMessage" runat="server" Text=""  CssClass="field-validation-error"></asp:Label>
                           <br />
                        <asp:Button ID="btnRegister" runat="server" Text="Register" OnClick="btnRegister_Click" />
                </div>
</asp:Content>
