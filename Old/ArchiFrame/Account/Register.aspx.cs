﻿using ArchiFrame.Class;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArchiFrame.Account
{
    public partial class Register1 : System.Web.UI.Page
    {
        static string DEFAULT_ROLE = "Administrator";
        //static string CnnString = ConfigurationManager.ConnectionStrings["ArchiFrameConnectionString"].ConnectionString;
        //static string AddClientQuery = "INSERT INTO [dbo].[Client] ([companyName]) VALUES (@companyName); SELECT CAST(scope_identity() AS int)";
        //static string UpdateUsersTableQuery = "UPDATE Users SET FirstName=@firstName, LastName=@LastName, clientId= @clientId where UserId=@userId";



        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private string AddUser()
        {           
            MembershipUser usr = Membership.CreateUser(txtEmail.Text, txtPassword.Text);
            string userId = usr.ProviderUserKey.ToString();
            return userId;
        }

        private string AddAdditionalUserProfileInfo_Transaction(string userId)
        {
            SqlTransaction transaction = null;
            SqlConnection con = null;
            string retVa = string.Empty;

            try
            {
                // Add user to role
                string[] users = { txtEmail.Text };
                Roles.AddUsersToRole(users, DEFAULT_ROLE);
                
                //// Add Client record
                //string strConnString = CnnString;
                //SqlConnection con = new SqlConnection(strConnString);
                //con.Open();
                //string query = AddClientQuery;
                //SqlCommand cmd = new SqlCommand(query, con);
                //cmd.Parameters.AddWithValue("@companyName", txtCompanyName.Text.Trim());
                //int id = (Int32)cmd.ExecuteScalar();
                //string clientId = id.ToString();
                //con.Close();

                string strConnString = CommonStatic.CnnString;
                con = new SqlConnection(strConnString);
                con.Open();

                // Add client (company name) - as part of Atomic Transaction
                string query = CommonStatic.ADD_CLIENT_QUERY;
                transaction = con.BeginTransaction();
                SqlCommand cmd = new SqlCommand(query, con, transaction);
                cmd.Parameters.AddWithValue("@companyName", txtCompanyName.Text.Trim());
                int id = (Int32)cmd.ExecuteScalar();
                string clientId = id.ToString();


                // Update additional fields in Users table - as part of Atomic Transaction

                query = CommonStatic.UPDATE_USERS_QUERY;
                cmd = new SqlCommand(query, con, transaction);
                cmd.Parameters.AddWithValue("@firstName", txtFirstName.Text);
                cmd.Parameters.AddWithValue("@lastName", txtLastName.Text);
                cmd.Parameters.AddWithValue("@clientId", clientId);
                cmd.Parameters.AddWithValue("@userId", userId);
                cmd.ExecuteNonQuery();
               
                // Commit the transaction.
                transaction.Commit();
            }

            catch
            {
                // Roll back the transaction. 
                transaction.Rollback();

                // TODO: Delete just created user and user_in_role
            }
            con.Close();
            return retVa;
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {

                string userId = AddUser();
                AddAdditionalUserProfileInfo_Transaction(userId);

                FormsAuthentication.SetAuthCookie(txtEmail.Text, createPersistentCookie: false);

                // Set Cookie


                // Redirect user
                Response.Redirect("~/Estimator/ProjectGeneralInfo.aspx");
            }
            catch(Exception ex) 
            {
                lblErrorMessage.Text = ex.Message;
            }



            
        }
    }
}