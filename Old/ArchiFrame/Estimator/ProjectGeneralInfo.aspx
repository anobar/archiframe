﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectGeneralInfo.aspx.cs" Inherits="ArchiFrame.Estimator.ProjectGeneralInfo" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentHeader" runat="server">
     <link href="../StyleSheets/ProjectGeneralInfo.css" rel="stylesheet" />
    <div class="body_header_style" >
        Project Information
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="body_content_style">
   
    <asp:Wizard ID="EstimatorWizard" runat="server" ActiveStepIndex="2" Height="330px" Width="984px" OnFinishButtonClick="EstimatorWizard_FinishButtonClick" DisplaySideBar="false">
        <WizardSteps>
            <asp:WizardStep ID="WizardStep1" runat="server" title="General">
                <br />
                Select the project from the list<br />
                <asp:DropDownList ID="ddlProjects" runat="server" OnSelectedIndexChanged="ddlProjects_SelectedIndexChanged" AutoPostBack="true" class="drop_down_list_style">
                </asp:DropDownList>
                &nbsp;or<asp:LinkButton ID="lbtnAddNewProject" runat="server" OnClick="lbtnAddNewProject_Click">add new project</asp:LinkButton>
                <br />
                <br />
                Project Name<br />
                <asp:TextBox ID="txtProjectName" runat="server" Width="397px" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProjectName" CssClass="field_validation_error" ErrorMessage="The Project Name is required." />
                <br />
                Project Type<br />
                <asp:DropDownList ID="ddlProjectType" runat="server" class="drop_down_list_style">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlProjectType"
                ErrorMessage="Please select a project type" InitialValue="-1" CssClass="field_validation_error"></asp:RequiredFieldValidator>
                <br />
                <br />
                </asp:WizardStep>
            <asp:WizardStep ID="WizardStep2" runat="server" title="Scope">
                Specify the scope of the project:<br />
                <asp:CheckBoxList ID="chkListProjectScope" runat="server" Class="check_box_list_style">
                </asp:CheckBoxList>              
              
                <br />

                Specify the environment(s) you&#39;ll be deploying the project to:<br />
                <asp:CheckBoxList ID="chkListEnvironments" runat="server" Class="check_box_list_style">
                </asp:CheckBoxList>

            </asp:WizardStep>
            <asp:WizardStep ID="WizardStep3" runat="server" Title="Assumptions">
                Contingency (%)<br />
                <asp:TextBox ID="txtContingency" runat="server" Width="145px" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtContingency" CssClass="field_validation_error" ErrorMessage="The Project Contingency is required" />
                <br />
                Project Overal Complexity<br />
                <asp:DropDownList ID="ddlProjectComplexity" runat="server" class="drop_down_list_style">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlProjectComplexity"
                ErrorMessage="Please select project complexity" InitialValue="-1" CssClass="field_validation_error"></asp:RequiredFieldValidator>

                <br />
                <br />
                Resources Skills<br />
                <asp:DropDownList ID="ddlResourcesSkills" runat="server" class="drop_down_list_style">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlResourcesSkills"
                ErrorMessage="Please select resources skills" InitialValue="-1" CssClass="field_validation_error"></asp:RequiredFieldValidator>
                <br />
                <br />
                Team Structure<br />
                <asp:RadioButtonList ID="rdoListTeamStructure" runat="server" Class="check_box_list_style">
                </asp:RadioButtonList>
<%--                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="rdoListTeamStructure" CssClass="field_validation_error" 
                    ErrorMessage="Please select team structure" />--%>

            </asp:WizardStep>
        </WizardSteps>
           <HeaderTemplate>
               <ul id="wizHeader">
                   <asp:Repeater ID="SideBarList" runat="server">
                       <ItemTemplate>
                           <li><a class="<%# GetClassForWizardStep(Container.DataItem) %>" title="<%#Eval("Name")%>">
                               <%# Eval("Name")%></a> </li>
                       </ItemTemplate>
                   </asp:Repeater>
               </ul>
           </HeaderTemplate>
    </asp:Wizard>

    </div>
</asp:Content>

