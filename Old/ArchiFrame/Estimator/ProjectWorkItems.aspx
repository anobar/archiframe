﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectWorkItems.aspx.cs" Inherits="ArchiFrame.Estimator.ProjectWorkItems" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
    <div class="body_header_style">
        Project Work Items
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="body_content_style">
    Select project from the list
    <br />
    <asp:DropDownList ID="ddlProjects" runat="server" OnSelectedIndexChanged="ddlProjects_SelectedIndexChanged" AutoPostBack="true" class="drop_down_list_style"></asp:DropDownList>
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnRowEditing = "EditWorkItem" OnRowDataBound = "WorkItemRowDataBound" OnRowUpdating = "UpdateWorkItem" OnRowCancelingEdit = "CancelEdit" OnRowDeleting="DeleteWorkItem" DataKeyNames = "id">
        <Columns>
            <asp:TemplateField HeaderText="type" SortExpression="type">
                    <ItemTemplate>
                    <asp:Label ID="lblWorkItemType" runat="server" Text='<%# Eval("name")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                        <asp:Label ID="lblWorkItemTypeHidden" runat="server" Text='<%# Eval("name")%>' Visible = "false"></asp:Label>
                    <asp:DropDownList ID="ddlType" runat="server" class="drop_down_list_style"></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="complexity" SortExpression="complexity">
                    <ItemTemplate>
                    <asp:Label ID="lblWorkItemComplexity" runat="server" Text='<%# Eval("complexity")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                        <asp:Label ID="lblWorkItemComplexityHidden" runat="server" Text='<%# Eval("complexity")%>' Visible = "false"></asp:Label>
                    <asp:DropDownList ID="ddlComplexity" runat="server" class="drop_down_list_style"></asp:DropDownList>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="description" SortExpression="description">
                    <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("description")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                        <asp:Label ID="lblDescriptionHidden" runat="server" Text='<%# Eval("description")%>' Visible = "false"></asp:Label>
                    <asp:TextBox ID="txtDescription" runat="server" ></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="quantity" SortExpression="quantity">
                    <ItemTemplate>
                    <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("quantity")%>'></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                        <asp:Label ID="lblQuantityHidden" runat="server" Text='<%# Eval("quantity")%>' Visible = "false"></asp:Label>
                    <asp:TextBox ID="txtQuantity" runat="server" ></asp:TextBox>
                </EditItemTemplate>
            </asp:TemplateField>

            <asp:CommandField ShowEditButton="True" ShowDeleteButton="true" />
        </Columns>
    </asp:GridView>

    <br />
    <asp:Label ID="lblAddType" runat="server" Text="Type"></asp:Label>
    <asp:DropDownList ID="ddlAddType" runat="server" class="drop_down_list_style">
    </asp:DropDownList>
    &nbsp;&nbsp;
    <asp:Label ID="lblAddComplexity" runat="server" Text="Complexity"></asp:Label>
    <asp:DropDownList ID="ddlAddComplexity" runat="server" class="drop_down_list_style">
    </asp:DropDownList>
    &nbsp;&nbsp;
    <asp:Label ID="lblAddDescription" runat="server" Text="Description"></asp:Label>
    <asp:TextBox ID="txtAddDescription" runat="server" Width="200px"></asp:TextBox>
    &nbsp;&nbsp;
    <asp:Label ID="lblAddQuantity" runat="server" Text="Quantity"></asp:Label>
    <asp:TextBox ID="txtAddQuantity" runat="server"  Width="25px"></asp:TextBox>
    &nbsp;&nbsp;
    <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add" class="button_style"/>

    <br />
    <br />
    <asp:Button ID="btnEditProjectInfo" runat="server" OnClick="btnEditProjectInfo_Click" Text="Edit Project Info" class="button_style"/>
    <asp:Button ID="btnEstimate" runat="server" OnClick="btnEstimate_Click" Text="Estimate" class="button_style"/>
    </div>
</asp:Content>

